# Brianzacque backend

> modello dati del backend BA

## Per visualizzare l'app in locale

Per la prima installazione: 
0. verifica di avere node 14.18.1
1. fai pull del branch 
2. cancella node_modules e client/lib
3. npm i
4. git checkout npm-shrinkwrap.json
2. nodemon

Altrimenti: 
0. verifica di avere node 14.18.1
1. fai pull del branch 
2. nodemon

## Comandi database

connessione db remoto

```js
mongo ds021388-a0.mlab.com:21388/heroku_4fs9nlvh -u plurimedia -p Canonico27!
```

export db locale

```js
mongodump --db brianzacque_backend (-c = Collection opzionale)
```

import db local in remoto

```js

es: importare la collection dei bandi
mongorestore -h ds021388-a1.mlab.com:21388 -d heroku_4fs9nlvh -c Bando -u plurimedia -p Canonico27! --batchSize=1000 ./dump/brianzacque_backend/Bando.bson

```

export db remoto a locale

```js
es: esportare la collection delle commesse
mongodump -h ds021388-a1.mlab.com:21388 -d heroku_4fs9nlvh -c Commessa -u plurimedia -p Canonico27! -o ~/Desktop

```

import file in db locale

```js
mongorestore -h localhost:27017 -d ilmas_admin -c Immagine Immagine.bson
```

## Pagina casetta

un endpoint per ogni casetta restituisce una pagina html da mostare a video sulla casetta fisica. ad ogni richiesta segna nella casetta la data alla quale è avvvenuta. Se una casetta non risponde entro nelle due ore mostro in interfaccia lo stato critico. 

## Cron

### recupero video casette

videoCasette.js: ogni giorno lavorativo alle 8 del mattino controlla quali video sono già stati trasferiti su s3 dal canale vimeo. Dobbiamo trasferire su s3 er evitare lo streaming e cahcare i video per consumare meno banda. Il cron confronta i video di vimeo di uno specifico album (in conf) e li confronta con quelli presenti nella collection video_casetta. Se alcuni non sono presenti li trasferisce e aggiorna la tabella. In questo modo l'utente può solo scegliere video già trasferiti su s3. Un log segna l'esito del cron ad ogni esecuzione.

### Controllo stato video delle casette

statoVideoCasette.js controlla per ogni casetta la data di richiesta della pagina: se l'intervallo è superiore a quello impostato in config.js invia una mail segnalando tutte le casette "critiche"

## Ruoli

vengono impostati in auth0 in app_metadata come array
un utente può avere uno o più di questo ruoli

### admin

gestisce e vede tutti gli elementi del backend

### appalti

gestisce e vede tutti gli elementi del backend della sezione gare e appalti (bandi, contratti, esisti)

### appalti_lettura

vede tutti gli elementi del backend della sezione gare e appalti (bandi, contratti, esisti) ma non può modificarli

### comunicati

gestisce e vede tutti gli elementi del backend della sezione comunicati

### cantieri

gestisce e vede tutti gli elementi del backend della sezione cantieri 

### acquisti

Gestisce le commesse nella fase acquisti

### casette

gestisce e vede tutti gli elementi del backend della sezione casette 

### casette_lettura

vede tutti gli elementi del backend della sezione casette 

### operatore_casette

Puo gestire un rilevamento o una manutenzione di una casetta

### commesse_admin

può inserire una commessa: può anche modificare, cancellare qualsiasi commessa, intervenire in ogni fase

### dirigente

vede tutte le commesse in lettura, vede la tab report della commessa

### rup

Può essere un responsabile di commessa, vede le commesse di cui è responsabile
Può inserire una commessa in uno dei suoi ambiti compilando solo alcuni campi

### rup_progettazione

gestisce le commesse per le quali è responsabile
può essere selezionato come responsabile di commessa nelle commesse di tipo progettazione

### rup_direzione_tecnica

gestisce le commesse per le quali è responsabile
può essere selezionato come responsabile di commessa nelle commesse di tipo direzione tecnica

### rup_altro

gestisce le commesse per le quali è responsabile
può essere selezionato come responsabile di commessa nelle commesse di tipo divisione sistemi

### commesse_delega

agisce su commesse e cantieri del settore di cui ha la delega

### commesse_delega_{settore}

Vede e agisce come rup nella commesse del suo settore
vede il report della commessa
gestisce i cantieri del suo settore

nb: un utente non può avere sia il ruolo di rup che delegato

### pagine

### eventi

### faq

### glossario

### sportelli

### autore
lettura scrittura articoli

### revisore
lettura scrittura e approvazione articoli
invio newsletter newsletter

### laboratorio-analisi

### laboratorio-regole



