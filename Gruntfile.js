module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    sass: {
      main: {
        options: {
          style: 'expanded',
          includePaths: [ 'client/lib' ]
        },
        files: {
          'client/css/app.css': 'client/scss/style.scss'
        }
      },
      ckeditor: {
        options: {
          style: 'compressed',
          includePaths: [ 'client/lib' ]
        },
        files: {
          'client/css/ckeditor.css': 'client/scss/ckeditor.scss'
        }
      }
    },
    watch: {
      styles : {
        files: ["client/scss/*.scss"],
        tasks: ['sass']
      }
    }
  });

  // Default task.
  grunt.registerTask('default', ['watch']);
  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  



};
