// autorizzazione per ruolo: roles= array di ruoli che possono eseguire l'operazione
var _ = require('underscore');

exports.role = function (roles)
{
   return function (req, res, next)
   {
   		// controllo prima se l'utente collegato ha ruoli
   		var userroles = req.user.app_metadata ? req.user.app_metadata.roles : false;
   		// e infine se ha almeno uno dei ruoli di roles
   		if(_.intersection(userroles,roles).length > 0)
   			next();
   		else
   			res.status(403).send('Non autorizzato');
   }
}