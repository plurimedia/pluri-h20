var CONFIG = require(process.cwd() + '/server/config.js'),
    https = require('https'),
    fs = require('fs'),
    mkdirp = require('mkdirp'),
    path = require('path'),
    mongoose = require('mongoose'),
	_ = require('underscore'),
    moment = require('moment'),
    zip = require('express-zip'),
    pdf = require('html-pdf'),
    mustache = require('mustache'),
    async = require('async'),
    axios = require('axios'),
    numeral = require('numeral'),
    s3 = require('s3'),
    s3Client = s3.createClient({
      s3Options: {
        accessKeyId: CONFIG.AWS_ACCESS_KEY_ID,
        secretAccessKey: CONFIG.AWS_SECRET_KEY,
        region: CONFIG.AWS_REGION
      },
    }),
    Casetta = mongoose.model('Casetta'),
    InterventoCasetta = mongoose.model('InterventoCasetta'),
    LaboratorioAnalisi = mongoose.model('LaboratorioAnalisi');
    LetturaCasetta = mongoose.model('LetturaCasetta'); 

/* @@@@@@@@@@@@@@ CASETTE @@@@@@@@@@@@@@  */

exports.casette = {};

getIdVideo = function(text) {
    var str = text; //"es. https://d3aefyb3iylg50.cloudfront.net/266112408.mp4";
    var slash = str.lastIndexOf("/");
    var dot = str.lastIndexOf(".");
    var finalString = str.substr(slash+1, dot);
    return finalString;
}


// pagina video della casetta
exports.casette.video = function (req, res, next) {
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) 
       return next("Casetta non trovata");

    var ipRichiesta = req.headers['x-forwarded-for'] || req.connection.remoteAddress

    Casetta.findOne({
        _id: req.params.id
    }, function (err, casetta) {
        
        if (err)
            return next(err);

        if(casetta) {
       casetta.mostravideo = casetta.mostrapagina = casetta.entrambi =false

        // per mustache
        if(casetta.tipovideo===10 || casetta.tipovideo===30)
            casetta.mostravideo = true
        if(casetta.tipovideo===20 || casetta.tipovideo===30)
            casetta.mostrapagina = true
        if(casetta.tipovideo===30)
            casetta.entrambi = true;

        casetta.durataPagina = CONFIG.VIDEO_CASETTA_PAGE_DURATION;
        casetta.intervalloRefresh = CONFIG.VIDEO_CASETTA_REFRESH_INTERVAL;
        casetta.data_avviamento_pretty = moment(casetta.data_avviamento).format('DD/MM/YYYY')

        /* recupero l'ultimo intervento: DUPLICO LA LOGICA API (migliorare se va in prod )*/
        
        InterventoCasetta.find({casetta: casetta._id, tipo:'rilevamento'}, function(err, rilevamenti){
            if(err)
                return next(err);

            var rilevamento = rilevamenti[0];


            // trovo le analisi
            LaboratorioAnalisi.findOne({visibile: true}, function (err, analisi) {

                if (err)
                    return next(err);
          
                if (analisi && !analisi.sospendi) {
                    casetta.analisi = _.find(analisi.analisi, function (a) {
                        return a.codiceSif === casetta.codiceSif
                    })
                }

                /* se c'è l'ultimo rilevamento sistemo per il FE aggiungo alcune proprietà derivate */

                if(rilevamento) {
                    var rilevamentoPretty = rilevamento;
                    rilevamentoPretty.data = moment(rilevamento).format('DD/MM/YYYY')
                    rilevamentoPretty.totale = rilevamento.ambiente + rilevamento.fredda + rilevamento.gasata;
                    rilevamentoPretty.risparmio_bottiglie = Math.round(rilevamentoPretty.totale/1.5);
                    rilevamentoPretty.risparmio_famiglie = Math.round(rilevamentoPretty.totale * 0.22 - rilevamentoPretty.totale * 0.05);
                    rilevamentoPretty.risparmio_acqua = Math.round(rilevamentoPretty.totale * 7);
                    rilevamentoPretty.risparmio_greggio = Math.round((rilevamentoPretty.totale * 162) / 1000);
                    rilevamentoPretty.risparmio_co = Math.round((rilevamentoPretty.totale * 100) / 1000);
                    rilevamentoPretty.risparmio_smaltimento = Math.round(rilevamentoPretty.totale * 0.08);
                    casetta.rilevamento = rilevamentoPretty;
                }


                // dopo una certa ora fai vedere una pagina con solo il logo
                var orasera = moment().set('hour', CONFIG.VIDEO_CASETTA_NIGHTPAGE_HOUR_EVENING);
                var oramattina = moment().set('hour', CONFIG.VIDEO_CASETTA_NIGHTPAGE_HOUR_MORNING);
                var adesso = moment();


                var nightpage = adesso.isAfter(orasera, 'hour') || adesso.isBefore(oramattina, 'hour');

                if(nightpage) {
                    casetta.mostravideo = casetta.mostrapagina = false
                    casetta.nightpage = true
                }

                casetta.formatPrice = function () {
                    return function (text, render) {
                      let n = parseFloat(render(text))
                      return numeral(n).format('$0,0.00')
                    }
                }

                casetta.formatNumber = function () {
                    return function (text, render) {
                      let n = parseFloat(render(text))
                      return numeral(n).format('0,0.00')
                    }
                }

                casetta.video = './videos/'+getIdVideo(casetta.video)// per nuova logica pluri-iot, ha già tutti i video cachati, basta passargli il nome del video

                res.render('paginacasetta',casetta);


                if(req.query.anteprima) { // se no è un anteprima salva la richiesta nei log

                    // log.save(function(err, resp) {
                       console.log('!')

                        // segno la data di richiesta nella casetta
                        Casetta.update({_id: casetta._id}, {
                            ultima_chiamata_video: new Date(),
                            ip: ipRichiesta
                        }, function(err, resp) {
                           
                           if(err)
                            return next(err)

                        })

                    //})
                }
            })           
            .sort({data: -1})
            .limit(1)
            .lean();

        })
        .sort({data: -1})
        .limit(1)
        .lean();
        } else {
            res.render('paginacasetta_not_found');
        }
    }).lean() // devo restituite il semplice oggetto 
}

exports.casetta = {
    lettura:{}
};

//la versione v2 deve andare per un pò in parallelo con la v1 di prod (in pluri h2o è la stessa)
exports.casetta.lettura.save = function (req, res, next) {

    console.log('api lettura pluri iot', req.body, req.body.fonte)
    console.log('tipo di dato', typeof req.body)

    if (req.body.fonte == 2) { // pluri IOT +

        LetturaCasetta.findOne({casetta:req.body.casetta},  // trova l'ultima lettura per tenere i totali in caso di mancato 
            function (err, lastRecord) {
                if (err){
                    // console.log(err)
                    return next(err);
                } else {
                    var lettura = {
                      casetta: req.body.casetta,
                      fonte: req.body.fonte,
                      mac_address: req.body.mac_address,
                      timestamp: req.body.timestamp,
                      user: req.body.user || null,
                      pins: []
                    }

                    if (lastRecord) {
                      lettura.pins['1'] = lastRecord.pins['1']
                      lettura.pins['2'] = lastRecord.pins['2']
                      lettura.pins['3'] = lastRecord.pins['3']
                      lettura.pins['4'] = lastRecord.pins['4']
                      lettura.pins['5'] = lastRecord.pins['5']
                      lettura.pins['6'] = lastRecord.pins['6']
                      lettura.pins['7'] = lastRecord.pins['7']
                    }

                    req.body.pins.forEach(function(pin){
                      if (pin.pin === 1) {
                        lettura.pins['1'] = pin
                      }
                      if (pin.pin === 2) {
                        lettura.pins['2'] = pin
                      }
                      if (pin.pin === 3) {
                        lettura.pins['3'] = pin
                      }
                      if (pin.pin === 4) {
                        lettura.pins['4'] = pin
                      }
                      if (pin.pin === 5) {
                        lettura.pins['5'] = pin
                      }
                      if (pin.pin === 6) {
                        lettura.pins['6'] = pin
                      }
                      if (pin.pin === 7) {
                        lettura.pins['7'] = pin
                      }
                    })

                    console.log('salvo lettura Pluri IOT +', lettura)

                    var lettura = new LetturaCasetta(lettura);
                    lettura.save(function (err, result) {
                        if (err) {
                          console.log('errore salvataggio lettura Pluri IOT +')
                          return next(err);
                        }
                        else {
                          res.status(200).send(result);
                        }
                    });
                }
            }
        ).sort({mdata:-1})

    }
    else if (req.body.fonte == 1) { // pluri iot mini: 1 solo pin
        console.log('lettura mini!', req.body)
     
        let casettaid;

        Casetta.findOneAndUpdate({
            mac_address: req.body.mac_address
        }, 
        { ultima_chiamata_video: new Date().toISOString() },
            function (err, result) {
                if (err)
                    res.status(500).send('pluri mini non associato')
                else{
                    if(result){
                        casettaid =result._doc._id
                        tipo = result._doc.tipo
                        try {
            
                            var lettura2 = {
                                casetta: casettaid,
                                tipo: tipo,
                                fonte: req.body.fonte,
                                mac_address: req.body.mac_address,
                                timestamp: new Date().toISOString(), //req.body.timestamp,
                                user: req.body.user || null,
                                pins: []
                            }

                                req.body.pins.forEach(function(pin){
                                if (pin.pin === 1) {
                                    lettura2.pins['1'] = pin
                                }
                                if (pin.pin === 2) {
                                    lettura2.pins['2'] = pin
                                }
                                if (pin.pin === 3) {
                                    lettura2.pins['3'] = pin
                                }
                                if (pin.pin === 4) {
                                    lettura2.pins['4'] = pin
                                }
                                if (pin.pin === 5) {
                                    lettura2.pins['5'] = pin
                                }
                                if (pin.pin === 6) {
                                    lettura2.pins['6'] = pin
                                }
                                if (pin.pin === 7) {
                                    lettura2.pins['7'] = pin
                                }
                                })
                            
                        console.log('salvo lettura PluriIot mini', lettura2)
                        var lettura3 = new LetturaCasetta(lettura2);
                    } catch (err) {
                            console.log('errore')
                            res.status(500).send('errorazzo mini 1')
                    }
            
            
                    lettura3.save(function (err, result) {
                            if (err){
                                console.log('errorazzo mini')
                                res.status(500).send('errorazzo mini 2')
                            }
                            else {
                                res.status(200).send('ok');
                            }
                        });
                    }
                }
            })
     
    } else {
        res.status(400).send('Dati in post non nel formato giusto')
    }
    
}
   