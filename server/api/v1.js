var CONFIG = require(process.cwd() + '/server/config.js'),
    https = require('https'),
    fs = require('fs'),
    mkdirp = require('mkdirp'),
    path = require('path'),
    mongoose = require('mongoose'),
	_ = require('underscore'),
    moment = require('moment'),
    zip = require('express-zip'),
    pdf = require('html-pdf'),
    mustache = require('mustache'),
    async = require('async'),
    s3 = require('s3'),
    s3Client = s3.createClient({
      s3Options: {
        accessKeyId: CONFIG.AWS_ACCESS_KEY_ID,
        secretAccessKey: CONFIG.AWS_SECRET_KEY,
        region: CONFIG.AWS_REGION
      },
    }),
    Comunicato = mongoose.model('Comunicato'),
    Evento = mongoose.model('Evento'),
    Bando = mongoose.model('Bando'),
    Esito = mongoose.model('Esito'),
    Casetta = mongoose.model('Casetta'),
    InterventoCasetta = mongoose.model('InterventoCasetta'),
    Pagina = mongoose.model('Pagina'),
    Menu = mongoose.model('Menu');
    Articolo = mongoose.model('Articolo'),
    Sportello = mongoose.model('Sportello'),
    Avviso = mongoose.model('Avviso'),
    Faq = mongoose.model('Faq'),
    Glossario = mongoose.model('Glossario'),
    LaboratorioAnalisi = mongoose.model('LaboratorioAnalisi');
    LetturaCasetta = mongoose.model('LetturaCasetta');

/* @@@@@@@@@@@@@@ COMUNICATI @@@@@@@@@@@@@@  */

exports.comunicati = {};

exports.comunicati.sito = function (req,res,next) {

    var query = {visibile: true},
        limit= 0;

    if(req.query.anno)
        query.anno = req.query.anno;

    if(req.query.limit)
        limit = parseInt(req.query.limit);

    Comunicato.find(query, function (err, result) {

        if (err)
            return next(err);

        res.status(200).json(result);

    })
    .sort({data: -1})
    .limit(limit);
}

exports.comunicati.get = function (req, res, next) {
    Comunicato.findOne({
        _id: req.params.id
    }, function (err, result) {
        if (err)
            return next(err);
        else
            res.status(200).json(result);
    });
}

/* @@@@@@@@@@@@@@ EVENTI @@@@@@@@@@@@@@  */

exports.eventi = {};

exports.eventi.sito = function (req,res,next) {

    var query = {visibile: true},
        limit= 0;

    if(req.query.anno)
        query.anno = req.query.anno;

    if(req.query.limit)
        limit = parseInt(req.query.limit);

    Evento.find(query, function (err, result) {
        if (err)
            return next(err);

        res.status(200).json(result);

    })
    .sort({da: -1})
    .limit(limit);
}

exports.eventi.get = function (req, res, next) {
    Evento.findOne({
        _id: req.params.id
    }, function (err, result) {
        if (err)
            return next(err);
        else
            res.status(200).json(result);
    });
}

/* @@@@@@@@@@@@@@ BANDI @@@@@@@@@@@@@@  */

exports.bandi = {};

exports.bandi.sito = function (req,res,next) {
    Bando.find({visibile: true}, function (err, result) {
        if (err)
            return next(err);

        res.status(200).json(result);

    })
    .sort({data_pubblicazione: -1});
}

exports.bandi.get = function (req, res, next) {
    Bando.findOne({
        _id: req.params.id
    }, function (err, result) {
        if (err)
            return next(err);
        else
            res.status(200).json(result);
    });
}

/* download di tutti i files di un bando */
exports.bandi.download = function (req, res, next) {
    Bando.findOne({
        _id: req.params.id
    }, function (err, bando) {
        if (err)
            return next(err);


        // 1) recupero i files da scaricare
        var filesToDownload = _.compact(_.flatten(_.pluck(_.pick(bando,'files').files,'files'))),
            date = new Date(),
            dir = path.join('.tmp','download_'+date.valueOf().toString());

        // 2) creo una dir temporanea dove mettere i files

        mkdirp(dir, function (err) {
            if (err)
                return next(err)

            // 3) scarico tutti i files e li metto nella dir
            async.each(filesToDownload, function(file,cb){

                var filestream = fs.createWriteStream(path.join(dir,file.name)+path.extname(file.url));

                https.get(file.url, function(response) {
                    response.pipe(filestream);
                    filestream.on('finish', function() {
                        filestream.close();
                        cb();
                    });
                })

            },function(err){
                if(err)
                    return next(err);

                // 4) leggo i files da zippare
                fs.readdir(dir, function (err, files) {
                    if(err)
                        return next(err);

                    // 5) li metto nel format che si aspetta la libreria
                    var filesToZip = _.map(files, function(file){
                            return {
                                path: path.join(dir,file),
                                name: path.basename(file)
                            }
                        }),
                        zipName = path.join(moment(bando.data_pubblicazione).format('DD_MM_YYYY'),bando.tipologia.split(' ').join('-').toLowerCase()+'.zip')

                    // 6) mando lo zip al client
                    res.zip(filesToZip,zipName, function(err, zipped){

                        if(err)
                            return next(err)
                    })

                });
            })
        });


    });
}


/* @@@@@@@@@@@@@@ ESITI @@@@@@@@@@@@@@  */

exports.esiti = {};

exports.esiti.sito = function (req,res,next) {

    var query = {visibile: true};

    if(req.query.anno)
        query.anno = req.query.anno;


    Esito.find(query, function (err, result) {
        if (err)
            return next(err);

        res.status(200).json(result);

    })
    .sort({data_aggiudicazione: -1});
}

exports.esiti.get = function (req, res, next) {
    Esito.findOne({
        _id: req.params.id
    }, function (err, result) {
        if (err)
            return next(err);
        else
            res.status(200).json(result);
    });
}


/* @@@@@@@@@@@@@@ CASETTE @@@@@@@@@@@@@@  */

exports.casette = {};

exports.casette.sito = function (req,res,next) {

    Casetta.find({}
    , function (err, result) {
        if (err)
            return next(err);
        res.status(200).json(result);
    })
    .sort({descrizione: 1})
    .lean();

}

//aggiunta per poc PluriTank sito
exports.casette.sito_esteso = function (req,res,next)
{
    Casetta.find({}, function (err, casette)
    {
        if (err) return next(err);

        InterventoCasetta.find({tipo:'rilevamento'}, function(err, rilevamenti)
        {
            if (err) return next(err);

            console.log("lunghezza casette", casette.length)
            console.log("lunghezza rilevamenti", rilevamenti.length)
            for (var i=0; i<casette.length; i++)
            {
                var last = null

                for (var j=0; j<rilevamenti.length; j++)
                {
                    if (casette[i]._id.toString() === rilevamenti[j].casetta.toString())
                    {
                        last = rilevamenti[j];

                        var rilevamentoPretty = last;
                        rilevamentoPretty.data_ultima_lettura= last.data;
                        rilevamentoPretty.ambiente = last.ambiente;
                        rilevamentoPretty.fredda = last.fredda;
                        rilevamentoPretty.gasata = last.gasata;
                        rilevamentoPretty.totale = last.ambiente + last.fredda + last.gasata;
                        rilevamentoPretty.risparmio_bottiglie = Math.round(rilevamentoPretty.totale/1.5);
                        rilevamentoPretty.risparmio_famiglie = Math.round(rilevamentoPretty.totale * 0.22 - rilevamentoPretty.totale * 0.05);
                        rilevamentoPretty.risparmio_acqua = Math.round(rilevamentoPretty.totale * 7);
                        rilevamentoPretty.risparmio_greggio = Math.round((rilevamentoPretty.totale * 162) / 1000);
                        rilevamentoPretty.risparmio_co = Math.round((rilevamentoPretty.totale * 100) / 1000);
                        rilevamentoPretty.risparmio_smaltimento = Math.round(rilevamentoPretty.totale * 0.08);

                        casette[i].rilevamento = rilevamentoPretty;
                    }
                }
            }

            res.status(200).json(casette);
        })
    })
    .sort({descrizione: 1})
    .lean();
}

exports.casette.get = function (req, res, next) {
    Casetta.findOne({
        _id: req.params.id
    }, function (err, casetta) {
        if (err)
            return next(err);

        /* recupero l'ultimo intervento*/
        if (casetta){
        InterventoCasetta.find({casetta: casetta._id, tipo:'rilevamento'}, function(err, rilevamenti){
            if(err)
                return next(err);

            var rilevamento = rilevamenti[0];

            /* se c'è l'ultimo rilevamento sistemo per il FE aggiungo alcune proprietà derivate */

            if(rilevamento) {
                var rilevamentoPretty = rilevamento;
                //rilevamentoPretty.totale = rilevamento.ingresso_casetta > 0 ? rilevamento.ingresso_casetta : rilevamento.ambiente + rilevamento.fredda + rilevamento.gasata;
                rilevamentoPretty.totale = rilevamento.ambiente + rilevamento.fredda + rilevamento.gasata;
                rilevamentoPretty.risparmio_bottiglie = Math.round(rilevamentoPretty.totale/1.5);
                rilevamentoPretty.risparmio_famiglie = Math.round(rilevamentoPretty.totale * 0.22 - rilevamentoPretty.totale * 0.05);
                rilevamentoPretty.risparmio_acqua = Math.round(rilevamentoPretty.totale * 7);
                rilevamentoPretty.risparmio_greggio = Math.round((rilevamentoPretty.totale * 162) / 1000);
                rilevamentoPretty.risparmio_co = Math.round((rilevamentoPretty.totale * 100) / 1000);
                rilevamentoPretty.risparmio_smaltimento = Math.round(rilevamentoPretty.totale * 0.08);
                casetta.rilevamento = rilevamentoPretty;
            }

            res.status(200).json(casetta)
        })
        .sort({data: -1})
        .limit(1)
        .lean();
    }
    }
    ).lean() // devo restituite il semplice oggetto
}

/* @@@@@@@@@@@@@@ PAGINE @@@@@@@@@@@@@@  */

exports.pagine = {};

exports.pagine.get = function (req, res, next) {

    Pagina.findOne({
        slug: req.params.slug
    }, function (err, pagina) {

        if (err)
            return next(err);

        if(!pagina || !pagina.visibile){
            res.status(404).send('Pagina non trovata');
        }

        else {
            res.status(200).json(_.omit(pagina,'user'));
        }


    }).lean();
}

/* @@@@@@@@@@@@@@ MENU @@@@@@@@@@@@@@  */

exports.menues = {};

exports.menues.get = function (req, res, next) {

    Menu.find({}, function (err, result) {

        if (err)
            return next(err);

        res.status(200).json(_.omit(result,'user'));

    }).lean();
}

// menu per titolo
exports.menues.titolo = function (req, res, next) {

    Menu.findOne({titolo: req.params.titolo}, function (err, result) {

        if (err)
            return next(err);

        res.status(200).json(_.omit(result,'user'));

    }).lean();
}

/*@@@@@@@@@@@@@ ARTICOLO @@@@@@@*/

exports.articoli = {};

exports.articoli.sito = function (req,res,next) {

    var query = {stato: 'Pubblicato'},
        limit= 0;

    if(req.query.anno)
        query.anno = req.query.anno;

    if(req.query.sezione){
        query.sezione = { $in: [req.query.sezione,'Entrambi'] }
    }
    else{
        query.sezione = { $in: ['Entrambi'] }
    }

    if(req.query.limit)
        limit = parseInt(req.query.limit);

    Articolo.find(query, function (err, result) {

        if (err)
            return next(err);

        res.status(200).json(result);

    })
    .sort({data: -1})
    .limit(limit);
}

exports.articoli.get = function (req, res, next) {

    Articolo.findOne(
    {
        _id: req.params.id
    },
    function (err, result) {
        if (err)
            return next(err);
        else
            res.status(200).json(result);
    });
}


/*@@@@@@@@@@@@@ Sportelli @@@@@@@*/

exports.sportelli = {};

exports.sportelli.sito = function (req,res,next) {

    var query = {visibile: true};

    Sportello.find(query, function (err, result) {
        if (err)
            return next(err);

        res.status(200).json(result);

    })
    .sort({comune:1})
}

exports.avvisi = {};

exports.avvisi.sito  = function (req, res, next) {

   var query = {
        visibile: true,
        sezione:req.params.sezione
    };

    Avviso.find(query, function (err, result) {
        if (err)
            return next(err);

        res.status(200).json(result);

    })
}

/*@@@@@@@@@@@@@ Faq @@@@@@@*/

exports.faqs = {};

exports.faqs.sito = function (req,res,next) {

    var query = {visibile: true};

    Faq.find(query, function (err, result) {
        if (err)
            return next(err);

        res.status(200).json(result);

    })
    .sort({domanda:1})
}

/*@@@@@@@@@@@@@ glossario @@@@@@@*/

exports.glossario = {};

exports.glossario.sito = function (req,res,next) {

    var query = {visibile: true};

    Glossario.find(query, function (err, result) {
        if (err)
            return next(err);

        res.status(200).json(result);

    })
    .sort({termine:1})
}

/* @@@@@@@@@@@@@@ ANALISI LABORATORIO @@@@@@@@@@@@@@  */

function getIntersect(arr1, arr2) {
    var temp = [];
    for(var i = 0; i < arr1.length; i++){
        for(var k = 0; k < arr2.length; k++){
            if(arr1[i].Comune.toLowerCase().trim() == arr2[k].Comune.toLowerCase().trim()){
                temp.push( arr1[i]);
                break;
            }
        }
    }
    return temp;
}


function _ordinamentoParametriAnalisi(analisi){

    for (i in analisi)
    {
        var analisiElement = analisi[i];
         analisiElement.valori = _.clone(_.sortBy(analisiElement.valori, function(o) { return parseFloat(o.ordinamento); }))
    }
}

var analisiSito = {};



getToDay = function(){
    var result = moment().format("YYYYMMDD");
    return result;
}

 /*
    Viene controllato se l'oggetto analisiSito contiene la proprietà data registrazione;
    se questa corrisponde ad oggi viene recuperato e passato al client l'oggetto dati(analisi)
    altrimenti se questa è diversa da oggi viene interrogato il db.
*/
getAnalisi = function(next)
{
    var toDay = getToDay(),
        dataRegistrazione = analisiSito.dataRegistrazione;

    if (dataRegistrazione && dataRegistrazione == toDay)
    {
        console.log("estraggo i dati dalla cache")
        return next(analisiSito.dati);
    }

    var query = {visibile: true};
    LaboratorioAnalisi.findOne(query, function (err, result)
    {
        if (err)
        {
            console.log(err)
            return next(err);
        }

        if (result?.length)
        {
            var laboratorio_comuni_daVisualizzare =  require(process.cwd()+'/server/json/laboratorio_comuni_daVisualizzare.json');
            var analisi_daVisualizzare = getIntersect(result.analisi, laboratorio_comuni_daVisualizzare);

            result.analisi = analisi_daVisualizzare
            _ordinamentoParametriAnalisi(result.analisi);

            var parametri_daVisualizzare = getIntersect(result.parametri, laboratorio_comuni_daVisualizzare);
            result.parametri = parametri_daVisualizzare

            analisiSito = {};
            analisiSito.dataRegistrazione = toDay;
            analisiSito.dati = result;
            return next(result);
        }

        analisiSito = {};
        analisiSito.dataRegistrazione = toDay;
        analisiSito.dati = [];
        return next([]);
    })
}

var formatters = { // formatters da usare nei tpl di mustcahe
    data : function () {

        return function (text, render) {

            var a  = moment(text, "DD-MM-YYYY");
            return render(a)
        }
    }
};

exports.analisi = {};

exports.analisi.sito = function (req,res,next) {
   var result = getAnalisi(
       function(result){
           console.log("finito estrazione dati");
        res.status(200).json(result);
       }
   )
}


exports.analisi.pdf = function (req,res,next) {

    console.log("start report analisi" );

    var pdfTempPath = process.cwd()+'/.tmp/',
    headerTemplate = fs.readFileSync(process.cwd()+'/server/templates/pdf/header.html','utf8'), // scheda tecnica...
    footerTemplate = fs.readFileSync(process.cwd()+'/server/templates/pdf/footer.html','utf8'), // comune a tutti
    analisiTemplate = fs.readFileSync(process.cwd()+'/server/templates/pdf/analisi.html', 'utf8'), // specifico offerta
    css = fs.readFileSync(process.cwd()+'/server/templates/pdf/style.css','utf8'); // comune a tutti

    var codiceSif  = req.params.codiceSif;
    console.log("codiceSif ", codiceSif)

    getAnalisi(
        function(result){
            console.log("finito estrazione dati")

            var analisi = _.filter(result.analisi, function(elem){
                return elem.codiceSif == codiceSif
            })

            if (analisi && analisi.length){
                analisi = analisi[0]

            var resultPdf = {};
            resultPdf.analisi = analisi;

            resultPdf.cdate =  moment(result.cdate).format('MM-DD-YYYY');

            var date = new Date(),
                timestamp = date.valueOf(),
                data = _.extend(resultPdf,{css: css, header: headerTemplate, footer:footerTemplate, formatters}),

                html = mustache.render(analisiTemplate,data),

                pdfOptions = {
                    format: 'A4',
                    orientation: 'portrait',
                    border: '0',
                    header: {
                        height: "3cm"
                    },
                    footer: {
                        height: "1cm"
                    },
                    quality: '100'
                }

                pdf.create(html, pdfOptions).toBuffer( function (err, response)
                {
                    if(err){
                        console.log(err)
                        res.status(500).send(err);
                    }
                    res.status(200).send(response);
                })
            }
            else
                res.status(500).send("err");
        }
    )
}

   //*********************** */
exports.casetta = {
    lettura:{},
    dettaglio:{}
};

exports.casetta.dettaglio = function (req, res, next)
{
    Casetta.findOne(
    {
        _id: req.params.id
    },
    function (err, result)
    {
        if (err)
            return next(err);
        else
            res.status(200).send(result);
    });
}

//modificare dopo aver sistemato script lettura da pluriot mini
exports.casetta.lettura.save = function (req, res, next) {

    console.log('api lettura pluri iot', req.body, req.body.fonte)
    console.log('tipo di dato', typeof req.body)

    if (req.body.fonte == 2) { // pluri IOT +

        LetturaCasetta.findOne({casetta:req.body.casetta},  // trova l'ultima lettura per tenere i totali in caso di mancato
            function (err, lastRecord) {
                if (err){
                    // console.log(err)
                    return next(err);
                } else {
                    var lettura = {
                      casetta: req.body.casetta,
                      fonte: req.body.fonte,
                      mac_address: req.body.mac_address,
                      timestamp: req.body.timestamp,
                      user: req.body.user || null,
                      pins: []
                    }

                    if (lastRecord) {
                      lettura.pins['1'] = lastRecord.pins['1']
                      lettura.pins['2'] = lastRecord.pins['2']
                      lettura.pins['3'] = lastRecord.pins['3']
                      lettura.pins['4'] = lastRecord.pins['4']
                      lettura.pins['5'] = lastRecord.pins['5']
                      lettura.pins['6'] = lastRecord.pins['6']
                      lettura.pins['7'] = lastRecord.pins['7']
                    }

                    req.body.pins.forEach(function(pin){
                      if (pin.pin === 1) {
                        lettura.pins['1'] = pin
                      }
                      if (pin.pin === 2) {
                        lettura.pins['2'] = pin
                      }
                      if (pin.pin === 3) {
                        lettura.pins['3'] = pin
                      }
                      if (pin.pin === 4) {
                        lettura.pins['4'] = pin
                      }
                      if (pin.pin === 5) {
                        lettura.pins['5'] = pin
                      }
                      if (pin.pin === 6) {
                        lettura.pins['6'] = pin
                      }
                      if (pin.pin === 7) {
                        lettura.pins['7'] = pin
                      }
                    })

                    console.log('salvo lettura Pluri IOT +', lettura)

                    var lettura = new LetturaCasetta(lettura);
                    lettura.save(function (err, result) {
                        if (err) {
                          console.log('errore salvataggio lettura Pluri IOT +')
                          return next(err);
                        }
                        else {
                          res.status(200).send(result);
                        }
                    });
                }
            }
        ).sort({mdata:-1})

    }
    else if (req.body.fonte == 1) { // pluri iot mini: 1 solo pin
        console.log('lettura mini!', req.body)

        let casettaid;

        Casetta.findOneAndUpdate({
            mac_address: req.body.mac_address
        },
        { ultima_chiamata_video: new Date().toISOString() },
            function (err, result) {
                if (err)
                    res.status(500).send('pluri mini non associato')
                else{
                    if(result){
                        casettaid =result._doc._id
                        tipo = result._doc.tipo
                        try {

                            var lettura2 = {
                                casetta: casettaid,
                                tipo: tipo,
                                fonte: req.body.fonte,
                                mac_address: req.body.mac_address,
                                timestamp: new Date().toISOString(), //req.body.timestamp,
                                user: req.body.user || null,
                                pins: []
                            }

                                req.body.pins.forEach(function(pin){
                                if (pin.pin === 1) {
                                    lettura2.pins['1'] = pin
                                }
                                if (pin.pin === 2) {
                                    lettura2.pins['2'] = pin
                                }
                                if (pin.pin === 3) {
                                    lettura2.pins['3'] = pin
                                }
                                if (pin.pin === 4) {
                                    lettura2.pins['4'] = pin
                                }
                                if (pin.pin === 5) {
                                    lettura2.pins['5'] = pin
                                }
                                if (pin.pin === 6) {
                                    lettura2.pins['6'] = pin
                                }
                                if (pin.pin === 7) {
                                    lettura2.pins['7'] = pin
                                }
                                })

                        console.log('salvo lettura PluriIot mini', lettura2)
                        var lettura3 = new LetturaCasetta(lettura2);
                    } catch (err) {
                            console.log('errore')
                            res.status(500).send('errorazzo mini 1')
                    }

                    lettura3.save(function (err, result) {
                            if (err){
                                console.log('errorazzo mini')
                                res.status(500).send('errorazzo mini 2')
                            }
                            else {
                                res.status(200).send('ok');
                            }
                        });
                    }
                }
            })
    } else {
        res.status(400).send('Dati in post non nel formato giusto')
    }
}
