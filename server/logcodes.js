/*
	codice dei log
 */


var events = 'Eventi',
	email = 'Notifiche email',
	cron= 'Cron',
	codes = [
		{ code: 11, description: 'Richiesta pagina video casetta', group: events },
		{ code: 101, description: 'Valori critici casette',group: email  },
		{ code: 102, description: 'Malfunzionamento video casette',group: email  },
		{ code: 1001, description: 'Recupero video',group: cron  },
		{ code: 1002, description: 'Verifica stato video casette',group: cron }
	];

module.exports = codes




