var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ComunicatoSchema = new Schema({
        mdate: {
            type: Date,
            default: Date.now
        },
        visibile: {
            type: Boolean,
        },
        data: {
            type: Date
        },
        anno: {
            type: String
        },
        titolo: {
            type: String,
            required: true
        },
        sottotitolo: {
            type: String
        },
        luogo: {
            type: String
        },
        copertina: {
            type: Schema.Types.Mixed,
            required: true
        },
        estratto: {
            type: String
        },
        testo: {
            type: String
        },
        files: {
            type: Schema.Types.Mixed
        },
        immagini: {
            type: Schema.Types.Mixed
        },
        user: { // utente che ha modificato
            type: Schema.Types.Mixed
        },
        tag: {
            type: String
        }
    });


ComunicatoSchema.index({ titolo: 'text'},{ default_language: "italian" });
ComunicatoSchema.index({anno:1, visibile:1});

mongoose.model('Comunicato', ComunicatoSchema, 'Comunicato');
