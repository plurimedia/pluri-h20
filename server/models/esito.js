var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    EsitoSchema = new Schema({
        mdate: {
            type: Date,
            default: Date.now
        },
        bando: {
            type: Schema.ObjectId, // ref a bando
            ref: 'Bando'
        },
        titolo: {
            type: String,
            required: true
        },
        visibile: {
            type: Boolean,
        },
        data_aggiudicazione: {
            type: Date
        },
        anno: {
            type: String
        },
        stato: {
            type: String
        },
        testo: {
            type: String
        },
        files: {
            type: Schema.Types.Mixed
        },
        user: { // chi l'ha modificato
            type: Schema.Types.Mixed
        }
    });


EsitoSchema.index({ titolo: 'text'},{ default_language: "italian" });
EsitoSchema.index({anno:1,visibile:1});

mongoose.model('Esito', EsitoSchema, 'Esito');
