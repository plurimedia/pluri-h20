var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    LaboratorioRegolaSchema = new Schema({ 
        regole: {
            type: Schema.Types.Mixed 
        },
        mdate: {
            type: Date,
            default: Date.now
        },
        user: {
            type: Schema.Types.Mixed
        } 
    });

mongoose.model('LaboratorioRegola', LaboratorioRegolaSchema, 'LaboratorioRegola');
