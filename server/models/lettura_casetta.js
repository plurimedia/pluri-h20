var CONFIG = require(process.cwd() + '/server/config.js'),
    moment = require('moment'),
    mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    LetturaCasettaSchema = new Schema({
        mdata: {
            type: Date,
            default: Date.now
        },
        casetta: {
            type: Schema.ObjectId,  
            ref: 'Casetta'
        },
        counter: {
            type: String
        },
        delta: {
            type: String
        },
        fonte: {
            type: String
        },
        mac_address: {
            type: String
        },
        timestamp: {
            type: String
        },
        user: {
            type: Schema.Types.Mixed
        },
        pins:{
          type: Schema.Types.Mixed
        }
    });
  
mongoose.model('LetturaCasetta', LetturaCasettaSchema, 'LetturaCasetta');