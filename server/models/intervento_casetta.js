// tengo rilevamenti e manutenzioni nella stessa collection
// filtro poi con query params per recuperarli

var CONFIG = require(process.cwd() + '/server/config.js'),
    Mail = require(process.cwd() + '/server/routes/mail.js'),
    moment = require('moment'),
    mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    InterventoCasettaSchema = new Schema({
        data: {
            type: Date,
            default: Date.now
        },
        casetta: {
            type: Schema.ObjectId, // ref a casetta
            ref: 'Casetta'
        },
        casetta_descrizione: {
            type: String
        },
        tipo: {
            type: String
        },
        note: { // comune
            type: String
        },             
        
        /* del rilevamento  ----------- */
        
        ambiente: {
            type: Number
        },
        ambiente_non_rilevato: {
            type: Boolean,
            default: false
        },
        fredda: {
            type: Number
        },
        fredda_non_rilevato: {
            type: Boolean,
            default: false
        },
        gasata: {
            type: Number
        },
        gasata_non_rilevato: {
            type: Boolean,
            default: false
        },
        ingresso_casetta: {
            type: Number
        },
        ingresso_casetta_non_rilevato: {
            type: Boolean,
            default: false
        },
        ingresso_acquedotto: {
            type: Number
        },
        ingresso_acquedotto_non_rilevato: {
            type: Boolean,
            default: false
        },
        litri_sostituzione_filtro: { // litri dalla sostituzione filtro
            type: Number,
            default: 0
        },
        litri_sostituzione_filtro_non_rilevato: {
            type: Boolean,
            default: false
        },
        lampada_uv: { // gg rimanenti
            type: Number
        },
        lampada_uv_non_rilevato: {
            type: Boolean,
            default: false
        },        
        /* della manutenzione ---------*/
        
        sostituzione_filtro: {
            type: Boolean
        },
        sostituzione_filtro_litri: { // arriva dal rilevamento quando spunto sostituzione filtro prima dell'azzeramento (tengo traccia dei litri alla quale è avvenuto)
            type: Number
        },
        sostituzione_filtro_note: {
            type: String
        },
        pulizia: {
            type: Boolean
        },
        pulizia_note: {
            type: String
        },  
        sostituzione_pulsanti: {
            type: Boolean
        },
        sostituzione_pulsanti_note: {
            type: String
        }, 
        disinfestazione: {
            type: Boolean
        },
        disinfestazione_note: {
            type: String
        },
        sanificazione: {
            type: Boolean
        },
        sanificazione_note: {
            type: String
        }, 
        sostituzione_lampade: {
            type: Boolean
        },
        sostituzione_lampade_giorni: { // mi segno i giorni rimanenti al quale è avvenuta la sostituzione lampade
            type: Number
        },
        sostituzione_lampade_note: {
            type: String
        }, 
        bombole_sostituite: {
            type: Number
        },       
        bombole_rimosse: {
            type: Number
        },
        durata_intervento: {
            type: Number
        }, 
         /* fine della manutenzione ---------*/
        
        user_id: { // per le ricerche
            type: String
        },
        user: { // chi l'ha modificato (tutto l'oggetto)
            type: Schema.Types.Mixed
        }
    });


InterventoCasettaSchema.index({
    user_id: 1,
    tipo:1,
    data:1,
    casetta:1,
    sostituzione_lampade: 1,
    sostituzione_filtro:1,
    pulizia:1
});

mongoose.model('InterventoCasetta', InterventoCasettaSchema, 'InterventoCasetta');



InterventoCasettaSchema.post('save', function(intervento,next){

    var Intervento = mongoose.model('InterventoCasetta');

    if(intervento.tipo === 'manutenzione' && intervento.sostituzione_filtro || intervento.sostituzione_lampade) {


            // trovo l'ultimo rilevamento
            Intervento.find({casetta: intervento.casetta, tipo:'rilevamento'}, function(err, result) {

                if(err)
                    return next(err)

                var rilevamento = result[0];

                // quando salvo una manutenzione con sosituzione filtro  devo azzerare il relativo campo nell'ultimo rilevamento di quella casetta
                /* non azzero più!
                if(intervento.sostituzione_filtro ) { 
                    // riparto da zero
                    Intervento.findOneAndUpdate({
                        _id: rilevamento._id
                    }, { $set:{ litri_sostituzione_filtro : 0 } }, function (err, intervento) {
                        if (err) {
                            return next(err);
                        } else {
                            next();
                        }
                    });
                }
                */

                // quando salvo una manutenzione con sostituzione lampade devo riportare a 0 i gg nel rilevamento
                if(intervento.sostituzione_lampade ) { 
                    // riparto da zero
                    Intervento.findOneAndUpdate({
                        _id: rilevamento._id
                    }, { $set:{ lampada_uv : 0 } }, function (err, intervento) {
                        if (err) {
                            return next(err);
                        } else {
                            next();
                        }
                    });
                }
                else {
                    next()
                }

            })
            .sort({data:-1})
            .limit(1);
        
    }
    // se supero i valori critici mando una mail
    if(intervento.tipo === 'rilevamento') {
    // if(intervento.lampada_uv && intervento.lampada_uv <= 20 || intervento.litri_sostituzione_filtro && intervento.litri_sostituzione_filtro >= 40000) {

         // trovo l'ultimo rilevamento
        Intervento.find({casetta: intervento.casetta, tipo:'rilevamento'}, function(err, result) {
            if(err)
                return next(err)

            var rilevamentoPrecedente = result[1] || { ingresso_casetta: 0, lampada_uv: 0};


            var soglia = intervento.ingresso_casetta - rilevamentoPrecedente.ingresso_casetta;

            if (soglia >= 38 || intervento.lampada_uv && intervento.lampada_uv >= 300) { // (in metri cubi...)
                var _sistemaInterventoPerMail = function (intervento) {
                    var data = {
                        data_rilevamento : moment(new Date(intervento.data)).format('DD/MM/YY - HH:mm'),
                        idcasetta: intervento.casetta,
                        valori_critici: []
                    }
                    // mostro solo i dati critici nella mail
                    if(soglia >= 38)
                        data.valori_critici.push({etichetta: 'Sostituzione filtro (mc)', valore: soglia })
                    if(intervento.lampada_uv >= 300)
                        data.valori_critici.push({etichetta: 'Lampada UV (gg trascorsi)', valore: intervento.lampada_uv})

                    return data;
                }

                var type = 'casette-superamento-valori-critici',
                    data = _sistemaInterventoPerMail(intervento),
                    subject = 'Richiesta intervento manutentivo per la casetta di '+intervento.casetta_descrizione,
                    from = {
                        name:  CONFIG.EMAIL_FROM_NAME,
                        email: CONFIG.EMAIL_FROM
                    }
                    to = CONFIG.EMAIL_RESPONABILI_CASETTE;
                
                Mail.send(type,data,subject,from,to,null,null,null,
                    function(err, mail) {
                        if(err) {
                            logger.error('Non sono riuscito a inviare la mail in InterventoCasetta',err)
                            next(err)
                        }
                        else
                            next();
                    }
                )
            } else {
                next() // niente da notificare
            }


        })
        .sort({data:-1})
        .limit(2);

    }
    else {
      next()
    }
});


// viceversa se cancello la manutenzione dopo l'inserimento devo ripristinare i litri del rilevamento
InterventoCasettaSchema.post('remove', function(intervento,next) {


    if(intervento.tipo === 'manutenzione' && intervento.sostituzione_filtro || intervento.sostituzione_lampade) { 

        var Intervento = mongoose.model('InterventoCasetta'),
            litri = intervento.sostituzione_filtro_litri, // mi segno i litri
            giorni = intervento.sostituzione_lampade_giorni; // mi segno i giorni


        // trovo l'ultimo rilevamento
        Intervento.find({casetta: intervento.casetta, tipo:'rilevamento'}, function(err, result) {

            if(err)
                return next(err)

            var rilevamento = result[0];

            if(intervento.sostituzione_filtro ) {
                // riporto i litri di prima
                Intervento.findOneAndUpdate({
                    _id: rilevamento._id
                }, { $set:{ litri_sostituzione_filtro : litri } }, function (err, intervento) {
                    if (err) {
                        return next(err);
                    } else {
                        next();
                    }
                });
            }

            if(intervento.sostituzione_lampade ) {
                // riporto i giorni di prima
                Intervento.findOneAndUpdate({
                    _id: rilevamento._id
                }, { $set:{ lampada_uv : giorni } }, function (err, intervento) {
                    if (err) {
                        return next(err);
                    } else {
                        next();
                    }
                });
            }

        })
        .sort({data:-1})
        .limit(1);

    }
    else {
        next();
    }
    
});