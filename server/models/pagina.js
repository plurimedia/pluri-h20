var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    PaginaSchema = new Schema({
        mdate: {
            type: Date,
            default: Date.now
        },
        visibile: {
            type: Boolean,
        },
        slug: {
            type: String,
            unique: true,
            required: true
        },
        titolo: {
            type: String,
            required: true
        },
        descrizione: {
            type: String,
            required: true
        },
        contenuti: {
        	type: Schema.Types.Mixed
        },
        user: { // utente che ha modificato
            type: Schema.Types.Mixed
        }
    });


PaginaSchema.index({slug:1});
PaginaSchema.index({titolo:1});
PaginaSchema.index({visibile:1});
mongoose.model('Pagina', PaginaSchema, 'Pagina');
