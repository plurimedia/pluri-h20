var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    LogSchema = new Schema({
        mdate: {
            type: Date,
            default: Date.now
        },
        title: {
            type: String
        },
        status : { 
            type: Number // 10 = info, 20 = success, 30 = warning, 40 = danger
        },
        type: { 
            type: Number, // vedi tabella di decodifica
            required: true
        },
        data: { // tutti i dati del log
            type: Schema.Types.Mixed
        }
    },{ capped : true, size : 1024 });


LogSchema.index({type:1});
LogSchema.index({mdate:1});
mongoose.model('Log', LogSchema, 'Log');
