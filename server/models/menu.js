var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    MenuSchema = new Schema({
        mdate: {
            type: Date,
            default: Date.now
        },
        titolo: {
            type: String,
            required: true
        },
        voci: [Schema.Types.Mixed],
        user: { // utente che ha modificato
            type: Schema.Types.Mixed
        }
    });

MenuSchema.index({titolo:1});
mongoose.model('Menu', MenuSchema, 'Menu');
