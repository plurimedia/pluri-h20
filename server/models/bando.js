var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    BandoSchema = new Schema({
        mdate: {
            type: Date,
            default: Date.now
        },
        titolo: {
            type: String,
            required: true
        },
        codice: {
            type: String
        },
        visibile: {
            type: Boolean,
        },
        data_guce: {
            type: Date
        },
        data_pubblicazione: {
            type: Date
        },
        data_scadenza: {
            type: Date
        },
        anno: {
            type: String
        },
        tipologia: {
            type: String
        },
        referente: {
            type: String
        },
        testo: {
            type: String
        },
        files: {
            type: Schema.Types.Mixed
        },
        user: { // utente che ha modificato
            type: Schema.Types.Mixed
        }
    });

BandoSchema.index({ titolo: 'text', codice: 'text'},{ default_language: "italian" });
BandoSchema.index({anno:1,visibile:1});

mongoose.model('Bando', BandoSchema, 'Bando');
