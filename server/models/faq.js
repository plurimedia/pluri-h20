var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    FaqSchema = new Schema({
        mdate: {
            type: Date,
            default: Date.now
        },
        domanda: {
            type: String,
            required: true
        },
        risposta: {
            type: String,
            required: true
        },
        settore: {
            type: String,
            required: true
        },
        visibile: {
            type: Boolean
        },
        user: { // utente che ha modificato
            type: Schema.Types.Mixed
        }
    });

mongoose.model('Faq', FaqSchema, 'Faq');
