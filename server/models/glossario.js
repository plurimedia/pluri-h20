var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    GlossarioSchema = new Schema({
        mdate: {
            type: Date,
            default: Date.now
        },
        termine: {
            type: String,
            required: true
        },
        descrizione: {
            type: String,
            required: true
        },
        visibile: {
            type: Boolean
        },
        user: { // utente che ha modificato
            type: Schema.Types.Mixed
        }
    });

mongoose.model('Glossario', GlossarioSchema, 'Glossario');
