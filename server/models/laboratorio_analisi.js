var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    LaboratorioAnalisiSchema = new Schema({
        analisi: [{
            type: Schema.Types.Mixed   
        }],
        prelievi: [{
            type: Schema.Types.Mixed   
        }],
        parametri: [{
            type: Schema.Types.Mixed   
        }],
        cdate: {
            type: Date,
            default: Date.now
        },
        mdate: {
            type: Date,
            default: Date.now
        },
        user: {
            type: Schema.Types.Mixed
        },
        visibile: {
            type: Boolean
        },
        sospendi: {
            type: Boolean,
            default: false
        },
        aNorma: {
            type: Boolean
        }
    });

LaboratorioAnalisiSchema.index({visibile: 1 });
LaboratorioAnalisiSchema.index({cdate: -1 });

mongoose.model('LaboratorioAnalisi', LaboratorioAnalisiSchema, 'LaboratorioAnalisi');
