/* Modello dati che ospita ii video caricati su vimeo */

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    VideoCasettaSchema = new Schema({
        mdate: { // data import
            type: Date,
            default: Date.now
        },
        id:{ // vimeo id
			type: String,
			required: true
        },
        name: { // nme del video su vimeo
            type: String
        },
        picture: { // anteprima
            type: String
        },
        s3_url: { // url su s3
			type: String
        }
    });

VideoCasettaSchema.index({name:1});
mongoose.model('VideoCasetta', VideoCasettaSchema, 'VideoCasetta');