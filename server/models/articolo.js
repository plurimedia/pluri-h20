var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ArticoloSchema = new Schema({
        idate: {
            type: Date,
            default: Date.now
        },
        mdate: {
            type: Date,
            default: Date.now
        },
        autore: { // utente che ha creato
            type: Schema.Types.Mixed
        },
        revisore: { // utente che ha modificato/revisionato
            type: Schema.Types.Mixed
        },
        revisore_id: { // per le ricerche
            type: String
        },
        titolo: {
            type: String,
            required: true
        },
        tag: {
            type: String
        },
        stato: {
            type: String
        },
        sezione: {
            type: String
        },
        testo: {
            type: String
        },
        estratto: {
            type: String
        },
        mostra_autore: {
             type: Boolean
        },
        files: {
            type: Schema.Types.Mixed
        },
        copertina: {
            type: Schema.Types.Mixed
        },
        immagini: {
            type: Schema.Types.Mixed
        },
        anno: {
            type: String
        },
        data: {
            type: Date
        }
    });

ArticoloSchema.index({ titolo: 'text'});
ArticoloSchema.index({ mdate: -1 });

mongoose.model('Articolo', ArticoloSchema, 'Articolo');
