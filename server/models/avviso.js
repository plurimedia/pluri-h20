var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    AvvisoSchema = new Schema({ 
        descrizione: {
            type: String
        },
        sezione: {
            type: String
        },
        mdate: {
            type: Date,
            default: Date.now
        },
        user: { // utente che ha modificato
            type: Schema.Types.Mixed
        },
        visibile: {
            type: Boolean,
        }
    });

mongoose.model('Avviso', AvvisoSchema, 'Avviso');
