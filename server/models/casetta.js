var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    CasettaSchema = new Schema({
        mdate: {
            type: Date,
            default: Date.now
        },
        visibile: {
            type: Boolean,
        },
        data_avviamento: {
            type: Date
        },
        tipo: {
            type: String
        },//erogatore o casetta o distributore
        comune: {
            type: String,
            required: true
        },
        indirizzo: {
            type: String
        },
        descrizione: {
            type: String
        },        
        coordinate: {
            lat: {
                type: Number
            },
            long: {
                type:  Number
            }
        },
        stato: {
            type: String,
            required: true
        },
        testo: {
            type: String
        },
        totem: {
            type: String
        },
        tipovideo: { // tipo di video da mostrare sullo shcermo (10=video/20=html/30=entrambi)
            type: Number
        },
        video: {
            type: String // url del video su s3
        },
        ultima_chiamata_video: {
            type: Date
        },
        alert: {
            type: String // eventuale msg da mostrare nella pagina analisi
        },
        copertina: {
            type: Schema.Types.Mixed
        },
        files: {
            type: Schema.Types.Mixed
        },
        immagini: {
            type: Schema.Types.Mixed
        },
        user: { // chi l'ha modificato
            type: Schema.Types.Mixed
        },
        codiceSif: {
            type: String
        },
        ip: { // ip ultima chiamata video
            type: String
        },
        monitor: { // la casetta è dotata do monitor?
            type: Boolean,
        },
        monitor_desc: { 
            type: String
        },
        videocamera: {// la casetta è dotata di videocamera?
            type: Boolean,
        },
        videocamera_desc: { 
            type: String
        },
        iot: {// la casetta ha la cassettina installata ?
            type: Boolean
        },
        mac_address: { 
            type: String
        },
        sim_codice: { 
            type: String
        },
        sim_pin: { 
            type: String
        },
        sim_numero: { 
            type: String
        },
        chiavetta: { 
            type: String
        },
        hdmi: { 
            type: String
        },
        ciabatta: { 
            type: String
        },
        trasformatore: { 
            type: String
        },
        tw_id: { 
            type: String
        },
        tw_pwd: { 
            type: String
        },
        tw_version: { 
            type: String
        },
        multimedia_note: { 
            type: String
        },
        data_inaugurazione: {
            type: Date
        },
        data_scadenza_contr: {
            type: Date
        },
        tenant: { 
            type: String
        }
    });


mongoose.model('Casetta', CasettaSchema, 'Casetta');
CasettaSchema.index({descrizione:1,visibile:1});