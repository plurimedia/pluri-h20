var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    NewsletterSchema = new Schema({
        idate: {
            type: Date,
            default: Date.now
        },
        mdate: {
            type: Date,
            default: Date.now
        },
        anno: { //anno di creazione
            type: String
        },
        dataInvio: {
            type: Date
        },
        dataTest: {
            type: Date
        },
        mailTest: 
        {
            type: String
        },
        titolo: {
            type: String,
            required: true
        }, 
        target: {
            type: Schema.Types.Mixed
        },
        target_id: {//per le ricerche
            type: String,
        },
        periodo: {
            type: String,
            required: true
        },
        stato: {
            type: String,
            required: true
        },
        articoli: {
            type: [Schema.Types.Mixed]
        },
        comunicati: {
            type: [Schema.Types.Mixed]
        },
        eventi: {
            type: [Schema.Types.Mixed]
        },
        user: { // utente che ha modificato
            type: Schema.Types.Mixed
        },
        notizie: {
            type: [Schema.Types.Mixed]
        }
    });

mongoose.model('Newsletter', NewsletterSchema, 'Newsletter');
