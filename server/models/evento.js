var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    EventoSchema = new Schema({
        mdate: {
            type: Date,
            default: Date.now
        },
        visibile: {
            type: Boolean,
        },
        anno: {
            type: String
        },
        titolo: {
            type: String,
            required: true
        },
        sottotitolo: {
            type: String
        },
        da: {
            type: Date
        },
        a: {
            type: Date
        },
        dove: {
            type: String
        },
        categoria: {
            type: String
        },
        testo: {
            type: String
        },
        prezzi: {
            type: String
        },
        orari: {
            type: String
        },
        organizzatore: {
            type: String
        },
        destinatari: {
            type: String
        },
        riconoscimenti: {
            type: String
        },
        informazioni: {
            type: String
        },
        files: {
            type: Schema.Types.Mixed
        },
        copertina: {
            type: Schema.Types.Mixed,
            required: true
        },
        immagini: {
            type: Schema.Types.Mixed
        },
        user: { // utente che ha modificato
            type: Schema.Types.Mixed
        },
        tag: {
            type: String
        }
    });


EventoSchema.index({ titolo: 'text'},{ default_language: "italian" });
EventoSchema.index({anno:1, visibile:1});

mongoose.model('Evento', EventoSchema, 'Evento');
