var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    SportelloSchema = new Schema({
        mdate: {
            type: Date,
            default: Date.now
        },
        comune: {
            type: String
        },
        comuneDesc: {
            type: String
        },
        indirizzo: {
            type: String
        },
        indirizzoCompleto: {
            type: String
        },
        noteIndirizzo: {
            type: String
        },
        orario: {
            type: String
        },
        descrizione: {
            type: String
        },
        avviso: {
            type: String
        }, 
        visibile: {
            type: Boolean
        },
        servizioLis: {
            type: Boolean
        },
        coordinate: {
            lat: {
                type: Number
            },
            long: {
                type: Number
            }
        },
        telefono: {
            type: String
        },
        copertina: {
            type: Schema.Types.Mixed
        },
        immagini: {
            type: Schema.Types.Mixed
        },
        user: { // utente che ha modificato
            type: Schema.Types.Mixed
        }
    });

mongoose.model('Sportello', SportelloSchema, 'Sportello');
