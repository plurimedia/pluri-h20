var mongoose = require('mongoose'),
    Log = mongoose.model('Log'),
    _ = require('underscore'),
    codes = require(process.cwd() + '/server/logcodes.js');


// lista dei logs
exports.query = function (req, res, next) {

    var query = {};

    if (req.query.stato)
        _.extend(query,{status: req.query.stato})

    if (req.query.tipo)
        _.extend(query,{type: req.query.tipo})

    Log.find(query, function (err, logs) {

        if (err)
            return next(err);

        res.status(200).send(logs);

    })
    .sort({mdate: -1})
    .limit(30)
    .lean();
}

exports.codes = function (req, res, next) {
	res.status(200).send(codes)
}