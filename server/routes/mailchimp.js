var mongoose = require('mongoose'), 
    Newsletter = mongoose.model('Newsletter'),
    Util = require(process.cwd() + '/server/routes/util.js'),
    CONFIG = require(process.cwd() + '/server/config.js'),
    Mailchimp = require('mailchimp-api-v3'),
    juice = require('juice'), // mette gli stili in linea in un html
    async = require('async'),
    fs = require('fs'),
    Mustache = require('mustache'),
     _ = require('underscore'),
    Tcontent = fs.readFileSync(process.cwd() + '/server/templates/email/newsletter.html', 'utf8'),
    Tcss = fs.readFileSync(process.cwd() + '/server/templates/email/style.css', 'utf8');
     
 
exports.list = function (req, res, next) 
{   
    res.status(200).send(CONFIG.MAILCHIMP_LIST); 
}


_manageCampaign  = function (newsLetterContent, lista, cb) {  

    var mailchimp = new Mailchimp(CONFIG.MAILCHIMP_API_KEY);
    var idCampaign; 

    async.series([
        function (callback) { 

            console.log("1. creazione campagna ", lista.name, " ", lista.id)

            mailchimp.post(
                {
                    path : '/campaigns',
                    body : 
                    {
                        type : 'regular',
                        recipients :
                        {
                            list_id : lista.id
                        },
                        settings : 
                        {
                            subject_line : newsLetterContent.subject,
                            from_name : newsLetterContent.from.name,
                            reply_to  : newsLetterContent.from.reply_to
                        }
                    }
                },
                function (err, results) 
                {
                    if (err){
                        console.log(err)
                        callback(err, 'errore');
                    } else
                    {
                        idCampaign = results.id;
                        callback(null);
                    }
                }
            )
        },
        function (callback, results) {

             console.log("2.    invio  template ", lista.name)


             html=juice(
                Mustache.render(Tcontent, 
                {
                    content: newsLetterContent 
                })
            )
            mailchimp.put(
                {
                    path : '/campaigns/'+idCampaign+'/content',
                    body : 
                    {
                        html : html
                    }
                }, 
                function (err, template) 
                {
                    if (err){ 
                        callback(err, 'errore');
                    }
                    else{
                        callback(null);
                    }
                }
            )
        },
        function (callback) {

            console.log("3.    invio  email ", lista.name, " idCampaign ", idCampaign)

            mailchimp.post(
                {
                    path : '/campaigns/'+idCampaign+'/actions/send'
                }, 
                function (err, resSend) 
                {
                    if (err) {
                        console.log(err)
                        return callback(err); 
                    }
                    else{ 
                        console.log("creazione campagna success ", lista.name)
                        callback(null, resSend);
                    }
                }
            )
        }
    ],
    function (err, finalResult) { 
        if (err)
           return cb(err);
        else
            return cb(null, finalResult);

    });
}


exports.newcampaign = function (req, res, next) {

    Util.getNewsLetterContent(req.body._id,  
        function(err, newsLetterContent) 
        {
            if (err)
                callback(err, 'errore');
            else{

                //recupero le liste in base al target_id salvato in DB 
                list = _.filter(CONFIG.MAILCHIMP_LIST, 
                    function(lista){
                        return lista.type === newsLetterContent.target_id;
                    }
                );

                console.log("list ", list)
                 
                //per ogni lista creo una campagna
                async.each(list, 
                    function(lista,callback)
                    {
                        _manageCampaign(newsLetterContent,lista,
                           function(err, result) 
                            {
                                if(err)
                                    return next(err); 
                                else{ 
                                    callback();
                                }
                            }
                        )
                    },
                    function(err)
                    {
                        if(err)
                            return next(err); 
                        else
                            res.status(200).send();
                    }
                )
                
            }
        } 
    )
} 

 
//not use
_getImageTarget = function (target) 
{
    if (target == 'Acquedotto')
        return '';
    else if (target == 'Ambiente')
        return '';
    else if (target == 'Azienda')
        return '';
    else if (target == 'Comunicati')
        return '';
    else if (target == 'Corsi')
        return '';
    else if (target == 'Depurazione')
        return '';
    else if (target == 'Dipendenti')
        return '';
    else if (target == 'Educazione')
        return '';
    else if (target == 'Eventi')
        return '';
    else if (target == 'Fognatura')
        return '';
    else if (target == 'Gli uffici si presentano')
        return '';
    else if (target == 'Istituzionale')
        return '';
    else if (target == 'Laboratori')
        return ''
    else if (target == 'Progettazione')
        return ''
    else if (target == 'Sociale')
        return ''
    else if (target == 'Sostenibilità')
        return ''
  }
