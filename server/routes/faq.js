var mongoose = require('mongoose'),
    Faq = mongoose.model('Faq'),
    _ = require('underscore');
  
exports.save = function (req, res, next) 
{
    var faq = new Faq(req.body);
    faq.mdate = new Date(); 
    faq.user = req.user;
    faq.save(
        function (err, result) 
        {
            if (err)
                return next(err);
            else
                res.status(200).send(result);
        }
    )
}
 
exports.query = function (req, res, next) 
{
    var query  = {};
    if(req.query.domanda)
        _.extend(query,{domanda:  {$regex: req.query.domanda,  $options: 'i'}  })

     if(req.query.settore)
        _.extend(query,{settore: req.query.settore})
    Faq.find(query, function (err, result) 
    {
        if (err)
            return next(err);
        res.status(200).send(result);
    }).sort({domanda: 1});
} 
 
exports.get = function (req, res, next) 
{
    Faq.findOne(
    {
        _id: req.params.id
    }, 
    function (err, result) 
    {
        if (err)
            return next(err);
        else
            res.status(200).send(result);
    })
} 
 
exports.update = function (req, res, next) 
{
    var Mfaq = req.body;
    Mfaq.mdate = new Date();
    Mfaq.user = req.user; 
    Faq.findById(req.params.id, 
        function (err, faq) 
        {
            faq.update(Mfaq, 
            {
                runValidators: true
            }, 
            function (err) {
                if (err)
                    return next(err);
                else
                    res.status(200).send(Mfaq);
            }
        )
    });
}
 
exports.delete = function (req, res, next) 
{
    Faq.remove(
        {
            _id: req.params.id
        }, 
        function (err) 
        {
            if (err)
                return next(err);
            else
                res.status(200).end();
        }
    )
}

exports.count = function (req, res, next) 
{
    Faq.count({}, 
        function( err, count)
        {
            if (err)
                return next(err);
            else
                res.status(200).send({total: count});        
        }
    )
}