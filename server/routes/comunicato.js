var mongoose = require('mongoose'),
    Comunicato = mongoose.model('Comunicato'),
    _ = require('underscore'),
    moment = require('moment'),
    copertina_default = {
        name: '',
        id: 'brianzacque_prod/settings/placeholder' // immagine di default su cloudinary
    };


// salvataggio comunicato
exports.save = function (req, res, next) {
    var comunicato = new Comunicato(req.body);
    comunicato.mdate = new Date();
    comunicato.user = req.user;
    comunicato.anno = moment(req.body.data).format('YYYY'); // salvo l'anno del comunicato come stringa
    
    if(!comunicato.copertina)
        comunicato.copertina = copertina_default; // bruttino sarebbe meglio coi validatori ma in update non vanno :(

    comunicato.save(function (err, result) {
        if (err)
            return next(err);
        else
            res.status(200).send(result);
    });
}

// lista dei comunicati
exports.query = function (req, res, next) {
    var query = {};

   
    if (req.query.testo) // sto cercando per titolo o codice
        _.extend(query, {
        $text: {
            $search: req.query.testo
        }
    });

    if (req.query.anno)
        _.extend(query, {
            anno: req.query.anno
        });

    if (req.query.visible)
    _.extend(query, {
        visible: req.query.visible
    });
     
    var limit =0;
    if (req.query.limit)
     {
        limit=parseInt(req.query.limit);
    }
    Comunicato.find(query, function (err, result) {

        if (err)
            return next(err);

        res.status(200).send(result);

    }).sort({
        mdate: -1
    })
    .limit(limit);
}

// get singolo comunicato
exports.get = function (req, res, next) {
    Comunicato.findOne({
        _id: req.params.id
    }, function (err, result) {
        if (err)
            return next(err);
        else
            res.status(200).send(result);
    });
}


// update singolo comunicato
exports.update = function (req, res, next) {

    var Mcomunicato = req.body;
    Mcomunicato.mdate = new Date();
    Mcomunicato.user = req.user;
    Mcomunicato.anno = moment(req.body.data).format('YYYY'); // salvo l'anno del comunicato come stringa

    if(!Mcomunicato.copertina)
        Mcomunicato.copertina = copertina_default; // bruttino sarebbe meglio coi validatori ma in update non vanno :(

    Comunicato.findOneAndUpdate({
        _id: req.params.id
    }, Mcomunicato, function (err, comunicato) {
        if (err) {
            return next(err);
        } else {
            res.status(200).send(Mcomunicato);
        }
    });
}

// delete singolo comunicato
exports.delete = function (req, res, next) {
    Comunicato.remove({
        _id: req.params.id
    }, function (err) {
        if (err)
            return next(err);
        else
            res.status(200).end();
    });
}

// count
exports.count = function (req, res, next) {
    Comunicato.count({}, function( err, count){
        if (err)
            return next(err);
        else
            res.status(200).send({total: count});        
    })
}