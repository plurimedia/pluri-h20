var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    LogSchema = require(process.cwd() + '/server/models/log'),
    Log = mongoose.model('Log'),
    mandrill = require('mandrill-api/mandrill'),
    mandrill_client = new mandrill.Mandrill(CONFIG.MANDRILL_API_KEY),
    juice = require('juice'), // mette gli stili in linea in un html
    fs = require('fs'),
    Mustache = require('mustache'),
    moment = require('moment'),
    _ = require('underscore'),
    Tcss = fs.readFileSync(process.cwd() + '/server/templates/email/style.css', 'utf8'),
    Theader = fs.readFileSync(process.cwd() + '/server/templates/email/header.html', 'utf8'), // header comune a tutte le mail
    Tfooter = fs.readFileSync(process.cwd() + '/server/templates/email/footer.html', 'utf8'), // footer comune a tutte le mail
    TCasetteValoriCritici = fs.readFileSync(process.cwd() + '/server/templates/email/casette-superamento-valori-critici.html', 'utf8'),
    TCasetteErroreVideo = fs.readFileSync(process.cwd() + '/server/templates/email/casette-segnalazione-errore-video.html', 'utf8'),
    TCasetteInvioTicket = fs.readFileSync(process.cwd() + '/server/templates/email/casette-invio-ticket.html', 'utf8'),
    TNewsletter = fs.readFileSync(process.cwd() + '/server/templates/email/newsletter.html', 'utf8'),
    TArticolonuovo = fs.readFileSync(process.cwd() + '/server/templates/email/articolo-nuovo.html', 'utf8'),

    // sistema i dentinatari col formato di mandrill
    _to = function (to,cc,ccn) {
        var toForMandrill = [],
        _formattaIndirizzi = function (indirizzi, tipo) {
            return _.map(indirizzi, function (ind) {
                return {
                    email: ind,
                    type: tipo
                }
            })
        };
        // mandrill li vuole in questo formato
        if (to)
            toForMandrill = _.union(toForMandrill, _formattaIndirizzi(to, 'to'));
        if (cc)
            toForMandrill = _.union(toForMandrill, _formattaIndirizzi(cc, 'cc'));
        if (ccn)
            toForMandrill = _.union(toForMandrill, _formattaIndirizzi(ccn, 'bcc'));

        return toForMandrill;
    },
    log = {
        mdate: new Date()
    };


/*
*   type: {String} tipo di mail che devo inviare, sceglie il template
*   data: {Object} dati da usare nel template
*   subject: {String} oggetto della mail
*   from: {Object} mittente della mail ({name:'', email:''})
*   to: {String} destinatario della mail
*   cc: {String} cc della mail
*   ccn: {String} ccn della mail
*   reply_to {String} eventuale reply to 
 */

exports.send = function (type,data,subject,from,to,cc,ccn,reply_to,callback) {


    try {
        // scegli i template giusti
        switch(type){

            case 'casette-superamento-valori-critici': 
            Tcontent = TCasetteValoriCritici;
            log.type = 101;
            break;

            case 'casette-invio-ticket': 
            Tcontent = TCasetteInvioTicket;
            log.type = 103;
            break;

            case 'casette-segnalazione-errore-video': 
            Tcontent = TCasetteErroreVideo;
            log.type = 102;
            break;     

            case 'newsletter_testEmail': 
            Tcontent = TNewsletter;
            Theader  = '';
            Tfooter  = ''
            log.type = 105;
            break;     


            case 'articolo-nuovo': 
            Tcontent = TArticolonuovo;
            Theader  = Theader;
            Tfooter  = Tfooter
            log.type = 110;
            break;   

            default: 
            return; // fermati
        }


        var header = Mustache.render(Theader, { // precompilo con il css
                css: Tcss
            }),
            message = { // preparo il messaggio
                html: juice(Mustache.render(Tcontent, {
                    content: _.extend(data, {app_data: CONFIG.APP_DATA}), //dati inviati dal client: ci aggiungo alcune cose che mi servono (url dell'app ecc)
                    header: header, // comune
                    footer: Tfooter // comune
                })),
                subject: subject,
                from_email: from.email,
                from_name: from.name,
                to: _to(to,cc,ccn), // come li vuole mandrill
                headers: {
                    "Reply-To": reply_to ? reply_to : CONFIG.EMAIL_FROM
                },
                track_opens: 1,
                track_clicks: 1
            };

        // mando!
        mandrill_client.messages.send({
                message: message,
                async: false, // singola mail non serve un batch
            },
            function (result) {

                log.title = 'Invio mail "' +type+' " riuscito'
                log.status = 20
                log.data = {
                    result: result,
                    email: message
                };

                var l = new Log(log)

                l.save(function(err, result) {
                    if (err)
                        console.log(err)
                    callback(null,result)
                });                


            },
            function (e) {

                console.log(e)
                // Mandrill returns the error as an object with name and message keys
                log.title = 'Errore invio mail "' +type+' "'
                log.status = 40
                log.data = {
                    error: e,
                    email: message
                };

                var l = new Log(log)
                
                l.save(function(err, result) {
                    if (err)
                        console.log(err)
                    callback(e)
                });     
            }
        );
    }
    catch(err) {
        console.log('impossibile inviare la mail', err)
    }
}


/* controlla lo stato di una mail */
exports.stato = function (req, res) {
    mandrill_client.messages.info({
            id: req.params.id
        },
        function (result) {
            res.status(200).send(result)
        },
        function (err) {
            res.status(200).send(err)
        }
    )
}
