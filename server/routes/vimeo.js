var CONFIG = require(process.cwd() + '/server/config.js'),
    _ = require('underscore'),
    Vimeo = require('vimeo').Vimeo,
    lib = new Vimeo(CONFIG.VIMEO_CLIENT_ID, CONFIG.VIMEO_CLIENT_SECRET, CONFIG.VIMEO_ACCESS_TOKEN);

/*
* documentazione: https://developer.vimeo.com/api/endpoints/
*/

exports.client = lib;

/*
* recupero i video caricati 
*/
exports.query = function(req, res, next) {

    lib.request({
        method: 'GET',
        path: '/me/videos',
    }, function(error, body, status_code, headers) {
        if (error) {
            return next(error)
        } else {
            res.status(200).send(body.data)
        }
    });   
}



/*
* recupero dati sul singolo video
*/
exports.get = function(req, res, next) {

    lib.request({
        method: 'GET',
        path: '/me/videos/'+req.params.id,
    }, function(error, body, status_code, headers) {
        if (error) {
            return next(error)
        } else {

            console.log('video',body)
            res.status(200).send(body)
        }
    });   
}




