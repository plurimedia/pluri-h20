var mongoose = require('mongoose'),
    LaboratorioRegola = mongoose.model('LaboratorioRegola');

exports.save = function (req, res, next) {
    var lRegola = new LaboratorioRegola(req.body);
    lRegola.mdate = new Date();
    lRegola.user = req.user;
 
    lRegola.save(function (err, result) {
        if (err)
            return next(err);
        else
            res.status(200).send(result);
    });
}

exports.listaRegole = function (req, res, next) {
    LaboratorioRegola.findOne({
        //_id: req.params.id
    }, function (err, result) { 
        if (err)
            return next(err);
        else
            res.status(200).send(result);
    });
}

exports.update = function (req, res, next) {
    var LRegola = req.body;
    LRegola.mdate = new Date();
    LRegola.user = req.user;


    console.log(LRegola)
 
    LaboratorioRegola.findById(req.params.id, function (err, regola) {
        regola.update(LRegola, function (err) {
            if (err) {
                return next(err);
            } else {
                res.status(200).end();
            }
        })
    });
}