var mongoose = require('mongoose'),
    Esito = mongoose.model('Esito'),
    _ = require('underscore'),
    moment = require('moment');

// salvataggio esito
exports.save = function (req, res, next) {
    var esito = new Esito(req.body);
    esito.mdate = new Date();
    esito.user = req.user;
    esito.anno = moment(req.body.data_aggiudicazione).format('YYYY'); // salvo l'anno del bando come stringa

    esito.save(function (err, result) {
        if (err)
            return next(err);
        else
            res.status(200).send(result);
    });
}

// lista degli esiti
exports.query = function (req, res, next) {
    var query = {};

    if (req.query.testo) // sto cercando per titolo o codice
        _.extend(query,  {$text: { $search: req.query.testo}});
    
    if(req.query.anno)
        _.extend(query, {anno:req.query.anno});

    Esito.find(query, function (err, result) {

        if (err)
            return next(err);

        res.status(200).send(result);

    }).sort({data_aggiudicazione: -1});
}

    // get singolo esito (devo popolare anche il bando collegato se c'è)
exports.get = function (req, res, next) {
    Esito.findOne({
            _id: req.params.id
        })
        .populate('bando', 'titolo')
        .exec(function (err, result) {
            if (err)
                return next(err);
            else
                res.status(200).send(result);
        });
}


// update singolo esito
exports.update = function (req, res, next) {

    var Mesito = req.body;
    Mesito.mdate = new Date();
    Mesito.user = req.user;
    Mesito.anno = moment(req.body.data_aggiudicazione).format('YYYY'); // salvo l'anno del bando come stringa

    Esito.findById(req.params.id, function (err, esito) {
        esito.update(Mesito, function (err) {
            if (err) {
                return next(err);
            } else {
                res.status(200).end();
            }
        })
    });
}

// delete singolo esito
exports.delete = function (req, res, next) {
    Esito.remove({
        _id: req.params.id
    }, function (err) {
        if (err)
            return next(err);
        else
            res.status(200).end();
    });
}

// count
exports.count = function (req, res, next) {
    Esito.count({}, function( err, count){
        if (err)
            return next(err);
        else
            res.status(200).send({total: count});        
    })
}
