var mongoose = require('mongoose'),
    CONFIG = require(process.cwd()+'/server/config.js'),
    Articolo = mongoose.model('Articolo'),
    Mail = require(process.cwd() + '/server/routes/mail.js'),
    _ = require('underscore'),
    moment = require('moment');

_userIsAutore = function (roles){

    autore = _.filter(roles, function (r) {
    return (r === 'autore')
    });

    if (autore.length>0)
        return true;
    else 
        return false;
 
}
exports.save = function (req, res, next) 
{ 

    var articolo = new Articolo(req.body);
    articolo.mdate = new Date(); 
    articolo.idate = new Date(); 
    articolo.anno = moment(articolo.idate).format('YYYY'); // salvo l'anno di inserimento per facilitare le ricerche
    articolo.autore = req.user;
    articolo.revisore = req.user;
    articolo.revisore_id = req.user.sub;
    articolo.save(
        function (err, result) 
        {
            if (err)
                return next(err);
            else{ 

                if (result.stato=='In revisione' && 
                    _userIsAutore(req.user.app_metadata.roles)==true){

                    var type = 'articolo-nuovo',
                    data ={
                        titolo:articolo.titolo,
                        stato:articolo.stato
                    }
                    subject = 'Nuovo articolo',
                    from = {
                        name:  CONFIG.EMAIL_FROM_NAME,
                        email: CONFIG.EMAIL_FROM
                    }
                    to = CONFIG.EMAIL_REVISORI_ARTICOLI;
                
                    Mail.send(type,data,subject,from,to,null,null,null,
                        function(err, mail) {
                            if(err) {
                                console.log('Non sono riuscito a inviare la mail in nuovo articolo',err)
                                next(err)
                            }
                            else
                                res.status(200).send(result);
                        }
                    )

                }
                else{
                    res.status(200).send(result);
                }
            }
        }
    );
}
 
exports.query = function (req, res, next) 
{
    var query  = {};

     if (req.query.listaNewsletter)
     {
        query.stato = 'Pubblicato';
        if (req.query.sezione)
        query.sezione = { $in: [req.query.sezione,'Entrambi'] }
    }
    else
    {
        if (req.user.app_metadata.roles.indexOf('autore')>=0)
             query.revisore_id  = req.user.sub;

        if (req.user.app_metadata.roles.indexOf('revisore')>=0) 
             query.$or = [
                    {stato:{ $in: ['In revisione','Revisionato','Pubblicato'] }}, 
                    {stato:{ $in: ['Bozza'] }, revisore_id  : req.user.sub }
            ]

         if (req.query.titolo) // sto cercando per titolo o codice
           query.titolo = {$regex: req.query.titolo, $options: 'i'};

        if(req.query.anno)
            query.anno = req.query.anno;

        if(req.query.tag)
             query.tag = req.query.tag;

        if(req.query.sezione)
            query.sezione = req.query.sezione;

    }

    var limit =0;
    if (req.query.limit)
     {
        limit=parseInt(req.query.limit);
    }
     
    Articolo.find(query, 
        function (err, result) 
        {
            if (err)
                return next(err);
            res.status(200).send(result);
        }
    )
    .sort({mdate: -1})
    .limit(limit);
}


exports.get = function (req, res, next) 
{
    Articolo.findOne(
    {
        _id: req.params.id
    }, 
    function (err, result) 
    {
        if (err)
            return next(err);
        else
            res.status(200).send(result);
    });
} 
 
exports.update = function (req, res, next) 
{
    var Marticolo = req.body;
    Marticolo.mdate = new Date();
    Marticolo.revisore = req.user; 
    Articolo.findById(req.params.id, 
        function (err, articolo) 
        {
            articolo.update(Marticolo, 
            {
                runValidators: true
            }, 
            function (err) 
            {
                if (err)
                    return next(err);
                else{ 
                   
                    if (Marticolo.stato=='In revisione' && 
                    _userIsAutore(req.user.app_metadata.roles)==true){

                    var type = 'articolo-nuovo',
                    data ={
                        titolo:Marticolo.titolo,
                        stato:Marticolo.stato
                    }
                    subject = 'Nuovo articolo',
                    from = {
                        name:  CONFIG.EMAIL_FROM_NAME,
                        email: CONFIG.EMAIL_FROM
                    }
                    to = CONFIG.EMAIL_REVISORI_ARTICOLI;
                
                    Mail.send(type,data,subject,from,to,null,null,null,
                        function(err, mail) {
                            if(err) {
                                console.log('Non sono riuscito a inviare la mail in nuovo articolo',err)
                                next(err)
                            }
                            else
                                res.status(200).send(Marticolo);
                        }
                    )

                }
                else{
                     res.status(200).send(Marticolo);
                }

                }
            })
        }
    )
}
 
exports.delete = function (req, res, next) 
{
    Articolo.remove(
        {
            _id: req.params.id
        }, 
        function (err) 
        {
            if (err)
                return next(err);
            else
                res.status(200).end();
        }
    )
}
 
exports.count = function (req, res, next) 
{
    var query  = {};

    if (req.user.app_metadata.roles.indexOf('autore')>=0)
         query.revisore_id  = req.user.sub;

    if (req.user.app_metadata.roles.indexOf('revisore')>=0){  
         query.$or = [
                {stato:{ $in: ['In revisione','Revisionato','Pubblicato'] }}, 
                {stato:{ $in: ['Bozza'] }, revisore_id  : req.user.sub }
        ]
    }

    Articolo.count(query, 
        function( err, count)
        {
            if (err)
                return next(err);
            else{ 
                res.status(200).send({total: count});        
            }
        }
    )
}