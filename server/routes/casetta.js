var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    Casetta = mongoose.model('Casetta'),
    Log = mongoose.model('Log'),
    InterventoCasetta = mongoose.model('InterventoCasetta'),
    VideoCasetta = mongoose.model('VideoCasetta'),
    LaboratorioAnalisi = mongoose.model('LaboratorioAnalisi'),
    _ = require('underscore'),
    moment = require('moment'),
    numeral = require('numeral'),
    async = require('async'),
    fs = require('fs'),
    mustache = require('mustache'),
    pdf = require('html-pdf'),
    axios = require('axios'),

    copertina_default = {
        name: '',
        id: 'plurih2o/placeholder' //'brianzacque_prod/settings/placeholder' // immagine di default su cloudinary
    };
    Util = require(process.cwd() + '/server/routes/util.js'),

numeral.register('locale', 'it', {
    delimiters: {
        thousands: '.',
        decimal: ','
    },
    abbreviations: {
        thousand: 'k',
        million: 'm',
        billion: 'b',
        trillion: 't'
    },
    ordinal : function (number) {
        return number === 1 ? 'er' : 'ème';
    },
    currency: {
        symbol: '€'
    }
})
numeral.locale('it')



// salvataggio casetta
exports.save = function (req, res, next) {
    var casetta = new Casetta(req.body);
    casetta.mdate = new Date();
    casetta.user = req.user;

    if(!casetta.copertina)
        casetta.copertina = copertina_default; // bruttino sarebbe meglio coi validatori ma in update non vanno :(

    if(!casetta.tenant)
        casetta.tenant = Util.getTenantUser(req)

    casetta.save(function (err, result) {
        if (err)
            return next(err);
        else{
            //webhook con netlify sito  -- PLURITANK --
            axios.post('https://api.netlify.com/build_hooks/6194c2e706a47ce52f7f9d37')
            .then((risp) =>{
                console.log(risp)
                res.status(200).send(result);
            })
            .catch((err) => {
                console.log("deploy sul sito non riuscito")
                res.status(200).send(result);
            })
        }
            
    });
}

// lista delle casette
exports.query = function (req, res, next) {
    var query = Util.getQuery(req)

    Casetta.find(query, function (err, result) {
        if (err)
            return next(err);

        var casette = _.map(result, function(casetta){ // mando al client la configurazione sull'intervallo della richiesta della pagina video
            return _.extend(casetta, {intervallo_pagina: CONFIG.VIDEO_CASETTA_REFRESH_INTERVAL})
        })

        res.status(200).send(casette);

    })
    .sort({descrizione: 1})
    .lean();
}

// get singola casetta
exports.get = function (req, res, next) {
    Casetta.findOne({
        _id: req.params.id
    }, function (err, result) {
        if (err)
            return next(err);
        else {
            result.intervallo_pagina = CONFIG.VIDEO_CASETTA_REFRESH_INTERVAL
            res.status(200).send(result);
        }
    }).lean()
}


// update singola casetta
exports.update = function (req, res, next) {

    var Mcasetta = req.body;
    Mcasetta.mdate = new Date();
    Mcasetta.user = req.user;  
    
    if(!Mcasetta.copertina)
        Mcasetta.copertina = copertina_default; // bruttino sarebbe meglio coi validatori ma in update non vanno :(
            
    if(!Mcasetta.tenant)
        Mcasetta.tenant = Util.getTenantUser(req)

    Casetta.findOneAndUpdate({
        _id: req.params.id
    }, Mcasetta, function (err, casetta) {
        if (err) {
            return next(err);
        } else {
            //webhook con netlify sito  -- PLURITANK --
            axios.post('https://api.netlify.com/build_hooks/6194c2e706a47ce52f7f9d37')
            .then((risp) =>{
                console.log(risp)
                res.status(200).send(Mcasetta);
            })
            .catch((err) => {
                console.log("deploy sul sito non riuscito")
                res.status(200).send(Mcasetta);
            })
        }
    });
}

// delete singola casetta
exports.delete = function (req, res, next) {
    Casetta.remove({
        _id: req.params.id
    }, function (err) {
        if (err)
            return next(err);
        else{
            //webhook con netlify sito  -- PLURITANK --
            axios.post('https://api.netlify.com/build_hooks/6194c2e706a47ce52f7f9d37')
            .then((risp) =>{
                console.log(risp)
                res.status(200).end();
            })
            .catch((err) => {
                console.log("deploy sul sito non riuscito")
                res.status(200).end();
            })
        }
           
    });
}

// video disponibili per la casetta

exports.videos = function (req, res, next) {
    VideoCasetta.find({},function(err, result){
        if (err)
            return next(err);

        res.status(200).send(result);

    })
}

getIdVideo = function(text) {
    var str = text; //"es. https://d3aefyb3iylg50.cloudfront.net/266112408.mp4";
    var slash = str.lastIndexOf("/");
    var dot = str.lastIndexOf(".");
    var finalString = str.substr(slash+1, dot);
    return finalString;
}

// pagina video della casetta
exports.video = function (req, res, next) {
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) 
       return next("Casetta non trovata");

    var ipRichiesta = req.headers['x-forwarded-for'] || req.connection.remoteAddress

    Casetta.findOne({
        _id: req.params.id
    }, function (err, casetta) {
        
        if (err)
            return next(err);

        if(casetta) {
       casetta.mostravideo = casetta.mostrapagina = casetta.entrambi =false

        // per mustache
        if(casetta.tipovideo===10 || casetta.tipovideo===30)
            casetta.mostravideo = true
        if(casetta.tipovideo===20 || casetta.tipovideo===30)
            casetta.mostrapagina = true
        if(casetta.tipovideo===30)
            casetta.entrambi = true;

        casetta.durataPagina = CONFIG.VIDEO_CASETTA_PAGE_DURATION;
        casetta.intervalloRefresh = CONFIG.VIDEO_CASETTA_REFRESH_INTERVAL;
        casetta.data_avviamento_pretty = moment(casetta.data_avviamento).format('DD/MM/YYYY')

        /* recupero l'ultimo intervento: DUPLICO LA LOGICA API (migliorare se va in prod )*/
        
        InterventoCasetta.find({casetta: casetta._id, tipo:'rilevamento'}, function(err, rilevamenti){
            if(err)
                return next(err);

            var rilevamento = rilevamenti[0];


            // trovo le analisi
            LaboratorioAnalisi.findOne({visibile: true}, function (err, analisi) {

                if (err)
                    return next(err);
          
                if (analisi && !analisi.sospendi) {
                    casetta.analisi = _.find(analisi.analisi, function (a) {
                        return a.codiceSif === casetta.codiceSif
                    })
                }

                /* se c'è l'ultimo rilevamento sistemo per il FE aggiungo alcune proprietà derivate */

                if(rilevamento) {
                    var rilevamentoPretty = rilevamento;
                    rilevamentoPretty.data = moment(rilevamento).format('DD/MM/YYYY')
                    rilevamentoPretty.totale = rilevamento.ambiente + rilevamento.fredda + rilevamento.gasata;
                    rilevamentoPretty.risparmio_bottiglie = Math.round(rilevamentoPretty.totale/1.5);
                    rilevamentoPretty.risparmio_famiglie = Math.round(rilevamentoPretty.totale * 0.22 - rilevamentoPretty.totale * 0.05);
                    rilevamentoPretty.risparmio_acqua = Math.round(rilevamentoPretty.totale * 7);
                    rilevamentoPretty.risparmio_greggio = Math.round((rilevamentoPretty.totale * 162) / 1000);
                    rilevamentoPretty.risparmio_co = Math.round((rilevamentoPretty.totale * 100) / 1000);
                    rilevamentoPretty.risparmio_smaltimento = Math.round(rilevamentoPretty.totale * 0.08);
                    casetta.rilevamento = rilevamentoPretty;
                }


                // dopo una certa ora fai vedere una pagina con solo il logo
                var orasera = moment().set('hour', CONFIG.VIDEO_CASETTA_NIGHTPAGE_HOUR_EVENING);
                var oramattina = moment().set('hour', CONFIG.VIDEO_CASETTA_NIGHTPAGE_HOUR_MORNING);
                var adesso = moment();


                var nightpage = adesso.isAfter(orasera, 'hour') || adesso.isBefore(oramattina, 'hour');

                if(nightpage) {
                    casetta.mostravideo = casetta.mostrapagina = false
                    casetta.nightpage = true
                }

                casetta.formatPrice = function () {
                    return function (text, render) {
                      let n = parseFloat(render(text))
                      return numeral(n).format('$0,0.00')
                    }
                }

                casetta.formatNumber = function () {
                    return function (text, render) {
                      let n = parseFloat(render(text))
                      return numeral(n).format('0,0.00')
                    }
                }

                casetta.video = './videos/'+getIdVideo(casetta.video)// per nuova logica pluri-iot, ha già tutti i video cachati, basta passargli il nome del video

                res.render('paginacasetta',casetta);


                if(req.query.anteprima) { // se no è un anteprima salva la richiesta nei log
                    /*var log = new Log ({
                        title: 'Richiesta pagina video dalla casetta di ' + casetta.descrizione,
                        status: 10, //info
                        mdate: new Date(),
                        type: 11,
                        data: casetta
                    })*/

                    // log.save(function(err, resp) {
                       console.log('!')
                       /*if(err)
                        return next(err)*/

                        // segno la data di richiesta nella casetta
                        Casetta.update({_id: casetta._id}, {
                            ultima_chiamata_video: new Date(),
                            ip: ipRichiesta
                        }, function(err, resp) {
                           
                           if(err)
                            return next(err)

                        })

                    //})
                }
            })           
            .sort({data: -1})
            .limit(1)
            .lean();

        })
        .sort({data: -1})
        .limit(1)
        .lean();
        } else {
            res.render('paginacasetta_not_found');
        }
    }).lean() // devo restituite il semplice oggetto 
}

exports.reportRisparmio = function (req, res, next) {
    var result={};
    var idCasetta=req.params.id; 
    var query = {
        _id: req.params.id
    };
    if (idCasetta=="tutte" || !mongoose.Types.ObjectId.isValid(req.params.id)){
        query =  (req.query.tipo) ? { tipo:req.query.tipo} : {};
        result.idCasetta=idCasetta 
    } 

    var dataDa = new Date(Date.UTC(req.params.anno, "00", "01", 00, 00, 00, 0))
    var dataA = new Date(Date.UTC(req.params.anno, "11", "31", 00, 00, 00, 0))

    Casetta.find( 
       query,
        function (err, listaCasette) {
            if (err)
                return next(err); 
            /*
            try{
                result.dal= moment(_.clone(req.params.dal)).format('DD/MM/YYYY') 
            }
            catch(e){}
            try{
                result.al= moment(_.clone(req.params.al)).format('DD/MM/YYYY')
            }
            catch(e){}
            */
            
            async.each(listaCasette, 
                function(casetta, callback) {  
                    if(casetta) {
                        _manageRilevamenti(casetta,dataDa,dataA,
                            function(err, result) 
                             {
                                 if(err)
                                     return next(err); 
                                 else{ 
                                    callback();
                                }
                            }
                        )  
                    }  
                },
                function(err)
                {
                    if (err){
                        console.log(err)
                        throw err;
                    }
                    else{  
                        if (listaCasette.length==1){
                            result.descrizione = listaCasette[0].descrizione
                            result.rilevamento = listaCasette[0].rilevamento
                        }
                        else{
                            result.rilevamento = {};
                            totale = 0;
                            risparmio_bottiglie = 0;
                            risparmio_famiglie = 0;
                            risparmio_acqua = 0;
                            risparmio_greggio = 0;
                            risparmio_co = 0;
                            risparmio_smaltimento = 0;
                            listaCasette.forEach(function (e){ 
                                if (e.rilevamento){ 
                                    totale = totale + e.rilevamento.totale;
                                    risparmio_bottiglie = risparmio_bottiglie + e.rilevamento.risparmio_bottiglie;
                                    risparmio_famiglie = risparmio_famiglie + e.rilevamento.risparmio_famiglie;
                                    risparmio_acqua = risparmio_acqua + e.rilevamento.risparmio_acqua;
                                    risparmio_greggio = risparmio_greggio + e.rilevamento.risparmio_greggio;
                                    risparmio_co = risparmio_co + e.rilevamento.risparmio_co;
                                    risparmio_smaltimento = risparmio_smaltimento + e.rilevamento.risparmio_smaltimento;
                                }
                            })
                            result.rilevamento.totale=totale;
                            result.rilevamento.risparmio_bottiglie=risparmio_bottiglie;
                            result.rilevamento.risparmio_famiglie=risparmio_famiglie;
                            result.rilevamento.risparmio_acqua=risparmio_acqua;
                            result.rilevamento.risparmio_greggio=risparmio_greggio;
                            result.rilevamento.risparmio_co=risparmio_co;
                            result.rilevamento.risparmio_smaltimento=risparmio_smaltimento;
                        }
                        res.status(200).send(result);
                    }
                }
            )
        }
    )
    .lean() 
}


_manageRilevamenti  = function (casetta,dataDa,dataA, cb) {  
    async.series([
        function (callback) { 
            var query = {
                casetta: casetta._id,
                tipo:'rilevamento',
                data:  {
                    "$gte":dataDa, 
                    "$lt":dataA
                }
            } 
            //result.casetta= casetta;  
            InterventoCasetta.find(
            query, 
            function(err, rilevamenti){ 
                if(err){
                    console.log(err)
                    callback(err, 'errore');
                }             

                /*
                ULTIMO RILEVAMENTO DEL PERIODO
                */
                var rilevamento = rilevamenti[0]; 
                var rilevamentoPretty = {};

                if(rilevamento) {
                    rilevamentoPretty = rilevamento;
                    rilevamentoPretty.data = moment(rilevamento).format('DD/MM/YYYY')
                    rilevamentoPretty.totale = rilevamento.ambiente + rilevamento.fredda + rilevamento.gasata;
                } 
                else{
                    rilevamentoPretty = {};
                    rilevamentoPretty.totale = 0;
                }
                rilevamentoPretty.risparmio_bottiglie = Math.round(rilevamentoPretty.totale/1.5);
                rilevamentoPretty.risparmio_famiglie = Math.round(rilevamentoPretty.totale * 0.22 - rilevamentoPretty.totale * 0.05);
                rilevamentoPretty.risparmio_acqua = Math.round(rilevamentoPretty.totale * 7);
                rilevamentoPretty.risparmio_greggio = Math.round((rilevamentoPretty.totale * 162) / 1000);
                rilevamentoPretty.risparmio_co = Math.round((rilevamentoPretty.totale * 100) / 1000);
                rilevamentoPretty.risparmio_smaltimento = Math.round(rilevamentoPretty.totale * 0.08);
                casetta.rilevamentoUltimo = rilevamentoPretty; 

                /*
                PRIMO RILEVAMENTO DEL PERIODO
                */
                //se esiste prendiamo il primo con data DataDa minore di dataDa
                var rilevamentoPretty = {}
                if (rilevamenti.length>1){
                    var rilevamento = rilevamenti[rilevamenti.length-1];
                    if(rilevamento) {
                        rilevamentoPretty = rilevamento;
                        rilevamentoPretty.data = moment(rilevamento).format('DD/MM/YYYY')
                        rilevamentoPretty.totale = rilevamento.ambiente + rilevamento.fredda + rilevamento.gasata;
                    }  
                }
                else{
                    rilevamentoPretty.totale = 0;
                }
                rilevamentoPretty.risparmio_bottiglie = Math.round(rilevamentoPretty.totale/1.5);
                rilevamentoPretty.risparmio_famiglie = Math.round(rilevamentoPretty.totale * 0.22 - rilevamentoPretty.totale * 0.05);
                rilevamentoPretty.risparmio_acqua = Math.round(rilevamentoPretty.totale * 7);
                rilevamentoPretty.risparmio_greggio = Math.round((rilevamentoPretty.totale * 162) / 1000);
                rilevamentoPretty.risparmio_co = Math.round((rilevamentoPretty.totale * 100) / 1000);
                rilevamentoPretty.risparmio_smaltimento = Math.round(rilevamentoPretty.totale * 0.08);
                casetta.rilevamentoPrimo = rilevamentoPretty; 

                callback(null);
            })
            .sort({data: -1})
            .lean(); 
        },
        function (callback, results) { 
            if (casetta.rilevamentoPrimo){ // è stato già calcolato 
                callback(null);
            }
            else{
                var query = {
                    casetta: casetta._id,
                    tipo:'rilevamento',
                    data:  {
                        "$lt":dataDa
                    }
                } 
                InterventoCasetta.find(
                query, 
                function(err, rilevamenti){ 
                    if(err)
                        return next(err);
                    if (rilevamenti){
                        var rilevamento = rilevamenti[0]; 
                        if(rilevamento) {
                            var rilevamentoPretty = rilevamento;
                            rilevamentoPretty.data = moment(rilevamento).format('DD/MM/YYYY')
                            rilevamentoPretty.totale = rilevamento.ambiente + rilevamento.fredda + rilevamento.gasata;
                            rilevamentoPretty.risparmio_bottiglie = Math.round(rilevamentoPretty.totale/1.5);
                            rilevamentoPretty.risparmio_famiglie = Math.round(rilevamentoPretty.totale * 0.22 - rilevamentoPretty.totale * 0.05);
                            rilevamentoPretty.risparmio_acqua = Math.round(rilevamentoPretty.totale * 7);
                            rilevamentoPretty.risparmio_greggio = Math.round((rilevamentoPretty.totale * 162) / 1000);
                            rilevamentoPretty.risparmio_co = Math.round((rilevamentoPretty.totale * 100) / 1000);
                            rilevamentoPretty.risparmio_smaltimento = Math.round(rilevamentoPretty.totale * 0.08);
                            casetta.rilevamentoPrimo = rilevamentoPretty; 
                        }  
                    }
                    callback(null);
                })
                .sort({data: -1})
                .limit(1)
                .lean(); 
            }
        }, 
        function (callback) {
            if (casetta.rilevamentoPrimo){ //calcolo la differenza fra il primo e l'ultimo
                if (!casetta.rilevamentoUltimo){
                    console.log("pippo")
                }
                var rilevamentoPretty= {};
                rilevamentoPretty.totale = casetta.rilevamentoUltimo.totale - casetta.rilevamentoPrimo.totale;
                rilevamentoPretty.risparmio_bottiglie = casetta.rilevamentoUltimo.risparmio_bottiglie - casetta.rilevamentoPrimo.risparmio_bottiglie;
                rilevamentoPretty.risparmio_famiglie = casetta.rilevamentoUltimo.risparmio_famiglie - casetta.rilevamentoPrimo.risparmio_famiglie;
                rilevamentoPretty.risparmio_acqua = casetta.rilevamentoUltimo.risparmio_acqua - casetta.rilevamentoPrimo.risparmio_acqua;
                rilevamentoPretty.risparmio_greggio = casetta.rilevamentoUltimo.risparmio_greggio - casetta.rilevamentoPrimo.risparmio_greggio;
                rilevamentoPretty.risparmio_co = casetta.rilevamentoUltimo.risparmio_co - casetta.rilevamentoPrimo.risparmio_co;
                rilevamentoPretty.risparmio_smaltimento = casetta.rilevamentoUltimo.risparmio_smaltimento - casetta.rilevamentoPrimo.risparmio_smaltimento;
                casetta.rilevamento = rilevamentoPretty;
            }
            else{//altrimenti non c'è mai stato un rilevamento precedente
                casetta.rilevamento = casetta.rilevamentoUltimo
            }
            callback(null, casetta); 
        }
    ],
    function (err, finalResult) { 
        if (err)
           return cb(err);
        else
            return cb(null, finalResult);

    });
}

var formatters = { // formatters da usare nei tpl di mustcahe
    data : function () {
       
        return function (text, render) { 
            
            var a  = moment(text, "DD-MM-YYYY");
            return render(a)
        }
    }
};

exports.reportRisparmioPdf = function (req, res, next) { 
    var pdfTempPath = process.cwd()+'/.tmp/',
    headerTemplate = fs.readFileSync(process.cwd()+'/server/templates/pdf/header.html','utf8'), // scheda tecnica...
    footerTemplate = fs.readFileSync(process.cwd()+'/server/templates/pdf/footer.html','utf8'), // comune a tutti
    risparmioTemplate = fs.readFileSync(process.cwd()+'/server/templates/pdf/reportRisparmio.html', 'utf8'),  
    css = fs.readFileSync(process.cwd()+'/server/templates/pdf/style.css','utf8'); // comune a tutti
 
    var result={};
    var idCasetta=req.params.id; 
    var query = {
        _id: req.params.id
    };
    if (idCasetta=="tutte" || !mongoose.Types.ObjectId.isValid(req.params.id)){
        query = (req.query.tipo) ? { tipo:req.query.tipo} : {};
        result.idCasetta=idCasetta 
    } 

    var dataDa = new Date(Date.UTC(req.params.anno, "00", "01", 00, 00, 00, 0))
    var dataA = new Date(Date.UTC(req.params.anno, "11", "31", 00, 00, 00, 0))

    Casetta.find( 
       query,
        function (err, listaCasette) {
            if (err)
                return next(err); 

                result.anno=req.params.anno,

                /*
            try{
                result.dal= moment(_.clone(req.params.dal)).format('DD/MM/YYYY') 
            }
            catch(e){}
            try{
                result.al= moment(_.clone(req.params.al)).format('DD/MM/YYYY')
            }
            catch(e){}
*/
            async.each(listaCasette, 
                function(casetta, callback) { 
                    if(casetta) {   
                         _manageRilevamenti(casetta,dataDa,dataA,
                            function(err, result) 
                             {
                                 if(err)
                                     return next(err); 
                                 else{ 
                                    callback();
                                }
                            }
                        )  
                    } 
                },
                function(err)
                {
                    if (err){
                        console.log(err)
                        throw err;
                    }
                    else{  
                        if (listaCasette.length==1){
                            result.descrizione = listaCasette[0].descrizione
                            result.rilevamento = listaCasette[0].rilevamento
                        }
                        else{
                            result.rilevamento = {};
                            totale = 0;
                            risparmio_bottiglie = 0;
                            risparmio_famiglie = 0;
                            risparmio_acqua = 0;
                            risparmio_greggio = 0;
                            risparmio_co = 0;
                            risparmio_smaltimento = 0;
                            listaCasette.forEach(function (e){ 
                                if (e.rilevamento){ 
                                    totale = totale + e.rilevamento.totale;
                                    risparmio_bottiglie = risparmio_bottiglie + e.rilevamento.risparmio_bottiglie;
                                    risparmio_famiglie = risparmio_famiglie + e.rilevamento.risparmio_famiglie;
                                    risparmio_acqua = risparmio_acqua + e.rilevamento.risparmio_acqua;
                                    risparmio_greggio = risparmio_greggio + e.rilevamento.risparmio_greggio;
                                    risparmio_co = risparmio_co + e.rilevamento.risparmio_co;
                                    risparmio_smaltimento = risparmio_smaltimento + e.rilevamento.risparmio_smaltimento;
                                }
                            })
                            result.rilevamento.totale=totale;
                            result.rilevamento.risparmio_bottiglie=risparmio_bottiglie;
                            result.rilevamento.risparmio_famiglie=risparmio_famiglie;
                            result.rilevamento.risparmio_acqua=risparmio_acqua;
                            result.rilevamento.risparmio_greggio=risparmio_greggio;
                            result.rilevamento.risparmio_co=risparmio_co;
                            result.rilevamento.risparmio_smaltimento=risparmio_smaltimento;
                        }
 
                        result.formatPrice = function () {
                            return function (text, render) {
                            let n = parseFloat(render(text))
                            return numeral(n).format('$0,0.00')
                            }
                        }
        
                        result.formatNumber = function () {
                            return function (text, render) {
                            let n = parseFloat(render(text))
                            return numeral(n).format('0,0.00')
                            }
                        }

                        if(req.query.tipo){
                            result.entita_plurale = "Gli erogatori dell'acqua"
                            result.entita_singolare = "L'erogatore dell'acqua"
                        }
                        else{
                            result.entita_plurale = "Le casette dell'acqua"
                            result.entita_singolare = "La casetta dell'acqua"
                        }
 
                        var date = new Date(),
                        timestamp = date.valueOf(),
                        data = _.extend(result,{css: css, header: headerTemplate, footer:footerTemplate, formatters}),

                        html = mustache.render(risparmioTemplate,data),
                        
                        pdfOptions = {
                            format: 'A4',
                            orientation: 'portrait',
                            border: '0',
                            header: {
                                height: "3cm"
                            },
                            footer: {
                                height: "1cm"
                            },
                            quality: '100'
                        }

                        pdf.create(html, pdfOptions).toBuffer( function (err, response) 
                        {
                            if(err){
                                console.log(err)
                                res.status(500).send(err); 
                            }
                            res.status(200).send(response);
                        })
                    }
                }
            )
        }
    )
    .lean() 
}

exports.reportRisparmioXls = function (req, res, next) {   
    var result={};
    var idCasetta=req.params.id; 
    var query = {
        _id: req.params.id
    };

    var dataDa = new Date(Date.UTC(req.params.anno, "00", "01", 00, 00, 00, 0))
    var dataA = new Date(Date.UTC(req.params.anno, "11", "31", 00, 00, 00, 0))

    if (idCasetta=="tutte" || !mongoose.Types.ObjectId.isValid(req.params.id)){
        query = (req.query.tipo) ? { tipo:req.query.tipo} : {};
        result.idCasetta=idCasetta 
    }

    Casetta.find( 
       query,
        function (err, listaCasette) {
            if (err)
                return next(err); 
            /*
            try{
                result.dal= moment(_.clone(req.params.dal)).format('DD/MM/YYYY') 
            }
            catch(e){}
            try{
                result.al= moment(_.clone(req.params.al)).format('DD/MM/YYYY')
            }
            catch(e){}
            */

           async.each(listaCasette, 
                function(casetta, callback) {  
                    if(casetta) {
                        _manageRilevamenti(casetta,dataDa,dataA,
                            function(err, result) 
                             {
                                 if(err)
                                     return next(err); 
                                 else{ 
                                    callback();
                                }
                            }
                        )  
                    }  
                },
                function(err)
                {
                    if (err){
                        console.log(err)
                        throw err;
                    }
                    else{  
                        if (listaCasette.length==1){
                            result.descrizione = listaCasette[0].descrizione
                            result.rilevamento = listaCasette[0].rilevamento
                        }
                        res.status(200).send(listaCasette);
                    }
                }
            )
        }
    )
    .sort({descrizione: 1})
    .lean() 
}

// count
exports.countCasette = function (req, res, next) {
    var query = Util.getQuery(req)
    //query.tipo = {$ne: 'erogatore'} //ridondanza necessaria
    //query.tipo = { "$nin": ['erogatore', 'distributore'] }
    Casetta.count(query, function( err, count){
        if (err)
            return next(err);
        else
            res.status(200).send({totalCasette: count});        
    })

}

// count
exports.countErogatori = function (req, res, next) {
    var query = Util.getQuery(req)
    //query.tipo = {$eq: 'erogatore'} //ridondanza necessaria

    Casetta.count(query, function( err, count){
        if (err)
            return next(err);
        else
            res.status(200).send({totalErogatori: count});        
    })

}

// count
exports.countDistributori = function (req, res, next) {
    var query = Util.getQuery(req)
    //query.tipo = {$eq: 'distributore'} //ridondanza necessaria

    Casetta.count(query, function( err, count){
        if (err)
            return next(err);
        else
            res.status(200).send({totalDistributori: count});        
    })

}

getAnno = function(date){
    return moment(date).format('YYYY')
}

getMese = function(date){
    return moment(date).format('MM')
}

getGiorno= function(date){
    return moment(date).format('dd')
}