var mongoose = require('mongoose'),
    VideoCasetta = mongoose.model('VideoCasetta');

// video disponibili per la casetta

exports.query = function (req, res, next) {
    var query = Util.getQuery(req)
    var queryString = null;
    if(query.comune){
        queryString = {$and: [ {tenant: query.tenant}, {$or: [{ comune: query.comune }, { comune: { $exists: false } }]}]}
    } //prende tutti i video disponibili per tutti (comune non esiste in db) e i video relativi al comune indicato dall'utenza (in db comune:"siglaComune")

    VideoCasetta.find(queryString,function(err, result){
        if (err)
            return next(err);

        res.status(200).send(result);

    }).sort({name:1})
    
}



