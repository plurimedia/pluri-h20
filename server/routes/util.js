var mongoose = require('mongoose'),
    Newsletter = mongoose.model('Newsletter'), 
    CONFIG = require(process.cwd() + '/server/config.js'),
    _ = require('underscore');

getTenantUserService = function (req) {
    /*
    il tenant viene salvato in auth0.
    nel caso di utenti  BA il tenant è null
    nel caso di utenti Artide il tenant è artide
     */
    return (req.user && req.user.app_metadata.tenant) ? req.user.app_metadata.tenant : 'plurih2o'
}

exports.getTenantUser = function (req) {
  return getTenantUserService(req)
}

exports.getQuery = function(req){
    var query = {};

    /*
    se entra un utente admin--> solo utenti di plurimedia
        vede tutte le casette
    
    se entra un utente BA 
        vede solo le casette di BA

    se entra un utente comune 
        vede solo le casette di BA del proprio comune
    
    se entra un utente Artide
        vede le casette di BA e di Artide
    */
    if(req.user.app_metadata.roles && req.user.app_metadata.roles.indexOf('admin')>-1) {
        //gli admin di plurimedia vedono tutto
    }
    else{
        var tenant = getTenantUserService(req);
        query.tenant = tenant

        //Solo nel caso di tenant artide ci sono delle eccezioni, ovvero
        // vede i dati del suo tenant + alcuni(***1) dati del tenant di ba

        if(tenant=='artide') {
            query.tenant = { "$in": ['ipsoftware', 'artide'] }
            query.manutentore =  { "$in": ['Artide', 'pippo', 'etc'] } // aggiungere qui i manutentori di artide
        }

        /*
        GESTIONE RUOLO COMUNI
        se entra un ruolo comune vedi i dati del proprio comune (in auth0 aggiungere 'comune':'DE')*/
        if(req.user.app_metadata.roles && req.user.app_metadata.roles.indexOf('comune')>-1)
            query.comune = getComune(req);

        if (req.user.app_metadata.manutentore)
            query.manutentore = getManutentore(req);
    }

    if(_.isNull(req.query.tipo) || _.isUndefined(req.query.tipo) || req.query.tipo === 'casetta'){
        //query.tipo = {$ne: 'erogatore'}
        query.tipo = { "$nin": ['erogatore', 'distributore'] }   
    }
    else{
        query.tipo = req.query.tipo
    }

    return query;
}

getComune = function(req){
    return req.user.app_metadata.comune
}

/*
input id : newsletter._id,
output data: {titolo,periodo,articoli,subject,from,target_id}
*/
exports.getNewsLetterContent = function (_id,callback) 
{ 
   Newsletter.findOne(
    {
        _id: _id
    }, 
    function (err, newsletter) 
    {
        if (err)
            callback(err,null)
        else
        {
            var data = {
                titolo: newsletter.titolo,
                periodo:newsletter.periodo,
                articoli : [],
                comunicati : [],
                eventi : [],
                subject : newsletter.titolo + ' | ' + newsletter.periodo,
                from : {
                    //per mandrill
                    name:  CONFIG.MAILCHIMP_FROM_NAME, 
                    email: CONFIG.MAILCHIMP_FROM,
                    reply_to  : CONFIG.MAILCHIMP_REPLAY_TO
                },
                target_id:newsletter.target_id
 
            }
            var countTotNews =  newsletter.notizie.length;
            var contatore =0;                   
            newsletter.notizie.forEach(
                function(art) 
                {
                    if (art.copertina)
                       urlImg = "https://res.cloudinary.com/plurimedia/image/upload/c_fill,h_350,q_auto,w_600/a_0/"+art.copertina.id;
                    else
                        urlImg = "https://res.cloudinary.com/plurimedia/image/upload/c_fill,g_auto,h_350/v1575295360/plurih20/placeholder.jpg"; //"https://res.cloudinary.com/plurimedia/image/upload/c_fill,h_350,q_auto,w_600/a_0/brianzacque_prod/settings/placeholder";

                    var articolo = {}
                    articolo._id = art._id; 
                    articolo.copertina = urlImg
                    articolo.imgTarget = _getImageTag(art.tag)
                    articolo.titolo = art.titolo
                    articolo.estratto =  art.estratto ? art.estratto : art.testo // potrebbe essere un evento
                    articolo.url=CONFIG.APP_DATA.app_url_sito;

                    /*
                    Tutti gli articoli vengono resi nascosti,
                    comunicati ed eventi sempre visibili
                    */

                    if (art.tipo == 'articolo'){
                        articolo.url = articolo.url + art.tipo + ".html?mchimp=true#/!" + art._id
                    }
                    else{
                        articolo.url = articolo.url + art.tipo + ".html#/!"+ art._id
                    }

                    //Gli articoli vengono resi nascosti se interni
                    /*
                    if (art.sezione=='Interno')
                    {
                        articolo.url = articolo.url + art.tipo + "hidden.html?mchimp#/!" + art._id
                    }
                    else{ //esterno, entrambi
                        articolo.url = articolo.url + art.tipo + ".html#/!"+ art._id
                    } 
                    */

                    contatore ++; 
                    if (contatore < countTotNews)
                        articolo.ultimo = 'false' 

                    data.articoli.push(_.clone(articolo));
                }
            )
           
            callback(null,data)
        }
    })
}

_getImageTag = function (tag) { 
    if (tag == 'Acquedotto')
        return 'http://res.cloudinary.com/plurimedia/image/upload/c_scale,h_28/brianzacque_prod/settings/ACQUEDOTTO.jpg';
    else if (tag == 'Ambiente')
       return 'https://res.cloudinary.com/plurimedia/image/upload/c_scale,h_28/brianzacque_prod/settings/AMBIENTE.jpg'
    else if (tag == 'Azienda')
        return 'https://res.cloudinary.com/plurimedia/image/upload/c_scale,h_28/brianzacque_prod/settings/AZIENDA.jpg'
    else if (tag == 'Comunicati')
        return 'https://res.cloudinary.com/plurimedia/image/upload/c_scale,h_28/brianzacque_prod/settings/COMUNICATI.jpg'
    else if (tag == 'Corsi')
       return 'https://res.cloudinary.com/plurimedia/image/upload/c_scale,h_28/brianzacque_prod/settings/CORSI.jpg'
    else if (tag == 'Depurazione')
        return 'https://res.cloudinary.com/plurimedia/image/upload/c_scale,h_28/brianzacque_prod/settings/DEPURAZIONE.jpg'
    else if (tag == 'Dipendenti')
        return 'https://res.cloudinary.com/plurimedia/image/upload/c_scale,h_28/brianzacque_prod/settings/DIPENDENTI.jpg'
    else if (tag == 'Educazione')
        return 'https://res.cloudinary.com/plurimedia/image/upload/c_scale,h_28/brianzacque_prod/settings/EDUCAZIONE.jpg'
    else if (tag == 'Eventi')
        return 'https://res.cloudinary.com/plurimedia/image/upload/c_scale,h_28/brianzacque_prod/settings/EVENTI.jpg'
    else if (tag == 'Fognatura')
       return 'https://res.cloudinary.com/plurimedia/image/upload/c_scale,h_28/brianzacque_prod/settings/FOGNATURA.jpg'
    else if (tag == 'Gli uffici si presentano')
        return 'https://res.cloudinary.com/plurimedia/image/upload/c_scale,h_28/brianzacque_prod/settings/GLI-UFFICI.jpg'
    else if (tag == 'Istituzionale')
        return 'https://res.cloudinary.com/plurimedia/image/upload/c_scale,h_28/brianzacque_prod/settings/ISTITUZIONALE.jpg'
    else if (tag == 'Laboratori')
        return 'https://res.cloudinary.com/plurimedia/image/upload/c_scale,h_28/brianzacque_prod/settings/LABORATORI.jpg'
    else if (tag == 'Progettazione')
        return 'https://res.cloudinary.com/plurimedia/image/upload/c_scale,h_28/brianzacque_prod/settings/PROGETTAZIONE.jpg'
    else if (tag == 'Sociale')
        return 'http://res.cloudinary.com/plurimedia/image/upload/c_scale,h_28/brianzacque_prod/settings/SOCIALE.jpg'
    else if (tag == 'Sostenibilità')
        return 'https://res.cloudinary.com/plurimedia/image/upload/c_scale,h_28/brianzacque_prod/settings/SOSTENIBILITA.jpg'
    else if (tag == 'Investimenti')
        return 'https://res.cloudinary.com/plurimedia/image/upload/c_scale,h_28/brianzacque_prod/settings/INVESTIMENTI.jpg'
    else if (tag == 'Case dell\'acqua')
        return 'https://res.cloudinary.com/plurimedia/image/upload/c_scale,h_28/brianzacque_prod/settings/CASEACQUA.jpg'
    else if (tag == 'Intranet')
        return 'https://res.cloudinary.com/plurimedia/image/upload/c_scale,h_28/brianzacque_prod/settings/INTRANET.jpg'
    else if (tag == 'Pensionamenti')
        return 'https://res.cloudinary.com/plurimedia/image/upload/c_scale,h_28/brianzacque_prod/settings/PENSIONAMENTI.jpg'
    else if (tag == 'Personale')
        return 'https://res.cloudinary.com/plurimedia/image/upload/c_scale,h_28/brianzacque_prod/settings/PERSONALE.jpg'
    else if (tag == 'Sicurezza')
        return 'https://res.cloudinary.com/plurimedia/image/upload/c_scale,h_28/brianzacque_prod/settings/SICUREZZA.jpg'
    else if (tag == 'Brand identity')
        return 'https://res.cloudinary.com/plurimedia/image/upload/c_scale,h_28/brianzacque_prod/settings/BRAND.jpg'
    else if (tag == 'Tecnologia')
        return 'https://res.cloudinary.com/plurimedia/image/upload/c_scale,h_28/brianzacque_prod/settings/TECNOLOGIA.jpg'
    else if (tag == 'Servizi')
        return 'https://res.cloudinary.com/plurimedia/image/upload/c_scale,h_28/brianzacque_prod/settings/SERVIZI.jpg'    
    else if (tag == 'Progetti')
        return 'https://res.cloudinary.com/plurimedia/image/upload/c_scale,h_28/brianzacque_prod/settings/PROGETTI.jpg'    
    else if (tag == 'Iniziative')
        return 'https://res.cloudinary.com/plurimedia/image/upload/c_scale,h_28/brianzacque_prod/settings/INIZIATIVE.jpg' 

    else
        return '';
    }
