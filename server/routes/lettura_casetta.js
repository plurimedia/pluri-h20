var mongoose = require('mongoose'),
    LetturaCasetta= mongoose.model('LetturaCasetta'),
    _ = require('underscore');

 
 
exports.query = function (req, res, next) {
    
    /*
        posso cercare per operatore, range di date, casetta e tipo
    */
    var query = {},
        limit = req.query.limit || 0;  
 
   
    if(req.query.casetta)
        _.extend(query,{casetta:req.query.casetta})
        
    if(req.query.al && !req.query.dal)
         _.extend(query,{mdata:{$lte:req.query.al}})
        
    if(req.query.dal && !req.query.al)
        _.extend(query,{mdata:{$gte:req.query.dal}})
        
    if(req.query.dal && req.query.al)
        _.extend(query,{mdata:{$lte:req.query.al,$gte:req.query.dal}})
 
    if(req.query.fonte)
        _.extend(query,{fonte:req.query.fonte})
 

    var sort = {mdata:-1};
    if(req.query.sort)
        sort=req.query.sort
        
    console.log("query ", query);

    LetturaCasetta.find(query, 
        function (err, result) {
        if (err)
            return next(err); 
        
        res.status(200).send(result);
    })
    .sort(sort)
    .limit(parseInt(limit));
}

// get singolo rilevamento
exports.get = function (req, res, next) {
    LetturaCasetta.findOne({
        _id: req.params.id
    }, function (err, result) {
        if (err)
            return next(err);
        else
            res.status(200).send(result);
    });
}
 