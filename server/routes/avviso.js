var mongoose = require('mongoose'),
    Avviso = mongoose.model('Avviso');
  
exports.save = function (req, res, next) 
{
    var avviso = new Avviso(req.body);
    avviso.mdate = new Date(); 
    avviso.user = req.user;
    avviso.save(
        function (err, result) 
        {
            if (err)
                return next(err);
            else
                res.status(200).send(result);
        }
    );
}
 
exports.query = function (req, res, next) 
{
    console.log(req.params)

    var query  = {};
    Avviso.find(query, 
        function (err, result) 
        {
            if (err)
                return next(err);
            res.status(200).send(result);
        }
    ).sort();
} 

exports.get = function (req, res, next) 
{
    Avviso.findOne(
    {
        _id: req.params.id
    }, 
    function (err, result) 
    {
        if (err)
            return next(err);
        else
            res.status(200).send(result);
    });
} 
 
exports.update = function (req, res, next) 
{
    var Mavviso = req.body;
    Mavviso.mdate = new Date();
    Mavviso.user = req.user; 
    Avviso.findById(req.params.id, 
        function (err, avviso) 
        {
            avviso.update(Mavviso, 
            {
                runValidators: true
            }, 
            function (err) 
            {
                if (err)
                    return next(err);
                else
                    res.status(200).send(Mavviso);
            })
        }
    )
}
 
exports.delete = function (req, res, next) 
{
    Avviso.remove(
        {
            _id: req.params.id
        }, 
        function (err) 
        {
            if (err)
                return next(err);
            else
                res.status(200).end();
        }
    )
}
