const CONFIG = require(process.cwd()+'/server/config.js')
const _ = require('underscore')

// recupera da auth0 tutti gli operatori di casette
exports.operatori_casette = function (req, res, next) {
  let users = []

  function _getUsers () {
    const axios = require("axios").default

    let options = {
      method: 'POST',
      url: `https://${CONFIG.AUTH0_DOMAIN}/oauth/token`,
      data: {
        grant_type: 'client_credentials',
        client_id: 'sV1diSR8mDC9R2BQ70jeCLt4sgKPRSXW',
        client_secret: '31rWavJVpROPMyIm79J_cbR72TS7P5cZfarChw9as7qofnYDkXcrYQw8nBHaHSBB',
        audience: `https://${CONFIG.AUTH0_DOMAIN}/api/v2/`
      }
    }

    axios.request(options).then(function (response) {
      let opts = {
        method: 'GET',
        url: `https://${CONFIG.AUTH0_DOMAIN}/api/v2/users`,
        headers: {'content-type': 'application/json', authorization: 'Bearer ' + response.data.access_token}
      }

      axios.request(opts).then(function (response) {
        console.log(response.data)
        if (response.data && response.data.length) {
          users = _.filter(response.data, function (u) {
            return u.app_metadata && u.app_metadata.roles.indexOf('operatore_casette')!==-1
          })
        }
        res.status(200).send(users)
      }).catch(function (error) {
        console.error(error)
        res.status(500).send(error)
      })
    }).catch(function (error) {
      console.error(error)
      res.status(500).send(error)
    })
  }

  _getUsers()
}
