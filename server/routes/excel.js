var json2xls = require('json2xls'),
	XLSX 	 = require('xlsx'),
	_ 		 = require('underscore'),
	async 	 = require('async'),
	mongoose = require('mongoose'),

    LaboratorioRegola	= mongoose.model('LaboratorioRegola'),
    LaboratorioAnalisi 	= mongoose.model('LaboratorioAnalisi'),

    indirizziPrelievi 	 = require(process.cwd()+'/server/json/laboratorio_indirizzi_prelievi.json'),
    comuni_daVisualizzare= require(process.cwd()+'/server/json/laboratorio_comuni_daVisualizzare.json');

 	
 	//struttra dati che viene salvata in DB
    var dati_Analisi = {},
	salvato;

	 

_importAnalisiAcqua  = function (req,res, cb) 
{   
    async.series([
        function (callback) 
        {
            console.log("1. Lettura contenuto file excel - 'Dati' - 'Num Parametri' - 'Num Prelievi'")
			try
			{ 

				console.log('server', req.files)
				//recupero il contenuto di tutti i fogli excel
		  		//'Dati','Num Parametri','Num Prelievi'
            	var workbook = XLSX.readFile(req.files.file.path),
            		sheet_name_list = workbook.SheetNames;

            	//Dati
            	analisiXls = XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]])

            	//Num Parametri
				parametriXls = XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name_list[1]])

				//Num Prelievi
				prelieviXls = XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name_list[2]])

				callback(null);
			}
			catch(err)
			{
			 	callback(err);
			}
        },
        function (callback) 
        {
            console.log("2.	Analisi(dati)- check foglio: analisiXls")
            _checkAnalisi(analisiXls, 
             	function(err)
             	{
		        	if (err)
		                callback(err, 'errore');
	                else
	                    callback(null);
             	}
            )
        },
        function (callback) 
        {
            console.log("3. Num Parametri - check foglio: parametriXls")
            _checkParametri(parametriXls, 
             	function(err)
             	{
		        	if (err)
		                callback(err, 'errore');
	                else
	                    callback(null);
             	}
            )
        },
        function (callback) 
        {
            console.log("4.  Num prelievi  - check foglio: prelieviXls") 
            _checkPrelievi(prelieviXls, 
             	function(err)
             	{
		        	if (err)
		                callback(err, 'errore');
	                else
	                    callback(null);
             	}
            )
        },
        function (callback) 
        {
            console.log("5. Registrazione Analisi - Parametri - Prelievi IN DB") 

            dati_Analisi.user = req.user;

            _saveAnalisi(function(err, result)
             	{
		        	if (err)
		                callback(err, 'errore');
	                else{
						//nel caso in cui il caricamento è a Norma eliminiamo l'ultimo import.

					 

						LaboratorioAnalisi.find({},{_id:1,visibile:1}, function (err, elencoAnalisi) {
							if (err)
								callback(err, 'errore');
							else{
								if (elencoAnalisi.length >= 6 && !elencoAnalisi[elencoAnalisi.length-1].visibile){
									LaboratorioAnalisi.remove(
										{
											_id: elencoAnalisi[elencoAnalisi.length-1]._id
										}, 
										function (err) 
										{
											if (err)
												callback(err, 'errore');
											else
												callback(null);
										}
									)
								}
								else
								callback(null);
							}
						}).sort({cdate:-1});
						
					}
             	}
            )
        },
        function (callback) 
        {
			console.log("8.    Eventuali step finali ")
			console.log("      Cancellazione quinto record ")
			
            callback(null);
        }
    ],
    function (err) 
    { 
        if (err)
           return cb(err);
        else
            return cb(null);
    });
}

_checkAnalisi = function(analisiXls, callBack)
{
	/*
	step:
	1) recupero elenco regole dal DB
	2) per ogni riga del file excel(analisiXls) viene:
		A) valorizzato e normalizzato l'oggetto recordAnalisi e successivamente datiAnalisi. vedi su la struttura dati
		B) viene registrata la latitudine e la longitudine, il dato viene recuperato da un json statico(laboratorio_indirizzi_prelievi)
		C) viene registrato il dato visibile. Il dato viene recuperato da un json statico (laboratorio_comuni_daVisualizzare)
		D) viene eseguito per ogni valore(campo excel) il controllo del dato secondo i limiti di legge(collection delle regole)).
		E) viene registrato l'ordinamento da visualizzare nel front-end
	*/

	//recupero elenco regole
	LaboratorioRegola.findOne({}, 
    function (err, result) 
    { 
    	if (err)
    		return callBack(err, null);
        else
        {	
			/*
			iterazione su collection analisiXls, la quale contiene i dati del file excel
			ogni riga di questa collection rappresenta una riga del file excel
			*/
		
         	_.map(analisiXls, 
				function(elementXls)
				{


					/*
					per ogni riga del file excel mi creo un mio record normalizzato e pronto per eessere registrato in DB
					var recordAnalisi = {
						comune : Agrate,
						acqua_di:Pozzo,
						prelievo:via Roma, 12,
						coordinate:{
							lat:
							long:
						}
						codiceSif:1234,
						daVisualizzare:true
	                    valori: [
	                        {nome: 'ph', valore : 100, isValid: true},
	                        {nome: 'ferro', valore : 100, isValid: false}
	                        {nome: 'altro .....}
	                    ]
                    */
                   
					var recordAnalisi = 
					{
						Comune 		: elementXls.Comune,
						acqua_di	: elementXls['Acqua di'],
						prelievo 	    : elementXls.Prelievo,
						coordinate 	: _getCoordinate(elementXls.CodiceSIF),
						via : _getVia(elementXls.CodiceSIF),
						codiceSif 	: elementXls.CodiceSIF,

						/*
						In teroria non ci sono analisi da escludere.
						Se volessimo escludere dei comuni in base al file statico ""laboratorio_comuni_daVisualizzare""
						dobbiamo solo abilitare il metodo _comuneIsVisibile(elementXls.Comune)
						*/
					
						visibile 	: _comuneIsVisibile(elementXls.Comune),
						valori  	: [],
						aNorma		:true
					};

					/*
					per ogni regola presente in DB effettuo il controllo (_isValidValue) sul rispettivo valore arrivato dall'excel
					*/
					for (i in result.regole) 
					{
				    	var nomeRegola = i,
							 objRegola = result.regole[i];
							 
							 try{
								// quando troviamo il valore < 1 nn facciamo nulla
								valoreExl = elementXls[nomeRegola]
							
								if (valoreExl.indexOf("<") != 0 )
									valoreExl = parseFloat(valoreExl).toFixed(objRegola.decimali).replace('.',',');
							}
							catch(err){
								valoreExl = elementXls[nomeRegola]
							}

				     	//Il nome della regola  per convenzione Ã¨ sempre uguale al nome della colonna delle analisi
				     	var valore = {
		                	nome:nomeRegola,
		                	valore:valoreExl,
		                	aNorma : _isValidValue(nomeRegola, elementXls[nomeRegola], parseFloat(objRegola.min), parseFloat(objRegola.max)),
							regola: objRegola,
							ordinamento:objRegola.ordinamento
		                }

		                if(!valore.aNorma)
							recordAnalisi.aNorma = false; 

		                recordAnalisi.valori.push(valore); 
				    }  
				    dati_Analisi.analisi.push(recordAnalisi)  
				}
			)
			return callBack(null);
        }
    })
} 

_checkParametri = function(parametriXls, callBack)
{ 
	/*
	step:
	1) per ogni riga del file excel(parametriXls) viene:
		A) valorizzato e normalizzato l'oggetto parametri. vedi su la struttura dati
		B) viene registrato il dato visibile. Il dato viene recuperato da un json statico (laboratorio_comuni_daVisualizzare)
	Al momento non vengono effettuati controlli di validitÃ  su questo file, nel caso ce ne fosse la necessitÃ  effettuare qui i dovuti check
	*/ 
	try
	{
		dati_Analisi.parametri = _.clone(parametriXls)

		_.map(dati_Analisi.parametri, 
			function(element)
			{	
				element.visibile 	= _comuneIsVisibile(element.Comune),
				element.asterisco 	= _getAsterisco(element.Comune)
			}
		)

		return callBack(null)
	}
	catch(err)
	{
		return callBack(err)
	}
}

_checkPrelievi = function(prelieviXls, callBack)
{ 
	/*
	step:
	1) per ogni riga del file excel(prelieviXls) viene:
		A) valorizzato e normalizzato l'oggetto dati_Prelievi. vedi su la struttura dati
		B) viene registrato il dato visibile. Il dato viene recuperato da un json statico (laboratorio_comuni_daVisualizzare)
	Al momento non vengono effettuati controlli di validitÃ  su questo file, nel caso ce ne fosse la necessitÃ  effettuare qui i dovuti check
	*/
	try
	{
		dati_Analisi.prelievi = _.clone(prelieviXls)
		_.map(dati_Analisi.prelievi, 
			function(element)
			{
				element.visibile 	= _comuneIsVisibile(element.Comune),
				element.asterisco 	= _getAsterisco(element.Comune);
			}
		)
		return callBack(null)
	}
	catch(err)
	{
		return callBack(err)
	} 
}

_isValidValue = function(nomeParam, valore, valoreMin, valoreMax)
{
	/*
	I controlli che vengono effettuati:
	1) se il valore del campo excel contiene il simbolo <, come da accordi col cliente per noi quel valore Ã¨ un valore OK
	2) viene controllato se il valore excel Ã¨ minore del campo regola(valoreMin) registrato in DB e maggiore del campo valoreMax registrato in DB
	*/
	 if (valore && valore.indexOf("<")!=-1)
		return true;
	if(valoreMin === 0 && valoreMax === 0)
		return true;
	else if (valore > valoreMin && valore < valoreMax) // chiedere se vogliono il >= e il =<
		return true;
	else
	{
		//Registro il file come non a norma
		dati_Analisi.aNorma = false;
		return false;
	}
}

_getCoordinate = function(codiceSif)
{	
	var result = {
		lat:'',
		long:''
	}

	var indirizzo = _.find(indirizziPrelievi,
		function(obj) 
		{ 
		 	return obj['Codice SIF']===codiceSif; 
		}
	)
	
	if (indirizzo){
		result = 
		{
			lat:indirizzo['lat DECIM'].replace(',','.'),
			long:indirizzo['lng DECIM'].replace(',','.'),
		}
	}
 
	return result;
}

_getVia = function(codiceSif)
{	

	var row =  _.find(indirizziPrelievi,
		function(obj) 
		{ 
		 	return obj['Codice SIF']===codiceSif; 
		}
	);

	if(row) {
		return row.Via;
	}
	else {
		return ''
	}

}

_comuneIsVisibile = function(comune){
	var dato = _.find(comuni_daVisualizzare,
		function(obj) 
		{ 
		 	return obj['Comune'].toLowerCase()===comune.toLowerCase(); 
		}
	);

	if (dato)
		return true;
	else
		return false;
}

_getAsterisco = function(comune){
	var dato = _.find(comuni_daVisualizzare,
		function(obj) 
		{ 
		 	return obj['Comune'].toLowerCase()===comune.toLowerCase() && obj['Asterisco']=='true'; 
		}
	)
	if (dato)
		return true;
	else
		return false;
}

_saveAnalisi = function (callBack) 
{

    var lAnalisi = new LaboratorioAnalisi(dati_Analisi);
    lAnalisi.save(
    	function (err, result) {
	        if (err)
	            return callBack(err);
	        else{
	        	salvato = result;
	            callBack(null)
	        }
    });
}

// export in excel
exports.generate = function (req, res, next) 
{
    var data = req.body;
    res.xls('data.xlsx', data);
}

// import da xls: riceve i dati e il tipo di import da fare, in base a quello processa in modo diverso.
exports.import = function (req, res, next) 
{

dati_Analisi={
    	analisi: [
            /* 
            {
                comune: 'AG',
                codiceSif:1234,
				visibile:true
                valori: [
                    {nome: 'ph', valore : 100, aNorma: false},
                    {nome: 'ferro', valore : 100, aNorma: false}
                ]
                aNorma:false //facilitÃ  la lettura dei dati sul front-end
            } 
            */
        ],
        parametri : [
		/*type: Schema.Types.Mixed  
		{
			comune:'',
			anno:'',
			conteggioDati:''
			visibile:true,
			parametri_legge:'' 
		}
		*/
		],
		prelievi : [
		/*
		{
			comune:'',
			conteggioDati:''
 			visibile:true
		}
		*/
		],
        mdate: new Date(), 
        user:'',
        visibile: false,
        aNorma:true // il valore viene deciso dal metodo _isValidValue
	}

	_importAnalisiAcqua(req, res,
		function(err) 
		{
	        if(err)
	            return next(err); 
	        else{
	           res.status(200).send(salvato);
	        }
	    }
	)
}