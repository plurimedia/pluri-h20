var mongoose = require('mongoose'),
    Pagina = mongoose.model('Pagina'),
    _ = require('underscore'),
    moment = require('moment');

// salvataggio pagina
exports.save = function (req, res, next) {
    var pagina = new Pagina(req.body);
    pagina.mdate = new Date();
    pagina.user = req.user;
  
    pagina.save(function (err, result) {
        if (err)
            return next(err);
        else
            res.status(200).send(result);
    });
}

// lista delle pagine
exports.query = function (req, res, next) {

    var query = {};

    if (req.query.testo) // sto cercando per titolo, provo con regexp, verificare performance
        _.extend(query,{titolo: {$regex: req.query.testo, $options:'$i'}})

    if (req.query.visibile)
        _.extend(query,{visibile: true})

    Pagina.find(query, function (err, result) {

        if (err)
            return next(err);

        res.status(200).send(result);

    }).sort({titolo: 1});
}

// get singola pagina
exports.get = function (req, res, next) {
    Pagina.findOne({
        _id: req.params.id
    }, function (err, result) {
        if (err)
            return next(err);
        else
            res.status(200).send(result);
    });
}


// update singola pagina
exports.update = function (req, res, next) {

    var Mpagina = req.body;
    Mpagina.mdate = new Date();
    Mpagina.user = req.user;

    Pagina.findOneAndUpdate({
        _id: req.params.id
    }, Mpagina, function (err, pagina) {
        if (err) {
            return next(err);
        } else {
            res.status(200).send(Mpagina);
        }
    });
}

// delete singola pagina
exports.delete = function (req, res, next) {
    Pagina.remove({
        _id: req.params.id
    }, function (err) {
        if (err)
            return next(err);
        else
            res.status(200).end();
    });
}

// count
exports.count = function (req, res, next) {
    Pagina.count({}, function( err, count){
        if (err)
            return next(err);
        else
            res.status(200).send({total: count});        
    })
}
