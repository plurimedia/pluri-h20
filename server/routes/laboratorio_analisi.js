var mongoose = require('mongoose'),
    async    = require('async'),
    LaboratorioAnalisi = mongoose.model('LaboratorioAnalisi');

exports.listaAnalisi = function (req, res, next) {
    LaboratorioAnalisi.find({},{cdate:1,user:1,visibile:1,aNorma:1,sospendi:1}, function (err, result) {
        if (err)
            return next(err);
        else
            res.status(200).send(result);
    }).sort({cdate:-1});
}


exports.save = function (req, res, next) {
    var lAnalisi = new LaboratorioAnalisi(req.body);
    lAnalisi.mdate = new Date();
    lAnalisi.user = req.user;
 
    lAnalisi.save(function (err, result) {
        if (err)
            return next(err);
        else
            res.status(200).send(result);
    });
}


exports.update = function (req, res, next) {
    var lAnalisi = req.body;
    lAnalisi.mdate = new Date();
    lAnalisi.user = req.user;
 
    LaboratorioAnalisi.findById(req.params.id, 
    	function (err, analisi) {
	        analisi.update(lAnalisi, function (err) {
	            if (err) {
	                return next(err);
	            } else {
	                res.status(200).end();
	            }
	        }
	    )
    });
}


exports.deleteAnalisi = function (req, res, next) 
{
    LaboratorioAnalisi.remove(
        {
            _id: req.params.id
        }, 
        function (err) 
        {
            if (err)
                return next(err);
            else
                res.status(200).end();
        }
    )
}

// update massivo
exports.updateAnalisi = function (req,res,next){
    console.log(req.query)

    LaboratorioAnalisi.find({}, function(err,analisi) {

        if(err)
            return next(err)

        async.each(analisi, function(a,cb){


            if(req.query.visibile) { // sto aggiornando la visibilità

                if(a._id.toString() === req.query.visibile)
                    a.visibile = true;
                else
                    a.visibile = false;

            }

            if(req.query.sospendi) {
                
                if(a.visibile)
                    a.sospendi = true;                
            }

            else if(req.query.ripristina) {
                if(a.sospendi)
                    a.sospendi = false;                
            }


            LaboratorioAnalisi.findById(a._id, 
                function (err, analisi) {
                    analisi.update(a, function (err) {
                        if (err) {
                            cb(err);
                        } else {
                            cb()
                        }
                    }
                )
            });

        }, function(err){
            if (err)
                return next(err);
            else {
                res.status(200).end();
            }
        })
    }).lean()
}

exports.get = function (req, res, next) 
{
    LaboratorioAnalisi.findOne(
    {
        _id: req.query._id
    }, 
    function (err, result) 
    {
        if (err)
            return next(err);
        else
            res.status(200).send(result);
    })
} 
