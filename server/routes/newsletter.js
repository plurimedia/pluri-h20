var mongoose = require('mongoose'),
    Newsletter = mongoose.model('Newsletter'), 
    Mail = require(process.cwd() + '/server/routes/mail.js'),
    CONFIG = require(process.cwd() + '/server/config.js'),
    Util = require(process.cwd() + '/server/routes/util.js'),
    _ = require('underscore'),
    moment = require('moment');
  
exports.save = function (req, res, next) 
{
    var newsletter = new Newsletter(req.body);
    newsletter.mdate = new Date(); 
    newsletter.idate = new Date(); 
    newsletter.anno = moment(newsletter.idate).format('YYYY'); // salvo l'anno di inserimento per facilitare le ricerche
    newsletter.user = req.user;
    newsletter.stato = 'Bozza';
    newsletter.target_id = newsletter.target.id; 

    newsletter.save(
        function (err, result) 
        {
            if (err)
                return next(err);
            else
                res.status(200).send(result);
        }
    );
}
 
exports.query = function (req, res, next) 
{
    var query  = {};

    if (req.query.anno)
        _.extend(query, {anno: req.query.anno});

    if (req.query.target){
        var tg = JSON.parse(req.query.target); 
        _.extend(query, {target_id: tg.id});
    }

    if (req.query.titolo)
       query.titolo = {$regex: req.query.titolo, $options: 'i'}; 

    Newsletter.find(query, 
        function (err, result) 
        {
            if (err)
                return next(err);
            res.status(200).send(result);
        }
    ).sort({stato:1,mdate: -1});
}

exports.get = function (req, res, next) 
{
    Newsletter.findOne(
    {
        _id: req.params.id
    }, 
    function (err, result) 
    {
        if (err)
            return next(err);
        else
            res.status(200).send(result);
    });
} 
 
exports.update = function (req, res, next) 
{
    var Mnewsletter = req.body;
    Mnewsletter.mdate = new Date();
    Mnewsletter.user = req.user; 

    Newsletter.findById(req.params.id, 
        function (err, newsletter) 
        {
            newsletter.update(Mnewsletter, 
            {
                runValidators: true
            }, 
            function (err) 
            {
                if (err)
                    return next(err);
                else
                    res.status(200).send(Mnewsletter);
            })
        }
    )
}
 
exports.delete = function (req, res, next) 
{
    Newsletter.remove(
        {
            _id: req.params.id
        }, 
        function (err) 
        {
            if (err)
                return next(err);
            else
                res.status(200).end();
        }
    )
}
 
exports.count = function (req, res, next) 
{
    Newsletter.count({}, 
        function( err, count)
        {
            if (err)
                return next(err);
            else
                res.status(200).send({total: count});        
        }
    )
} 

exports.testmail = function (req, res, next) 
{
    Util.getNewsLetterContent(req.query._id, 

        function(err, newsLetterContent) {  

            var to = [];
            req.query.mailTest.split(",").forEach(
                function(item, i) 
                {
                    if (item!=''){
                        item = item.replace(/\s/g, "")
                        to.push(item)
                    }
                }
            ) 

           //to = ["amedeo.buco@plurimedia.it"];//per i test
           //

         
             Mail.send('newsletter_testEmail',newsLetterContent,newsLetterContent.subject,newsLetterContent.from,to,null,null,null,function(err, mail) {
                if(err) {
                    console.log('Non sono riuscito a inviare la mail di verifica %j',err)
                    return next(err)
                }
                else
                    res.status(200).end();
            }) 

        }
    ) 
}

