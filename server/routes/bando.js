var mongoose = require('mongoose'),
    Bando = mongoose.model('Bando'),
    _ = require('underscore'),
    moment = require('moment');

// salvataggio bando
exports.save = function (req, res, next) {
    var bando = new Bando(req.body);
    bando.mdate = new Date();
    bando.user = req.user;
    bando.anno = moment(req.body.data_pubblicazione).format('YYYY'); // salvo l'anno del bando come stringa

    bando.save(function (err, result) {
        if (err)
            return next(err);
        else
            res.status(200).send(result);
    });
}

// lista dei bandi
exports.query = function (req, res, next) {

    var query = {};

    if (req.query.testo) // sto cercando per titolo o codice
        _.extend(query,  {$text: { $search: req.query.testo}});
    
    if(req.query.anno)
        _.extend(query, {anno:req.query.anno});
    

    Bando.find(query, function (err, result) {

        if (err)
            return next(err);

        res.status(200).send(result);

    }).sort({data_pubblicazione: -1});
}

// get singolo bando
exports.get = function (req, res, next) {
    Bando.findOne({
        _id: req.params.id
    }, function (err, result) {
        if (err)
            return next(err);
        else
            res.status(200).send(result);
    });
}

// update singolo bando
exports.update = function (req, res, next) {

    var Mbando = req.body;
    Mbando.mdate = new Date();
    Mbando.user = req.user;
    Mbando.anno = moment(req.body.data_pubblicazione).format('YYYY');

    Bando.findById(req.params.id, function (err, bando) {
        bando.update(Mbando, function (err) {
            if (err) {
                return next(err);
            } else {
                res.status(200).end();
            }
        })
    });
}

// delete singolo bando
exports.delete = function (req, res, next) {
    Bando.remove({
        _id: req.params.id
    }, function (err) {
        if (err)
            return next(err);
        else
            res.status(200).end();
    });
}


// count
exports.count = function (req, res, next) {
    Bando.count({}, function( err, count){
        if (err)
            return next(err);
        else
            res.status(200).send({total: count});        
    })
}
