var mongoose = require('mongoose'),
    Glossario = mongoose.model('Glossario'),
    _ = require('underscore');
  
exports.save = function (req, res, next) 
{
    var glossario = new Glossario(req.body);
    glossario.mdate = new Date(); 
    glossario.user = req.user;

    console.log("glossario", glossario);

    glossario.save(
        function (err, result) 
        {
            if (err)
                return next(err);
            else
                res.status(200).send(result);
        }
    )
}
 
exports.query = function (req, res, next) 
{
    var query  = {};
    if(req.query.termine)
        _.extend(query,{termine:  {$regex: req.query.termine,  $options: 'i'}  })

    Glossario.find(query, function (err, result) 
    {
        if (err)
            return next(err);
        res.status(200).send(result);
    }).sort({termine: 1});
} 
 
exports.get = function (req, res, next) 
{
    Glossario.findOne(
    {
        _id: req.params.id
    }, 
    function (err, result) 
    {
        if (err)
            return next(err);
        else
            res.status(200).send(result);
    })
} 
 
exports.update = function (req, res, next) 
{
    var MGlossario = req.body;
    MGlossario.mdate = new Date();
    MGlossario.user = req.user; 
    Glossario.findById(req.params.id, 
        function (err, faq) 
        {
            faq.update(MGlossario, 
            {
                runValidators: true
            }, 
            function (err) {
                if (err)
                    return next(err);
                else
                    res.status(200).send(MGlossario);
            }
        )
    });
}
 
exports.delete = function (req, res, next) 
{
    Glossario.remove(
        {
            _id: req.params.id
        }, 
        function (err) 
        {
            if (err)
                return next(err);
            else
                res.status(200).end();
        }
    )
}

exports.count = function (req, res, next) 
{
    Glossario.count({}, 
        function( err, count)
        {
            if (err)
                return next(err);
            else
                res.status(200).send({total: count});        
        }
    )
}