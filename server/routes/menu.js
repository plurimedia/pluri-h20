var mongoose = require('mongoose'),
    Menu = mongoose.model('Menu'),
    _ = require('underscore'),
    moment = require('moment');

// salvataggio menu
exports.save = function (req, res, next) {
    var menu = new Menu(req.body);
    menu.mdate = new Date();
    menu.user = req.user;
  
    menu.save(function (err, result) {
        if (err)
            return next(err);
        else
            res.status(200).send(result);
    });
}

// lista dei menu
exports.query = function (req, res, next) {

    var query  = {};

    if(req.query.titolo)
        _.extend(query,{titolo: req.query.titolo})

    Menu.find(query, function (err, result) {

        if (err)
            return next(err);

        res.status(200).send(result);

    }).sort({titolo: -1});
}

// get singolo menu
exports.get = function (req, res, next) {
    Menu.findOne({
        _id: req.params.id
    }, function (err, result) {
        if (err)
            return next(err);
        else
            res.status(200).send(result);
    });
}


// update singolo menu
exports.update = function (req, res, next) {

    var Mmenu = req.body;
    Mmenu.mdate = new Date();
    Mmenu.user = req.user;

    Menu.findById(req.params.id, function (err, menu) {
        menu.update(Mmenu, {
            runValidators: true
        }, function (err) {
            if (err) {
                return next(err);
            } else {
                res.status(200).send(Mmenu);
            }
        })
    });
}

// delete singola pagina
exports.delete = function (req, res, next) {
    Menu.remove({
        _id: req.params.id
    }, function (err) {
        if (err)
            return next(err);
        else
            res.status(200).end();
    });
}


// count
exports.count = function (req, res, next) {
    Menu.count({}, function( err, count){
        if (err)
            return next(err);
        else
            res.status(200).send({total: count});        
    })
}