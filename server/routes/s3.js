var CONFIG = require(process.cwd()+'/server/config.js'),
    aws = require('aws-sdk');

    aws.config.update({
      accessKeyId: CONFIG.AWS_ACCESS_KEY_ID,
      secretAccessKey: CONFIG.AWS_SECRET_KEY,
      signatureVersion: CONFIG.AWS_SIGNATURE_VERSION,
      region: CONFIG.AWS_REGION
    });


var s3Client = new aws.S3();

exports.client = s3Client;

exports.getSignedUrl = function (req, res){
    var percorso = req.body.percorso,
        tipo = req.body.tipo,
        dimensione = req.body.dimensione,
        s3 = s3Client,
        s3_params = {
          Bucket: CONFIG.AWS_S3_BUCKET,
          Key: percorso,
          Expires: 900,
          ACL: 'public-read',
          ContentType: tipo
        };

    s3.getSignedUrl('putObject', s3_params, function (err, signedUrl) {
        res.send(signedUrl)
    });
}



