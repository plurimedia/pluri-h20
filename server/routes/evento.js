var mongoose = require('mongoose'),
    Evento = mongoose.model('Evento'),
    _ = require('underscore'),
    moment = require('moment'),
    copertina_default = {
        name: '',
        id: 'brianzacque_prod/settings/placeholder' // immagine di default su cloudinary
    };


// salvataggio Evento
exports.save = function (req, res, next) {
    var evento = new Evento(req.body);
    evento.mdate = new Date();
    evento.user = req.user;
    evento.anno = moment(req.body.da).format('YYYY'); // salvo l'anno del Evento come stringa
    
    if(!evento.copertina)
        evento.copertina = copertina_default; // bruttino sarebbe meglio coi validatori ma in update non vanno :(

    evento.save(function (err, result) {
        if (err)
            return next(err);
        else
            res.status(200).send(result);
    });
}

// lista eventi
exports.query = function (req, res, next) {
    var query = {};

    if (req.query.testo) // sto cercando per titolo o codice
        _.extend(query, {
        $text: {
            $search: req.query.testo
        }
    });

    if (req.query.anno)
        _.extend(query, {
            anno: req.query.anno
        });

    if (req.query.visibile)
        _.extend(query, {
            visibile: req.query.visibile
        }); 
     
    var limit=0;
    if (req.query.limit)
     {
        limit=parseInt(req.query.limit);
    }
    Evento.find(query, function (err, result) {

        if (err)
            return next(err);

        res.status(200).send(result);

    }).sort({
        mdate: -1
    })
    .limit(limit);
}

// get singolo Evento
exports.get = function (req, res, next) {
    Evento.findOne({
        _id: req.params.id
    }, function (err, result) {
        if (err)
            return next(err);
        else
            res.status(200).send(result);
    });
}


// update singolo Evento
exports.update = function (req, res, next) {

    var MEvento = req.body;
    MEvento.mdate = new Date();
    MEvento.user = req.user;
    MEvento.anno = moment(req.body.da).format('YYYY'); // salvo l'anno del Evento come stringa

    if(!MEvento.copertina)
        MEvento.copertina = copertina_default; // bruttino sarebbe meglio coi validatori ma in update non vanno :(

    Evento.findOneAndUpdate({
        _id: req.params.id
    }, MEvento, function (err, Evento) {
        if (err) {
            return next(err);
        } else {
            res.status(200).send(MEvento);
        }
    });
}

// delete singolo Evento
exports.delete = function (req, res, next) {
    Evento.remove({
        _id: req.params.id
    }, function (err) {
        if (err)
            return next(err);
        else
            res.status(200).end();
    });
}

// count
exports.count = function (req, res, next) {
    Evento.count({}, function( err, count){
        if (err)
            return next(err);
        else
            res.status(200).send({total: count});        
    })
}