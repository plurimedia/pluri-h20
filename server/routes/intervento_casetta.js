var mongoose = require('mongoose'),
    Intervento = mongoose.model('InterventoCasetta'),
    _ = require('underscore');

// salvataggio rilevamento
exports.save = function (req, res, next) {
    var intervento = new Intervento(req.body);
    intervento.user_id = req.user ? req.user.sub : req.body.user.sub;
    intervento.user = req.user ? req.user : req.body.user;
    
    intervento.save(function (err, result) {
        if (err)
            return next(err);
        else
            res.status(200).send(result);
    });
}

// lista dei rilevamenti
exports.query = function (req, res, next) {
    
    /*
        posso cercare per operatore, range di date, casetta e tipo
    */
    var query = {},
        limit = req.query.limit || 0;
 
    if(req.query.tipo)
        _.extend(query,{tipo:req.query.tipo}) 
   
    if(req.query.casetta)
        _.extend(query,{casetta:req.query.casetta})
        
    if(req.query.al && !req.query.dal)
         _.extend(query,{data:{$lte:req.query.al}})
        
    if(req.query.dal && !req.query.al)
        _.extend(query,{data:{$gte:req.query.dal}})
        
    if(req.query.dal && req.query.al)
        _.extend(query,{data:{$lte:req.query.al,$gte:req.query.dal}})
         
    if(req.query.operatore)
         _.extend(query,{user_id:req.query.operatore})  
    
    if(req.query.sostituzione_filtro)
         _.extend(query,{sostituzione_filtro:true})    

    if(req.query.sostituzione_lampade)
         _.extend(query,{sostituzione_lampade:true})  

    if(req.query.pulizia)
         _.extend(query,{pulizia:true})  
    
    if(req.query.anno)
         _.extend(query,{data:true})  

    var sort = {data:-1};
    if(req.query.sort)
        sort=req.query.sort
        
       //  console.log("query", query )
       //  console.log("sort", sort)
       //  console.log("limit", limit)
    Intervento.find(query, function (err, result) {
        if (err)
            return next(err); 

        res.status(200).send(result);

    })
    .sort(sort)
    .limit(parseInt(limit));
}

// get singolo rilevamento
exports.get = function (req, res, next) {
    Intervento.findOne({
        _id: req.params.id
    }, function (err, result) {
        if (err)
            return next(err);
        else
            res.status(200).send(result);
    });
}

// delete singolo rilevamento
exports.delete = function (req, res, next) {
    Intervento.findOneAndRemove({
        _id: req.params.id
    }, function (err, intervento) {
        if (err)
            return next(err);

        intervento.remove(); // mi serve per far scattare il middleware post('remove')
        res.status(200).end();
    });
}
