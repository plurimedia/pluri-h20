var mongoose = require('mongoose'),
	slug = require('slugger'),
    options = {alsoAllow: "/"};




// controlla se esiste uno slug in una collection (tranne se stesso) in caso contrario torna la sua proposta in base al candidato
exports.get = function (req, res, next) {
    var candidato = req.query.candidate,
        pagina = req.query.pagina,
        collection = mongoose.model(req.query.collection),
        found = false;

    console.log(candidato,slug(candidato,options))

    collection.findOne({slug: candidato}, function(err, result){
        if(err)
            return next(err)

        if(pagina) { // sto aggiornando una pagina esistente
            if(result && pagina!==result._id.toString()){
                found = true;
            }
        }
        else {
            if(result) {
                found = true;
            }
        }

        res.status(200).send({found:found, string: slug(candidato,options)});
    })
}
