var mongoose = require('mongoose'),
    Sportello = mongoose.model('Sportello'),
    _ = require('underscore');
  
exports.save = function (req, res, next) 
{
    var sportello = new Sportello(req.body);
    sportello.mdate = new Date(); 
    sportello.user = req.user;
    sportello.save(
        function (err, result) 
        {
            if (err)
                return next(err);
            else
                res.status(200).send(result);
        }
    );
}
 
exports.query = function (req, res, next) 
{
    var query  = {};
    if(req.query.indirizzoCompleto)
        _.extend(query,{indirizzoCompleto: {$regex: req.query.indirizzoCompleto, $options: 'i'}})

    Sportello.find(query, 
        function (err, result) 
        {
            if (err)
                return next(err);
            res.status(200).send(result);
        }
    ).sort({indirizzoCompleto: 1});
} 

exports.get = function (req, res, next) 
{
    Sportello.findOne(
    {
        _id: req.params.id
    }, 
    function (err, result) 
    {
        if (err)
            return next(err);
        else
            res.status(200).send(result);
    });
} 
 
exports.update = function (req, res, next) 
{
    var Msportello = req.body;
    Msportello.mdate = new Date();
    Msportello.user = req.user; 

    console.log(Msportello.copertina);

    Sportello.findById(req.params.id, 
        function (err, sportello) 
        {

            console.log("sportello ", sportello);


            sportello.update(Msportello, 
            {
                runValidators: true
            }, 
            function (err) 
            {
                if (err)
                    return next(err);
                else{
                    console.log("Msportello ", Msportello)
                    res.status(200).send(Msportello);
                }
            })
        }
    )
}
 
exports.delete = function (req, res, next) 
{
    Sportello.remove(
        {
            _id: req.params.id
        }, 
        function (err) 
        {
            if (err)
                return next(err);
            else
                res.status(200).end();
        }
    )
}
 
exports.count = function (req, res, next) 
{
    Sportello.count({}, 
        function( err, count)
        {
            if (err)
                return next(err);
            else
                res.status(200).send({total: count});        
        }
    )
}
