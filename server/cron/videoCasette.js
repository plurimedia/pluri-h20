/*
 * cron video casette: scarica i video da vimeo e li mette su s3 per farli erogare con cache dalla casette
 */

var CONFIG = require(process.cwd() + '/server/config.js'),
    path = require('path'),
    fs = require('fs'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    mkdirp = require('mkdirp'),
    async = require('async'),
    request = require('request'),
    LogSchema = require(process.cwd() + '/server/models/log'),
    Log = mongoose.model('Log'),
    VideoCasettaSchema = require(process.cwd() + '/server/models/video_casetta'),
    VideoCasetta = mongoose.model('VideoCasetta'),
    Vimeo = require('../routes/vimeo').client,
    S3 = require('../routes/s3').client,
    s3Stream = require('s3-upload-stream')(S3),
    CronJob = require('cron').CronJob;



// 1) controllo i video che ho su vimeo, torno id e url per il download
function _checkVideosOnVimeo(callback) {

    Vimeo.request({
        method: 'GET',
        path: '/users/' + CONFIG.VIMEO_USER_ID + '/albums/' + CONFIG.VIMEO_CASETTE_ALBUM + '/videos',
    }, function(error, body, status_code, headers) {

        if (error)
            callback({
                type: 'vimeo',
                msg: error
            })
        else {

            var videos = body.data;

            videos = _.filter(_.map(videos, function(video) {

                video.download = _.find(video.download, function(video) {
                    return video.quality === 'source';
                })

                video.picture = _.find(video.pictures.sizes, function(picture) {
                    return picture.width === 295;
                })

                return {
                    name: video.name,
                    id: video.uri,
                    filename: video.uri.split('/').pop() + '.mp4',
                    download: video.download && video.download.link ? video.download.link : '',
                    picture: video.picture && video.picture.link ? video.picture.link : '',
                };
            }), function(v) {
                return v.download;
            })

            callback(null, videos);
        }
    });
}

// 2) controllo i video che ho già caricato su s3
function _checkAlreadyLoadedVideos(videosOnVimeo, callback) {


    VideoCasetta.find({}, function(err, videos) {

        if (err)
            callback(err);

        var videosOnS3 = videos.length ? _.pluck(videos, 'id') : [],
            videosToUpload = _.difference(_.pluck(videosOnVimeo, 'id'), videosOnS3);

        console.log('video su s3', videosOnS3)
        console.log('video da caricare', videosToUpload)

        // se non è mai stato controllato, oppure tutti i video sono già stati trasferiti, non fare nulla e segna il log
        if (videosToUpload.length === 0) {
            console.log('no devo caricare nulla su s3')
            callback(null, []);
        } else {
            console.log('devo caricare su s3')
            callback(null, _.filter(videosOnVimeo, function(v) {
                return videosToUpload.indexOf(v.id) !== -1
            }));
        }

    }).sort({
        mdate: -1
    });

}

// 3) carico gli eventuali video su s3
function _uploadVideosTos3(videosToUpload, callback) {

    if (videosToUpload.length > 0) {

        async.eachSeries(videosToUpload, function(video, cbVideo) {

                console.log('carico', video.id);

                mkdirp(path.join(process.cwd(), '.tmp'), function (err) {
                    if (err) {
                        console.error(err)
                    }

                    else {

                        var videoname = video.filename,
                            videoLocalPath = path.join(process.cwd(), '.tmp', videoname),
                            write = fs.createWriteStream(videoLocalPath),
                            req = request(video.download).pipe(write);


                        write.on('finish', function() {

                            console.log('file scaricato in locale')

                            var read = fs.createReadStream(videoLocalPath),
                                upload = s3Stream.upload({
                                    Bucket: CONFIG.AWS_S3_BUCKET_VIDEO_CASETTE,
                                    ACL: 'public-read',
                                    CacheControl: 'max-age=604800, public',
                                    ContentType: 'video/mp4',
                                    Key: videoname
                                });

                            upload.on('part', function(details) {
                                console.log('dati ricevuti', details.receivedSize);
                                console.log('dati caricati', details.uploadedSize);
                            });

                            upload.on('error', function(error) {
                                console.log('errore durante il caricamento', error);
                                cbVideo(error);
                            });

                            upload.on('uploaded', function(details) {
                                console.log('video caricato su s3')
                                cbVideo();
                            });

                            read.pipe(upload);
                        })
                    }
                });

            },

            function(err) {
                if (err) {
                    console.log('Non tutti i video sono stati caricati, non aggiorno il log')
                    callback({
                        type: 's3',
                        msg: err
                    });
                } else {
                    console.log('finito di caricare tutti i video su s3 aggiorno la tabella dei video')
                    callback(null, videosToUpload);
                }
            })

    } else {
        callback(null, []);
    }
}

function _updateVideoTable(videosUploaded, callback) {

    if (videosUploaded.length > 0) {

        var videosSaved = [];

        console.log('aggiorno la tabella dei video', videosUploaded)

        async.eachSeries(videosUploaded, function(video, cb) {

            console.log('Inserisco il singolo video', video)

            video.mdate = new Date();
            video.s3_url = CONFIG.AWS_S3_BUCKET_VIDEO_CASETTE_PATH + video.filename;

            console.log('inserisco il video', video)
            var videoToInsert = new VideoCasetta(video);

            videoToInsert.save(function(err, result) {
                if (err) {
                    cb(err);
                } else {
                    videosSaved.push(result)
                    cb();
                }
            });

        }, function(err) {
            if (err) {
                console.log('errore durante il salvataggio del video su mongo')
                callback({
                    type: 'mongo',
                    msg: err
                })
            } else {
                console.log('video salvati su mongo', videosSaved)
                callback(null, videosSaved)
            }
        })
    } else {
        callback(null, [])
    }

}

function _tasks(standAlone) {

    async.waterfall([
        _checkVideosOnVimeo,
        _checkAlreadyLoadedVideos,
        _uploadVideosTos3,
        _updateVideoTable
    ], function(err, videosSaved) {

        var log = {
            title: 'Nessun video aggiornato',
            status: 10, //info
            mdate: new Date(),
            type: 1001
        }

        if (err) {
            log.title = 'Errore durante il caricamento dei video su s3'
            log.status = 40; // error
            log.data = err;
        } else {

            log.data = videosSaved;

            if (videosSaved.length > 0) {
                log.status = 20; //success
                log.title = 'Caricati ' + videosSaved.length + ' video su S3'
            }

            var l = new Log(log)

            l.save(function(err, result) {
                if (err)
                    console.log(err)
                else {
                    console.log('finito il cron di aggiornamento video')

                    if(standAlone){
                        console.log('chiudo la connessione al db')
                        mongoose.connection.close();
                    }
                }
            });

        }

    });
}


function _start(standAlone) {

    if(standAlone) { // se lo lancio come comando in test devo connettermi al db

        mongoose.connect(CONFIG.MONGO, {}, function (err) {

            if (err)
                throw err;

            console.log('connesso al db')

            _tasks(standAlone);

        });        
    }

    else {
        _tasks();
    }
}



var job = new CronJob({
  cronTime: '00 00 17 * * 1-5',
  onTick: function() {
    /*
     * Runs every weekday (Monday through Friday)
     * at 07:30:00 AM. It does not run on Saturday
     * or Sunday.
     */
    _start();
  },
  runOnInit: false,
  start: false,
  timeZone: CONFIG.TIMEZONE
});

job.start();

//per testarlo in locale -> node -e 'require("./server/cron/videoCasette")(true)'
module.exports = _start;
