/*
 * stato video casette: ogni tot controlla l'ultima chimata del video delle casette: se supera l'intervallo della config manda una mail
 */

var CONFIG = require(process.cwd() + '/server/config.js'),
Mail = require(process.cwd() + '/server/routes/mail.js'),
mongoose = require('mongoose'),
Log = mongoose.model('Log'),
_ = require('underscore'),
moment = require('moment'),
async = require('async'),
CasettaSchema = require(process.cwd() + '/server/models/casetta'),
Casetta = mongoose.model('Casetta'),
CronJob = require('cron').CronJob;


function _checkCasette (callback) {
	Casetta.find({}, function(err, casette){
		if(err)
			callback(err)

		// trovo quelle che non sono aggiornate & quelle che hanno un mìonitor
		
		var casetteAll = _.map(_.filter(casette, function (casetta){
			return 	casetta.iot && 
					casetta.monitor &&
					casetta.stato=='Attiva'
		}),function(casetta){
			return _.pick(casetta,'descrizione','ultima_chiamata_video')
		})

		var casetteNonAggiornate = _.filter(casetteAll, function (casetta) {
			if(!casetta.ultima_chiamata_video) {
				return true;
			}
			else {
				return moment().diff(moment(casetta.ultima_chiamata_video),'minutes') >= CONFIG.VIDEO_CASETTA_REFRESH_INTERVAL / 1000 / 60;
			}
		})

		var casetteAggiornate = _.filter(casetteAll, function (casetta) { 
			if(casetta.ultima_chiamata_video) {
				return  moment().diff(moment(casetta.ultima_chiamata_video),'minutes') < CONFIG.VIDEO_CASETTA_REFRESH_INTERVAL / 1000 / 60;
			} else {
				return false
			}
			
		})
	 

		// sistemo già date e altro
		callback(null,
			_.sortBy(
				_.map(casetteNonAggiornate,function(c){ c.ultima_chiamata_video = moment(c.ultima_chiamata_video).format('DD/MM/YY - HH:mm'); return c;}),function(c){
					return c.descrizione;
				}
			),
			_.sortBy(
				_.map(casetteAggiornate,function(c){ c.ultima_chiamata_video = moment(c.ultima_chiamata_video).format('DD/MM/YY - HH:mm'); return c;}),function(c){
					return c.descrizione;
				}
			)
		)
	})
}

function _sendMail (casetteNonAggiornate, casetteAggiornate, callback) {

if(casetteNonAggiornate.length > 0 || casetteAggiornate.length > 0) {

	var type = 'casette-segnalazione-errore-video',
		Data = { 
			data_controllo: moment().format('DD/MM/YY - HH:mm'),
			casetteNonAggiornate: casetteNonAggiornate,
			casetteNonAggiornateExist: casetteNonAggiornate.length ? true : false,

			casetteAggiornate: casetteAggiornate,
			casetteAggiornateExist: casetteAggiornate.length ? true : false

		},
		//subject = 'Riscontrato probabile malfunzionamento del video di '+ casetteNonAggiornate.length+ ' casette'
		subject = 'Esito riscontro video casette'
		from = {
			name:  CONFIG.EMAIL_FROM_NAME,
			email: CONFIG.EMAIL_FROM
		}
		to = CONFIG.EMAIL_AVVISO_VIDEO_CASETTE;

	 
	Mail.send(type,Data,subject,from,to,null,null,null,
		function(err, mail) {
			if(err) {
				console.log('Non sono riuscito a inviare la mail di verifica %j',err)
				//logger.error('Non sono riuscito a inviare la mail di verifica %j',err)
				return next(err)
			}
			else { 
				if (casetteNonAggiornate.length === 0) {
					callback(null,Data);
				} else {
					// apro un ticket per ogni casetta che non funzionano
					async.each(casetteNonAggiornate, function(casetta, cb) {

						var type = 'casette-invio-ticket',
							Data = _.extend(casetta, {data_controllo: moment().format('DD/MM/YY - HH:mm')}),
							subject = 'Riscontrato problema casetta di: '+ casetta.descrizione,
							from = {
								name:  CONFIG.EMAIL_FROM_NAME,
								email: CONFIG.EMAIL_FROM
							},
							to = CONFIG.EMAIL_TICKET_CASETTE;

							Mail.send(type,Data,subject,from,to,null,null,null,function (err, mail) {
								if(err) {
									console.log('Non sono riuscito a inviare la mail di verifica %j',err)
									//logger.error('Non sono riuscito a inviare la mail di verifica %j',err)
									cb(err)
								} else {
									cb()
								}
							})


					}, function (err) {

						if(err)
							console.log('errore invio ticket casetta', err)

						callback()
					})
				}
				
			}
		}
	)
}
else {
	console.log('Tutte le casette sono aggiornate')
	callback(null,false)
}

}

function _tasks(standAlone) {

async.waterfall([
	_checkCasette,
	_sendMail
], function(err, data) {

		var log = {
			title: 'Verificato lo stato dei video delle casette',
			status: data ? 30 : 10, //warn o info
			mdate: new Date(),
			type: 1002,
			data: data ? data : 'Tutte le casette rispondono correttamente'
		}

		var l = new Log(log)

		l.save(function(err, result) {
			if (err)
				console.log(err)
			else {
				console.log('finito il cron di aggiornamento stato video casette')

				if(standAlone){
					console.log('chiudo la connessione al db')
					mongoose.connection.close();
				}
			}
		});
})
}

function _start(standAlone) {

if(standAlone) { // se lo lancio come comando in test devo connettermi al db

	mongoose.connect(CONFIG.MONGO, {}, function (err) {

		if (err)
			throw err;

		console.log('connesso al db')

		_tasks(standAlone);

	});        
}

else {
	_tasks();
}
}


var job = new CronJob({
	cronTime: '00 00 09 * * 1-7',
	onTick: function() {
		_start();
	},
	runOnInit: false, 
	start: false,
	timeZone: CONFIG.TIMEZONE
});

var job2 = new CronJob({
	cronTime: '00 00 14 * * 1-7',
	onTick: function() {
		_start();
	},
	runOnInit: false, 
	start: false,
	timeZone: CONFIG.TIMEZONE
});

job.start();
job2.start();

//per testarlo in locale -> node -e 'require("./server/cron/statoVideoCasette")(true)'
module.exports = _start;
