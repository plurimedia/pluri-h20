
importScripts('https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js')


const version = 'v1'
workbox.core.skipWaiting()
workbox.core.clientsClaim()


/* richiesta per l'inject automatico del precaching  */
self.__precacheManifest = [].concat(self.__precacheManifest || [])
//workbox.precaching.suppressWarnings()
workbox.precaching.precacheAndRoute(self.__precacheManifest, {})

// Use a StaleWhileRevalidate strategy as default
workbox.routing.setDefaultHandler(
    new workbox.strategies.StaleWhileRevalidate({
      cacheName: 'allPr'
    })
  )



const matchCb = ({url, event}) => {
  if (url.pathname === '/secure/interventi_casetta') {
      return true
  } else {
      return false
  }
}

const bgSyncPlugin = new workbox.backgroundSync.Plugin('myQueueName', {
    maxRetentionTime: 24 * 60 // Retry for max of 24 Hours (specified in minutes)
});
  
workbox.routing.registerRoute(
  matchCb,
  new workbox.strategies.NetworkOnly({
    plugins: [bgSyncPlugin]
  }),
  'POST'
);