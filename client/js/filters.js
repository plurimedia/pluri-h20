numeral.language('it');

app
.filter('statobando', function () {
	return function (data) {
		return data ? 'Pubblicato' : 'Scaduto';
	}
})
.filter('comune', function (UTILITIES) {
	return function (data) {
        var comune = _.find(UTILITIES.comuni,function (c){
            return c.codice === data;
        })
		return comune ? comune.nome : 'Comune non trovato ('+data+')';
	}
})
.filter('litri', function () {
	return function (data) {

        if(data >= 0) {
            if(data === 1)
                return data +' litro';
            else
                return data +' litri'
        }
        else {
            return '';
        }
	}
})

.filter('prezzo', function () {
    return function (input) {
        if(input === 0)
            return numeral(0).format('$ 0,0.00')
        else
            return input ? numeral(input).format('$ 0,0.00') : '0,0';
    }
})
.filter('number', function () {
  
      return function (input) { 
        if(input === 0)
        return numeral(0).format('0,0.00')
    else
        return input ? numeral(input).format('0,0.00') : '0,0'; 
         
    }
})
.filter('abs', function () {
    return function (data) {
        return Math.abs(data);
    }
})
/*
* converte secondi in minuti e secondi (durata video vimeo)
*/

.filter('SECONDSTOMINUTES', function () {
    return function (seconds) {
        if(seconds){
            var minutes,
                seconds;
            minutes = Math.floor(seconds/60);
            seconds = seconds%60;

            return minutes+'m '+seconds+'s' 
        }
        else {
            return '';
        }
    }
});

