var KTenantBA = "ba";
var KTenantArt = "artide";
var KTenantAdmin = "admin";

function manageInfoTenant($rootScope){
    
    if ($rootScope.user && $rootScope.user.app_metadata && $rootScope.user.app_metadata.roles && $rootScope.user.app_metadata.roles.indexOf('admin')>-1) {
        //gli admin di plurimedia vedono tutto
        $rootScope.userIsAdmin = true
        $rootScope.tenant = KTenantAdmin
        $rootScope.logo = $rootScope.CONFIG[KTenantAdmin+'_logo_src'] 
        $rootScope.logo_small = $rootScope.CONFIG[KTenantAdmin+'_logo_small_src'] 
    }
    else{
        $rootScope.userIsAdmin = false
        $rootScope.tenant = ($rootScope.user.app_metadata && $rootScope.user.app_metadata.tenant)?$rootScope.user.tenant:KTenantBA
    }
    setData($rootScope)
}

function manageInfoTenantByUrl($rootScope){
    /*
        utilizzato in login.js
        qui non abbiamo effettuato la login per cui controllo l'url
    */
    if (window.location.hostname.indexOf(KTenantArt)>-1)
        $rootScope.tenant = KTenantArt
    else
        $rootScope.tenant = KTenantBA 
   
    setData($rootScope)
}

function setData($rootScope){
    $rootScope.logoAuth0 = $rootScope.CONFIG[$rootScope.tenant+'_logo_auth0'] 
    $rootScope.appName = $rootScope.CONFIG[$rootScope.tenant+'_appName'] 
    $rootScope.favicon = $rootScope.CONFIG[$rootScope.tenant+'_favicon'] 
    $rootScope.appNamIntro = $rootScope.CONFIG[$rootScope.tenant+'_intro'] 
    $rootScope.appNamAssistenza = $rootScope.CONFIG[$rootScope.tenant+'_assistenza'] 
    $rootScope.testoMailLogin = $rootScope.CONFIG[$rootScope.tenant+'_testo_mail_login'] 
    $rootScope.mailAssistenza = $rootScope.CONFIG[$rootScope.tenant+'_mail_assistenza']
    $rootScope.logo = $rootScope.CONFIG[$rootScope.tenant+'_logo_src'] 
    $rootScope.logo_small = $rootScope.CONFIG[$rootScope.tenant+'logo_small_src'] 
    $rootScope.favicon = $rootScope.CONFIG[$rootScope.tenant+'_favicon'] 
    $rootScope.casetteExtraColumn = $rootScope.CONFIG[$rootScope.tenant+'_casetteExtraColumn'] 
    
}
