var app = angular.module('app', [
  'ngCkeditor', // editor
  'bootcomplete', // autocomplete
  'dndLists', // drag & drop
  'ngNotify', // messagggi
  'angular-loading-bar', // automatic loading bar
  'mgcrea.ngStrap', // angular strap
  'ui.router', // routing
  'ngResource', // services
  'ngSanitize', // pulizia stringhe html
  'ngFileUpload', // upload files
  'angularInlineEdit', //edit inline
  'ui.tree', // tree sortable
  'angularUtils.directives.dirPagination',
  'angular-jwt'
])
.config(function ($stateProvider, $httpProvider, $urlRouterProvider, jwtOptionsProvider) {
    $stateProvider
        .state('dashboard', {
            url: '/dashboard',
            templateUrl: '/html/views/dashboard.html',
            controller: 'dashboard',
            resolve: {
                bandi: ['BANDO',
                        function (BANDO)
                    {
                        return BANDO.count();
                    }],
                esiti: ['ESITO',
                        function (ESITO)
                    {
                        return ESITO.count();
                    }],
                comunicati: ['COMUNICATO',
                        function (COMUNICATO)
                    {
                        return COMUNICATO.count();
                    }],
                eventi: ['EVENTO',
                        function (EVENTO)
                    {
                        return EVENTO.count();
                    }],
                casette: ['CASETTA',
                        function (CASETTA)
                    {
                        return CASETTA.count();
                    }],
                erogatori: ['EROGATORE',
                        function (EROGATORE)
                    {
                        return EROGATORE.count();
                    }],
                distributori: ['DISTRIBUTORE',
                    function (DISTRIBUTORE)
                {
                    return DISTRIBUTORE.count();
                }],
                pagine: ['PAGINA',
                        function (PAGINA)
                    {
                        return PAGINA.count();
                    }],
                menues: ['MENU',
                        function (MENU)
                    {
                        return MENU.count();
                    }],
                faqs: ['FAQ',
                    function (FAQ)
                    {
                        return FAQ.count();
                    }
                ],
                glossari: ['GLOSSARIO',
                    function (GLOSSARIO)
                    {
                        return GLOSSARIO.count();
                    }
                ],
                sportelli: ['SPORTELLO',
                    function (SPORTELLO)
                    {
                        return SPORTELLO.count();
                    }
                ],
                articoli: ['ARTICOLO',
                    function (ARTICOLO)
                    {
                        return ARTICOLO.count();
                    }
                ],
                newsletters: ['NEWSLETTER',
                    function (NEWSLETTER)
                    {
                        return NEWSLETTER.count();
                    }
                ] 
            },
        })
        /* BANDI */
        .state('bandi', {
            url: '/bandi',
            templateUrl: '/html/views/bandi.html',
            controller: 'bandi',
            resolve: {
                bandi: ['BANDO',
                        function (BANDO)
                    {
                        return BANDO.query({
                            anno: moment().format('YYYY') // di default apro sull'anno corrente
                        });
                    }],
                bando: function () {
                        return {};
                    } // qui non ho il singolo bando
            },
        })
        .state('bando', {
            url: '/bando/:id',
            templateUrl: '/html/views/bando.html',
            controller: 'bandi',
            resolve: {
                bandi: function () {
                    return [];
                }, // qui non ho la lista
                bando: ['$stateParams', 'BANDO',
                        function ($stateParams, BANDO)
                    {
                        return $stateParams.id ? BANDO.get({
                            id: $stateParams.id
                        }) : {}; // nuovo o esistente
                    }]
            },
        })
        .state('bando.allegati', {
            url: '/allegati',
            templateUrl: '/html/views/bando-allegati.html',
            controller: 'bandi'
        })
        /* ESITI Di GARA */
        .state('esiti', {
            url: '/esiti',
            templateUrl: '/html/views/esiti.html',
            controller: 'esiti',
            resolve: {
                esiti: ['ESITO',
                        function (ESITO)
                    {
                        return ESITO.query({
                            anno: moment().format('YYYY') // di default apro sull'anno corrente
                        });
                    }],
                esito: function () {
                    return {};
                }
            },
        })
        .state('esito', {
            url: '/esito/:id',
            templateUrl: '/html/views/esito.html',
            controller: 'esiti',
            resolve: {
                esiti: function () {
                    return [];
                }, // qui non ho la lista
                esito: ['$stateParams', 'ESITO',
                        function ($stateParams, ESITO)
                    {
                        return $stateParams.id ? ESITO.get({
                            id: $stateParams.id
                        }) : {}; // nuovo o esistente
                    }]
            },
        })
        .state('esito.allegati', {
            url: '/allegati',
            templateUrl: '/html/views/esito-allegati.html',
            controller: 'esiti'
        })

        /* COMUNICATI  */
        .state('comunicati', {
            url: '/comunicati',
            templateUrl: '/html/views/comunicati.html',
            controller: 'comunicati',
            resolve: {
                comunicati: ['COMUNICATO',
                        function (COMUNICATO)
                    {
                        return COMUNICATO.query({
                            anno: moment().format('YYYY') // di default apro sull'anno corrente
                        });
                    }],
                comunicato: function () {
                    return {};
                }
            },
        })
        .state('comunicato', {
            url: '/comunicato/:id',
            templateUrl: '/html/views/comunicato.html',
            controller: 'comunicati',
            resolve: {
                comunicati: function () {
                    return [];
                }, // qui non ho la lista
                comunicato: ['$stateParams', 'COMUNICATO',
                        function ($stateParams, COMUNICATO)
                    {
                        return $stateParams.id ? COMUNICATO.get({
                            id: $stateParams.id
                        }) : {}; // nuovo o esistente
                    }]
            },
        })
        .state('comunicato.immagini', {
            url: '/immagini',
            templateUrl: '/html/views/comunicato-immagini.html',
            controller: 'comunicati'
        })
        .state('comunicato.allegati', {
            url: '/allegati',
            templateUrl: '/html/views/comunicato-allegati.html',
            controller: 'comunicati'
        })
        /* EVENTI */
        .state('eventi', {
            url: '/eventi',
            templateUrl: '/html/views/eventi.html',
            controller: 'eventi',
            resolve: {
                eventi: ['EVENTO',
                        function (EVENTO)
                    {
                        return EVENTO.query({
                            anno: moment().format('YYYY') // di default apro sull'anno corrente
                        });
                    }],
                evento: function () {
                    return {};
                }
            },
        })
        .state('evento', {
            url: '/evento/:id',
            templateUrl: '/html/views/evento.html',
            controller: 'eventi',
            resolve: {
                eventi: function () {
                    return [];
                }, // qui non ho la lista
                evento: ['$stateParams', 'EVENTO',
                        function ($stateParams, EVENTO)
                    {
                        return $stateParams.id ? EVENTO.get({
                            id: $stateParams.id
                        }) : {}; // nuovo o esistente
                    }]
            },
        })
        .state('evento.immagini', {
            url: '/immagini',
            templateUrl: '/html/views/evento-immagini.html',
            controller: 'eventi'
        })
        .state('evento.allegati', {
            url: '/allegati',
            templateUrl: '/html/views/evento-allegati.html',
            controller: 'eventi'
        })
        /* CANTIERI  */
        .state('cantieri', {
                url: '/cantieri',
                templateUrl: '/html/views/cantieri.html',
                controller: 'cantieri',
                resolve: {
                    cantieri: ['CANTIERE',
                        function (CANTIERE)
                        {
                            return CANTIERE.query({
                                anno: moment().format('YYYY') // di default apro sull'anno corrente
                            });
                    }],
                    cantiere: function () {
                        return {};
                    }
                },
        })
        .state('cantiere', {
            url: '/cantiere/:id',
            templateUrl: '/html/views/cantiere.html',
            controller: 'cantieri',
            resolve: {
                cantieri: function () {
                    return [];
                }, // qui non ho la lista
                cantiere: ['$stateParams', 'CANTIERE',
                    function ($stateParams, CANTIERE)
                    {
                        return $stateParams.id ? CANTIERE.get({
                            id: $stateParams.id
                        }) : {avanzamento:0}; // nuovo o esistente
                }]
            },
        })
        .state('cantiere.immagini', {
            url: '/immagini',
            templateUrl: '/html/views/cantiere-immagini.html',
            controller: 'cantieri'
        })
        .state('cantiere.allegati', {
            url: '/allegati',
            templateUrl: '/html/views/cantiere-allegati.html',
            controller: 'cantieri'
        })
        /* homepage operatore casette */
        .state('dashboard_operatore_casette', {
            url: '/dashboard_operatore_casette',
            templateUrl: '/html/views/dashboard_operatore_casette.html',
            controller: 'dashboard_operatore_casette'
        })
         /* EROGATORI  */
         .state('erogatori', {
            url: '/erogatori',
            templateUrl: '/html/views/erogatori.html',
            controller: 'casette',
            resolve: {
                casette: ['EROGATORE',
                    function (EROGATORE)
                    {
                        return EROGATORE.query();
                }],
                casetta: function () {
                    return {};
                },
                interventi: function () {
                    return [];
                }, // qui non ho la lista
                operatori: function () {
                    return [];
                }, // qui non ho la lista
                video: function () {
                    return [];
                }, //
                consumi: function () {
                    return [];
                }, //
                letture: function () {
                    return [];
                } //
            },
        })
        .state('erogatore', {
            url: '/erogatore/:id',
            templateUrl: '/html/views/erogatore.html',
            controller: 'casette',
            resolve: {
                casette: function () {
                    return [];
                }, // qui non ho la lista
                casetta: ['$stateParams', 'EROGATORE',
                    function ($stateParams, EROGATORE)
                    {
                        return $stateParams.id ? EROGATORE.get({
                            id: $stateParams.id,
                            tipo: 'erogatore'
                        }) : {}; // nuova o esistente
                }],
                interventi: function () {
                    return [];
                }, // qui non ho la lista
                operatori: function () {
                    return [];
                }, // qui non ho la lista
                video: function () {
                    return [];
                }, //
                consumi: function () {
                    return [];
                }, //
                letture: function () {
                    return [];
                } //
            },
        })
        .state('erogatore.immagini', {
            url: '/immagini',
            templateUrl: '/html/views/casetta-immagini.html',
            controller: 'casette'
        })
        .state('erogatore.allegati', {
            url: '/allegati',
            templateUrl: '/html/views/casetta-allegati.html',
            controller: 'casette'
        })
        .state('erogatore.interventi', {
            url: '/interventi',
            templateUrl: '/html/views/casetta-interventi.html',
            resolve: {
                interventi: ['$stateParams','INTERVENTO_CASETTA',
                    function ($stateParams,INTERVENTO_CASETTA)
                    {
                        return INTERVENTO_CASETTA.query({limit:10,casetta:$stateParams.id});
                }],
                operatori: ['AUTH0',
                    function (AUTH0)
                    {
                        return AUTH0.operatoriCasette();
                }]
            },
            controller: 'casette'
        })
        .state('erogatore.video', {
            url: '/video',
            templateUrl: '/html/views/casetta-video.html',
            resolve: {
                video: ['$stateParams','VIDEO_CASETTA',
                    function ($stateParams,VIDEO_CASETTA)
                    {
                        return VIDEO_CASETTA.query();
                }]
            },
            controller: 'casette'
        })
        .state('erogatore.multimedia', {
            url: '/multimedia',
            templateUrl: '/html/views/erogatore-multimedia.html',
            controller: 'casette'
        })
        .state('erogatore.consumi', {
            url: '/consumi',
            templateUrl: '/html/views/casetta-consumi.html',
            controller: 'casette'
        })
        .state('erogatori-report', { 
            url: '/erogatori-report',
            templateUrl: '/html/views/erogatori-report-interventi.html',
            resolve: {
                casette: ['EROGATORE',
                    function (EROGATORE)
                    {
                        return EROGATORE.query();
                }],
                casetta: function () {
                    return {};
                },
                interventi: function () {
                    return [];
                }, // qui non ho la lista
                operatori: function () {
                    return [];
                }, // qui non ho la lista
                video: function () {
                    return [];
                }, //
                consumi: function () {
                    return [];
                }, //
                letture: function () {
                    return [];
                } // //
            },
            controller: 'casette'

        })
        .state('erogatori-report.consumi', {
            url: '/consumi',
            templateUrl: '/html/views/erogatori-report-consumi.html',
            controller: 'casette',
            resolve: {
                casette: ['EROGATORE',
                function (EROGATORE)
                {
                    return EROGATORE.query();
                }],
                casetta: function () {
                    return {};
                },
                interventi: function () {
                    return [];
                },
                operatori: function () {
                    return [];
                },
                video: function () {
                    return [];
                },  
                consumi: function () {
                    return [];
                }, //
                letture: function () {
                    return [];
                } // 
            },
        })
        .state('erogatori-report.risparmio', {
            url: '/risparmio',
            templateUrl: '/html/views/erogatori-report-risparmio.html',
            controller: 'casette',
            resolve: {
                casette: ['EROGATORE',
                function (EROGATORE)
                {
                    return EROGATORE.query();
                }],
                casetta: function () {
                    return {};
                },
                interventi: function () {
                    return [];
                },
                operatori: function () {
                    return [];
                },
                video: function () {
                    return [];
                },  
                consumi: function () {
                    return [];
                }, //
                letture: function () {
                    return [];
                } // 
            },
        })
        .state('erogatore.letture', {
            url: '/lettura',
            templateUrl: '/html/views/erogatore-letture.html',
            resolve: {
                letture: ['$stateParams','LETTURA_CASETTA',
                    function ($stateParams,LETTURA_CASETTA)
                    {
                        return LETTURA_CASETTA.query({limit:10,casetta:$stateParams.id});
                }]
            },
            controller: 'casette'
        })
        /* DISTRIBUTORI  */
        .state('distributori', {
            url: '/distributori',
            templateUrl: '/html/views/distributori.html',
            controller: 'casette',
            resolve: {
                casette: ['DISTRIBUTORE',
                    function (DISTRIBUTORE)
                    {
                        return DISTRIBUTORE.query();
                }],
                casetta: function () {
                    return {};
                },
                interventi: function () {
                    return [];
                }, // qui non ho la lista
                operatori: function () {
                    return [];
                }, // qui non ho la lista
                video: function () {
                    return [];
                }, //
                consumi: function () {
                    return [];
                }, //
                letture: function () {
                    return [];
                } //
            },
        })
        .state('distributore', {
            url: '/distributore/:id',
            templateUrl: '/html/views/distributore.html',
            controller: 'casette',
            resolve: {
                casette: function () {
                    return [];
                }, // qui non ho la lista
                casetta: ['$stateParams', 'DISTRIBUTORE',
                    function ($stateParams, DISTRIBUTORE)
                    {
                        return $stateParams.id ? DISTRIBUTORE.get({
                            id: $stateParams.id,
                            tipo: 'distributore'
                        }) : {}; // nuova o esistente
                }],
                interventi: function () {
                    return [];
                }, // qui non ho la lista
                operatori: function () {
                    return [];
                }, // qui non ho la lista
                video: function () {
                    return [];
                }, //
                consumi: function () {
                    return [];
                }, //
                letture: function () {
                    return [];
                } //
            },
        })
        .state('distributore.immagini', {
            url: '/immagini',
            templateUrl: '/html/views/casetta-immagini.html',
            controller: 'casette'
        })
        .state('distributore.allegati', {
            url: '/allegati',
            templateUrl: '/html/views/casetta-allegati.html',
            controller: 'casette'
        })
        /* CASETTE  */
        .state('casette', {
                url: '/casette',
                templateUrl: '/html/views/casette.html',
                controller: 'casette',
                resolve: {
                    casette: ['CASETTA',
                        function (CASETTA)
                        {
                            return CASETTA.query();
                    }],
                    casetta: function () {
                        return {};
                    },
                    interventi: function () {
                        return [];
                    }, // qui non ho la lista
                    operatori: function () {
                        return [];
                    }, // qui non ho la lista
                    video: function () {
                        return [];
                    }, //
                    consumi: function () {
                        return [];
                    },
                    letture: function () {
                        return [];
                    } //
                },
        })
        .state('casetta', {
            url: '/casetta/:id',
            templateUrl: '/html/views/casetta.html',
            controller: 'casette',
            resolve: {
                casette: function () {
                    return [];
                }, // qui non ho la lista
                casetta: ['$stateParams', 'CASETTA',
                    function ($stateParams, CASETTA)
                    {
                        return $stateParams.id ? CASETTA.get({
                            id: $stateParams.id
                        }) : {}; // nuova o esistente
                }],
                interventi: function () {
                    return [];
                }, // qui non ho la lista
                operatori: function () {
                    return [];
                }, // qui non ho la lista
                video: function () {
                    return [];
                },
                consumi: function () {
                    return [];
                },
                letture: function () {
                    return [];
                } //
            },
        })
        .state('casetta.immagini', {
            url: '/immagini',
            templateUrl: '/html/views/casetta-immagini.html',
            controller: 'casette'
        })
        .state('casetta.allegati', {
            url: '/allegati',
            templateUrl: '/html/views/casetta-allegati.html',
            controller: 'casette'
        })
        .state('casetta.interventi', {
            url: '/interventi',
            templateUrl: '/html/views/casetta-interventi.html',
            resolve: {
                interventi: ['$stateParams','INTERVENTO_CASETTA',
                    function ($stateParams,INTERVENTO_CASETTA)
                    {
                        return INTERVENTO_CASETTA.query({limit:10,casetta:$stateParams.id});
                }],
                operatori: ['AUTH0',
                    function (AUTH0)
                    {
                        return AUTH0.operatoriCasette();
                }]
            },
            controller: 'casette'
        })
        .state('casetta.video', {
            url: '/video',
            templateUrl: '/html/views/casetta-video.html',
            resolve: {
                video: ['$stateParams','VIDEO_CASETTA',
                    function ($stateParams,VIDEO_CASETTA)
                    {
                        return VIDEO_CASETTA.query();
                }]
            },
            controller: 'casette'
        })
        .state('casetta.multimedia', {
            url: '/multimedia',
            templateUrl: '/html/views/casetta-multimedia.html',
            controller: 'casette'
        })
        .state('casetta.consumi', {
            url: '/consumi',
            templateUrl: '/html/views/casetta-consumi.html',
            controller: 'casette'
        })
        .state('casette-report', { 

            url: '/casette-report',
            templateUrl: '/html/views/casette-report-interventi.html',
            resolve: {
                casette: ['CASETTA',
                    function (CASETTA)
                    {
                        return CASETTA.query();
                }],
                casetta: function () {
                    return {};
                },
                interventi: function () {
                    return [];
                }, // qui non ho la lista
                operatori: function () {
                    return [];
                }, // qui non ho la lista
                video: function () {
                    return [];
                }, //
                consumi: function () {
                    return [];
                }, //
                letture: function () {
                    return [];
                } //
            },
            controller: 'casette'

        })
        .state('casette-report.consumi', {
            url: '/consumi',
            templateUrl: '/html/views/casette-report-consumi.html',
            controller: 'casette',
            resolve: {
                casette: ['CASETTA',
                function (CASETTA)
                {
                    return CASETTA.query();
                }],
                casetta: function () {
                    return {};
                },
                interventi: function () {
                    return [];
                },
                operatori: function () {
                    return [];
                },
                video: function () {
                    return [];
                },  
                consumi: function () {
                    return [];
                },
                letture: function () {
                    return [];
                } // 
            },
        })
        .state('casette-report.risparmio', {
            url: '/risparmio',
            templateUrl: '/html/views/casette-report-risparmio.html',
            controller: 'casette',
            resolve: {
                casette: ['CASETTA',
                function (CASETTA)
                {
                    return CASETTA.query();
                }],
                casetta: function () {
                    return {};
                },
                interventi: function () {
                    return [];
                },
                operatori: function () {
                    return [];
                },
                video: function () {
                    return [];
                },  
                consumi: function () {
                    return [];
                },
                letture: function () {
                    return [];
                } // 
            },
        })
        .state('casetta.letture', {
            url: '/lettura',
            templateUrl: '/html/views/casetta-letture.html',
            resolve: {
                letture: ['$stateParams','LETTURA_CASETTA',
                    function ($stateParams,LETTURA_CASETTA)
                    {
                        return LETTURA_CASETTA.query({limit:10,casetta:$stateParams.id});
                }]
            },
            controller: 'casette'
        })
        /* PAGINE  */
        .state('pagine', {
                url: '/pagine',
                templateUrl: '/html/views/pagine.html',
                controller: 'pagine',
                resolve: {
                    pagine: ['PAGINA',
                        function (PAGINA)
                        {
                            return PAGINA.query();
                    }],
                    pagina: function () {
                        return {};
                    }
                },
        })
        .state('pagina', {
            url: '/pagina/:id',
            templateUrl: '/html/views/pagina.html',
            controller: 'pagine',
            resolve: {
                pagine: function () {
                    return [];
                }, // qui non ho la lista
                pagina: ['$stateParams', 'PAGINA',
                    function ($stateParams, PAGINA)
                    {
                        return $stateParams.id ? PAGINA.get({
                            id: $stateParams.id
                        }) : {contenuti:[]}; // nuova o esistente
                }]
            },
        })
        /* MENUES  */
        .state('menues', {
                url: '/menues',
                templateUrl: '/html/views/menues.html',
                controller: 'menues',
                resolve: {
                    menues: ['MENU',
                        function (MENU)
                        {
                            return MENU.query();
                    }],
                    menu: function () {
                        return {};
                    }
                },
        })
        .state('menu', {
            url: '/menu/:id',
            templateUrl: '/html/views/menu.html',
            controller: 'menues',
            resolve: {
                menues: function () {
                    return [];
                }, // qui non ho la lista
                menu: ['$stateParams', 'MENU',
                    function ($stateParams, MENU)
                    {
                        return $stateParams.id ? MENU.get({
                            id: $stateParams.id
                        }) : {}; // nuovo o esistente
                }]
            },
        })
        /* LOGS  */
        .state('logs', {
            url: '/logs',
            templateUrl: '/html/views/logs.html',
            controller: 'logs',
            resolve: {
                logs: ['LOG',
                        function (LOG)
                    {
                        return LOG.query();
                    }],
                logcodes:['LOG',
                        function (LOG)
                    {
                        return LOG.codes();
                    }]
            },
        })
        /* FAQS  */
        .state('faqs', {
                url: '/faqs',
                templateUrl: '/html/views/faqs.html',
                controller: 'faqs',
                resolve: {
                    faqs: ['FAQ',
                        function (FAQ)
                        {
                            return FAQ.query();
                        }
                    ],
                    faq: function () {
                        return {};
                    }
                },
        })
        .state('faq', {
            url: '/faq/:id',
            templateUrl: '/html/views/faq.html',
            controller: 'faqs',
            resolve: {
                faqs: function () {
                    return [];
                },
                faq: ['$stateParams', 'FAQ',
                    function ($stateParams, FAQ)
                    {
                        return $stateParams.id ? FAQ.get({id: $stateParams.id}) : {};
                    }
                ]
            },
        })
         /* GLOSSARIO  */
        .state('glossari', {
                url: '/glossari',
                templateUrl: '/html/views/glossari.html',
                controller: 'glossario',
                resolve: {
                    glossari: ['GLOSSARIO',
                        function (GLOSSARIO)
                        {
                            return GLOSSARIO.query();
                        }
                    ],
                    glossario: function () {
                        return {};
                    }
                },
        })
        .state('glossario', {
            url: '/glossario/:id',
            templateUrl: '/html/views/glossario.html',
            controller: 'glossario',
            resolve: {
                glossari: function () {
                    return [];
                },
                glossario: ['$stateParams', 'GLOSSARIO',
                    function ($stateParams, GLOSSARIO)
                    {
                        return $stateParams.id ? GLOSSARIO.get({id: $stateParams.id}) : {};
                    }
                ]
            },
        })
         /* SPORTELLI  */
        .state('sportelli', {
            url: '/sportelli',
            templateUrl: '/html/views/sportelli.html',
            controller: 'sportelli',
            resolve: {
                sportelli: ['SPORTELLO',
                   function (SPORTELLO)
                    {
                        return SPORTELLO.query();
                    }],
                sportello: function () {
                    return {};
                }
            }
        }) 
        .state('sportello', {
            url: '/sportello/:id',
            templateUrl: '/html/views/sportello.html',
            controller: 'sportelli',
            resolve: {
                sportelli: function () {
                    return [];
                }, 
                sportello: ['$stateParams', 'SPORTELLO',
                    function ($stateParams, SPORTELLO)
                    {
                        return $stateParams.id ? SPORTELLO.get({id: $stateParams.id}) : {};  
                    }
                ]
            },
        })
        .state('sportello.immagini', {
            url: '/immagini',
            templateUrl: '/html/views/sportello-immagini.html',
            controller: 'sportelli'
        })
        .state('avvisi', {
            url: '/avvisi',
            templateUrl: '/html/views/avvisi.html',
            controller: 'avvisi',
            resolve: {
                avvisi: ['AVVISO',
                   function (AVVISO)
                    {
                        return AVVISO.query();
                    }],
                avviso: function () {
                    return {};
                }
            }
        }) 
        .state('avviso', {
            url: '/avviso/:id',
            templateUrl: '/html/views/avviso.html',
            controller: 'avvisi',
            resolve: {
                avvisi: function () {
                    return [];
                },
                avviso: ['$stateParams', 'AVVISO',
                    function ($stateParams, AVVISO)
                    {
                        return $stateParams.id ? AVVISO.get( {id: $stateParams.id} ) : {};  
                    }
                ]
            },
        })
        .state('laboratorio-regole', {
            url: '/laboratorio-regole',
            templateUrl: '/html/views/laboratorio-regole.html',
            controller: 'laboratori',
            resolve: {
                regole: ['LABORATORIO',
                   function (LABORATORIO)
                    {
                        return LABORATORIO.listaRegole();
                    }],
                analisi:
                    function () {
                        return [];
                    }
            }
        })
        .state('laboratorio-analisi', {
            url: '/laboratorio-analisi',
            templateUrl: '/html/views/laboratorio-analisi.html',
            controller: 'laboratori',
            resolve: {
                regole:
                    function () {
                        return [];
                    },
                analisi: ['LABORATORIO',
                   function (LABORATORIO)
                    {
                        return LABORATORIO.listaAnalisi();
                    }]
            }
        })

        .state('articoli', {
                url: '/articoli',
                templateUrl: '/html/views/articoli.html',
                controller: 'articoli',
                resolve: {
                    articoli: ['ARTICOLO',
                        function (ARTICOLO)
                        {
                            return ARTICOLO.query();
                        }
                    ],
                    articolo: function () {
                        return {};
                    }
                },
        })
        .state('articolo', {
            url: '/articolo/:id',
            templateUrl: '/html/views/articolo.html',
            controller: 'articoli',
            resolve: {
                articoli: function () {
                    return [];
                }, 
                articolo: ['$stateParams', 'ARTICOLO',
                    function ($stateParams, ARTICOLO)
                    {
                        return $stateParams.id ? ARTICOLO.get({id: $stateParams.id}) : {stato:'Bozza'};  
                    }
                ]
            },
        })
        .state('articolo.immagini', {
            url: '/immagini',
            templateUrl: '/html/views/articolo-immagini.html',
            controller: 'articoli'
        })
        .state('articolo.allegati', {
            url: '/allegati',
            templateUrl: '/html/views/articolo-allegati.html',
            controller: 'articoli'
        })
        /* NEWSLETTER  */
        .state('newsletters', {
                url: '/newsletters',
                templateUrl: '/html/views/newsletters.html',
                controller: 'newsletters',
                resolve: {
                    newsletters: ['NEWSLETTER',function (NEWSLETTER){
                        return NEWSLETTER.query({
                            anno: moment().format('YYYY') // di default apro sull'anno corrente
                        });
                    }],
                    newsletter: function () {
                        return {};
                    }
                },
        })
        .state('newsletter', {
            url: '/newsletter/:id',
            templateUrl: '/html/views/newsletter.html',
            controller: 'newsletters',
            resolve: {
                newsletters: function () {
                    return [];
                }, 
                newsletter: ['$stateParams', '$rootScope', 'NEWSLETTER',
                    function ($stateParams, $rootScope , NEWSLETTER)
                    {  
                        return $stateParams.id ? NEWSLETTER.get({id: $stateParams.id}) : $rootScope.newsletter;  
                    }
                ]
            }
        })
        
        .state('stats-richiestaassistenza', {
            url: '/stats-richiestaassistenza',
            templateUrl: '/html/views/stats-richiestaassistenza-data.html',
            controller: 'stats'
        })
        .state('stats-richiestaassistenza-tipo', {
            url: '/stats-richiestaassistenza-tipo',
            templateUrl: '/html/views/stats-richiestaassistenza-tipo.html',
            controller: 'stats'
        })
            
        /* LOGIN  */
        .state('login', {
            url: '/login',
            templateUrl: '/html/views/login.html',
            controller: 'login',
        });

        jwtOptionsProvider.config({
          tokenGetter: function() {
            return localStorage.getItem('id_token');
          }
        });
    
        $httpProvider.interceptors.push('jwtInterceptor');
    // $httpProvider.interceptors.push('jwtInterceptor');



})
.run(function ($rootScope,$state,$location,$timeout) {

  $rootScope.user = JSON.parse(localStorage.getItem('profile')) || {};

  $rootScope.logout = function () {
    localStorage.removeItem('id_token');
    localStorage.removeItem('profile');
    $rootScope.login = true;
    $rootScope.isAutenticated = false;
    $rootScope.user = null;
    $state.go('login');
  }

  // console.log('run url', window.location.protocol + "//" + window.location.host + "/#login")
  $rootScope.lock = new Auth0Lock('ypulY5XgjrfZ6NmoaoepJ2DY25VmypwY', 'plurih2o.eu.auth0.com', {
    auth: {
      domain: 'plurih2o.eu.auth0.com',
      audience: 'https://plurih2o.eu.auth0.com/api/v2/',
      issuer: 'https://plurih2o.eu.auth0.com',
      clientID: 'ypulY5XgjrfZ6NmoaoepJ2DY25VmypwY',
      scope: 'openid profile email',
      responseType: 'token id_token',
      redirect: true,
      redirectUrl: window.location.origin,
      params: {
        scope: 'openid email user_metadata app_metadata picture'
      }
  }})

  var dateNow = new Date().getTime();
  if (dateNow > $rootScope.user.expires) $rootScope.logout()

  $rootScope.lock.on('authenticated', function (response) {
    $rootScope.lock.hide()
    $rootScope.lock.getUserInfo(response.accessToken, function (err, profile) {
      if (err) {
        console.log('errore login', err)
      } else {
        // console.log('login ok', profile)
        const namespace = "https://plurih2o-backend-it/"; // See Auth0 rules
        profile.roles = profile[namespace + "app_metadata"].roles
        profile.tenant = profile[namespace + "app_metadata"].tenant
        profile.name = profile[namespace + "user_metadata"].name
        profile.app_metadata = profile[namespace + "app_metadata"]
        profile.user_metadata = profile[namespace + "user_metadata"]
        profile.expires = 36000 * 1000 + new Date().getTime(); // 12h modificare il 36000 in base all'expired impostato su Auth0
        var user_profile = JSON.stringify(profile),
          json_user_profile = JSON.parse(user_profile),
          roles = json_user_profile.roles;
        // console.log('json_user_profile', json_user_profile)
        // console.log('response', response)
        localStorage.setItem('id_token', response.idToken);
        $rootScope.isAutenticated = true;
        localStorage.setItem('profile', user_profile);
        $rootScope.user = json_user_profile

        if (roles.indexOf('operatore_casette') > -1)
          $state.go('dashboard_operatore_casette');
        else
          $state.go('dashboard');
      }
    })
  })

  // verifica permessi
  $rootScope.$on('$locationChangeStart', function (event) {
    $rootScope.state = $state;
    $timeout(function () {
      if ((!$rootScope.user || !$rootScope.user.name) && $state.current.name==='')
        $state.go('login');
    }, 500)
  });

  // per stampare o meno la navbar
  $rootScope.$on('$stateChangeSuccess', function (scope, current, pre, CONFIG) {
        if ($state.is('login')) 
            $rootScope.login = true;
        else {
            $rootScope.login = false;
            manageInfoTenant($rootScope);

            if ('serviceWorker' in navigator) {
                navigator.serviceWorker.getRegistrations().then(function(registrations) {
                 //console.log("n registration", registrations) 
                for(let registration of registrations) {
                        registration.unregister()
                }}).catch(function(err) {
                    console.log('Service Worker registration failed: ', err);
                });
            }
/* 
            if( $rootScope.user.app_metadata.roles && $rootScope.user.app_metadata.roles.indexOf('operatore_casette')>0) {
                if ('serviceWorker' in navigator) {
                    navigator.serviceWorker.register('/service-worker.js');
                }
            }  */
        }
  });

  $rootScope.logout = function () {
    localStorage.removeItem('id_token');
    localStorage.removeItem('profile');
    $rootScope.login = true;
    $rootScope.isAutenticated = false;
    $rootScope.user = null;
    $state.go('login');
  }

})

.config(function ($datepickerProvider,$collapseProvider) {
    angular.extend($datepickerProvider.defaults, {
        dateFormat: 'dd/MM/yyyy',
        startWeek: 1
    });
    angular.extend($collapseProvider.defaults, {
        animation: 'am-flip-x'
    });
})

