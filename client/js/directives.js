/**
 * sideNavigation - Directive for run metsiMenu on sidebar navigation
 */
function sideNavigation($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element) {
            // Call the metsiMenu plugin and plug it to sidebar navigation
            $timeout(function () {
                element.metisMenu();
            });
        }
    };
};
/**
 * minimalizaSidebar - Directive for minimalize sidebar
*/
function minimalizaSidebar($timeout) {
    return {
        restrict: 'A',
        template: '<a class="navbar-minimalize minimalize-styl-2 btn btn-default " href="" ng-click="minimalize()"><i class="fa fa-bars"></i></a>',
        controller: function ($scope, $element) {
            $scope.minimalize = function () {
                $("body").toggleClass("mini-navbar");
                if (!$('body').hasClass('mini-navbar') || $('body').hasClass('body-small')) {
                    // Hide menu in order to smoothly turn on when maximize menu
                    $('#side-menu').hide();
                    // For smoothly turn on menu
                    setTimeout(
                        function () {
                            $('#side-menu').fadeIn(400);
                        }, 200);
                } else if ($('body').hasClass('fixed-sidebar')){
                    $('#side-menu').hide();
                    setTimeout(
                        function () {
                            $('#side-menu').fadeIn(400);
                        }, 100);
                } else {
                    // Remove all inline style from jquery fadeIn function to reset menu state
                    $('#side-menu').removeAttr('style');
                }
            }
        }
    };
};

/**
 * fitHeight - Directive for set height fit to window height
 */
function fitHeight(){
    return {
        restrict: 'A',
        link: function(scope, element) {
            angular.element(element).css('min-height','100vh');
        }
    };
}

function fastclick(){
    return {
        restrict: 'A',
        link: function(scope, element) {
            FastClick.attach(document.body);
        }
    };
}

function menusize(){
    return {
        restrict: 'A',
        link: function(scope, element) {
            window.addEventListener('resize', function (e) {
                if (this.innerWidth < 801) {
                    document.body.classList.add('body-small')
                } else {
                    document.body.classList.remove('body-small')
                }
            })
            window.addEventListener('load', function (e) {
                if (this.innerWidth < 801) {
                    document.body.classList.add('body-small')

                } else {
                    document.body.classList.remove('body-small')
                }
            })
        }
    };
}


/**
 *
 * Pass theme functions into module
 */

app
    .directive('sideNavigation', sideNavigation)
    .directive('minimalizaSidebar', minimalizaSidebar)
    .directive('altezza', fitHeight)
    .directive('fastclick', fastclick)
    .directive('menusize', menusize)
    .directive('statobando', function () { // stampa il l'etichetta dello stato di un bando in base alla sua data di scadenza
        return {
            restrict: 'E',
            replace: true,
            scope: {
                scadenza: '@',
            },
            link: function (scope, element, attrs) {
                scope.scaduto = moment(scope.scadenza).isBefore(new Date(), 'hour');
            },
            template: '<span>\
                       <span ng-if="scaduto" class="label label-warning">Scaduto</span>\
                       <span ng-if="!scaduto" class="label label-success">Pubblicato</span>\
                       </span>',

        }
    })
    .directive('statoesito', function () { // stampa il l'etichetta dello stato di un esito
        return {
            restrict: 'E',
            replace: true,
            scope: {
                stato: '@',
            },
            template: '<span><span ng-if="stato==\'Assegnato\'" class="label label-success">{{stato}}</span>\
                       <span ng-if="stato!==\'Assegnato\'" class="label label-warning">{{stato}}</span></span>'
        }
    })
    .directive('statocantiere', function () { // stampa il l'etichetta dello stato di un statocantiere in base al suo avanzamento
        return {
            restrict: 'E',
            replace: true,
            scope: {
                avanzamento: '@',
            },
            template: '<span><span ng-if="avanzamento==0" class="label label-danger">Lavori previsiti a breve</span>\
                       <span ng-if="avanzamento==100" class="label label-success">Lavori conclusi</span>\
                       <span ng-if="avanzamento > 0 && avanzamento < 100" class="label label-warning">Lavori in corso</span></span>'
        }
    })
    .directive('statocasetta', function () { // stampa lo stato di una casetta con una label
        return {
            restrict: 'E',
            replace: true,
            scope: {
                stato: '@',
            },
            template: '<span><span ng-if="stato==\'Attiva\'" class="label label-success">{{stato}}</span>\
                       <span ng-if="stato===\'In manutenzione\'" class="label label-danger">{{stato}}</span>\
                       <span ng-if="stato===\'Da attivare\'" class="label label-warning">{{stato}}</span>\
                       </span> '
        }
    })
    .directive('statovideocasetta', function () { // stampa lo stato di un video di una casetta (controlla il tempo dall'ultimo log)
        return {
            restrict: 'E',
            replace: true,
            scope: {
                casetta: '=',
                ultimaChiamata: '@', // data ultimo chiamata pagina
                intervallo: '@' // ogni quanto la pagina viene refreshata
            },
            template: '<span ng-class="{\'text-success\': status === 10, \'text\': status === 20,\'text-danger\': status === 30 }">\
                       <i class="fa fa-circle" ng-if="status === 10 || status === 30"></i><span ng-if="status === 20">-</span>\</span>',
            link: function (scope){
                var log = moment(scope.ultimaChiamata),
                    now = moment(),
                    intervalloInMinuti = scope.intervallo / 1000 / 60;

                // console.log('CASETTA', scope.casetta.descrizione)
                // console.log('iot', scope.casetta.iot)
                // console.log('monitor', scope.casetta.monitor)
                // console.log('stato', scope.casetta.stato)
                // console.log('attiva', scope.casetta.stato==='Attiva')
                // console.log('ultima chiamata', scope.ultimaChiamata)
                // console.log('now', now)
                // console.log('log', log)
                // console.log('intervallo', intervalloInMinuti)
                // console.log('controllo', now.diff(log,'minutes')>=intervalloInMinuti)
                
                if(scope.casetta.iot && (scope.casetta.monitor || scope.casetta.tipo === 'erogatore') && scope.casetta.stato==='Attiva') {

                    // console.log('1')

                    if(!scope.ultimaChiamata) {
                        console.log('2')
                        scope.status = 30; // problema
                    }

                    else {
                        // console.log('3')
                        if(now.diff(log,'minutes')>intervalloInMinuti) {// ultima chimata entro...
                           //  console.log('4')
                            scope.status = 30; // problema
                        } else {
                            // console.log('5')
                            scope.status = 10; //ok                            
                        }
                    }
                }
                else {
                    // console.log('6')
                    scope.status = 20
                }

                // console.log('status', scope.status)
                // console.log('------------------------------------------')
            }
        }
    })
    .directive('statoarticolo', function () { // stampa lo stato di un articolo con una label
        return {
            restrict: 'E',
            replace: true,
            scope: {
                stato: '@',
            },
            template: '<span><span ng-if="stato==\'Bozza\'" class="label label-danger">{{stato}}</span>\
                       <span ng-if="stato===\'In revisione\'" class="label label-warning">{{stato}}</span>\
                       <span ng-if="stato===\'Revisionato\'" class="label label-info">{{stato}}</span>\
                       <span ng-if="stato===\'Pubblicato\'" class="label label-success">{{stato}}</span>\
                       </span> '
        }
    })
    .directive('cloudinary', function () { // stampa il tag img di una foto su cloudinary
        return {
            restrict: 'E',
            replace: true,
            scope: {
                id: '@',
                transformation: '@',
                bootstrapclass: '@'
            },
            template: '<img ng-src="https://res.cloudinary.com/plurimedia/image/upload/{{transformation}}/{{id}}" class="{{bootstrapclass}}"/>'
        }
    });
    
