app.controller('avvisi', [
	'$scope','$rootScope', '$state', 'ngNotify', 'CONFIG', 'UTILITIES', 'AVVISO',  'avviso',
    function ($scope,$rootScope, $state,  ngNotify, CONFIG, UTILITIES, AVVISO, avviso)
    { 
        var pagina = $state.current.url, 
            utente = $rootScope.user.user_metadata.name;
        $scope.avviso = avviso; // dal resolve del router
        //$scope.avvisi = avvisi; // dal resolve del router
        $scope.state = $state;
        // richiamo nello scope tutte le funzioni delle utilities in modo da poterle usare nelle view
        $scope.UTILITIES = UTILITIES; 
        var  toolBar = [['Source', 'Bold', 'Italic', 'Link', 'NumberedList', 'BulletedList', 'PasteText', 'PasteFromWord']];
        $scope.editorOptionsAvviso = UTILITIES.ckEditor("small", toolBar); 
         
        $scope.salvaAvviso = function () 
        {
          if (!$state.params.id) 
          {
            
            $scope.avviso.sezione='sportelli'

            AVVISO.save($scope.avviso, 
              function (avviso) 
              {
                ngNotify.set(CONFIG.messages.avviso_saved, 'success');
                //attualmente esiste solo un avviso nella sezione sportelli
                //se ce ne saranno altri in varie sezioni gestire il return allo stato di partenza
               // $state.go('sportelli');
              },
              function (err) 
              {
                console.log(err)
                UTILITIES.errorMessage(pagina,utente,err.data);
              });
          }
          else 
          {
            $scope.avviso.$update(
            function () 
            {
              ngNotify.set(CONFIG.messages.avviso_updated, 'success');
              
              //attualmente esiste solo un avviso nella sezione sportelli
              //se ce ne saranno altri in varie sezioni gestire il return allo stato di partenza
              //$state.go('sportelli');
                
              },
              function (err) {
                  UTILITIES.errorMessage(pagina,utente,err.data);
              }
            )
          }
        } 
 

}]);
