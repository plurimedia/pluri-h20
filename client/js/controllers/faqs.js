app.controller('faqs', [
	'$scope','$rootScope','$location','$window','$http','$sce','$compile', '$state', '$modal', 'ngNotify', 'CONFIG', 'UTILITIES', 'FAQ', 'faq', 'faqs',
    function ($scope,$rootScope,$location,$window,$http,$sce,$compile, $state, $modal, ngNotify, CONFIG, UTILITIES, FAQ, faq, faqs)
    {

        var pagina = $state.current.url,
            utente = $rootScope.user.user_metadata.name;

        $scope.faq = faq; // dal resolve del router
        $scope.faqs = faqs; // dal resolve del router

        // richiamo nello scope il service degli stati in modo da poterlo usare nelle view
        $scope.state = $state;
        // richiamo nello scope tutte le funzioni delle utilities in modo da poterle usare nelle view
        $scope.UTILITIES = UTILITIES; 
        $scope.editorOptions = UTILITIES.ckEditor(); 
      	 
        $scope.salvaFaq = function () 
        {
          if (!$state.params.id) 
          {
            FAQ.save($scope.faq, 
              function (faq) 
              {
                ngNotify.set(CONFIG.messages.faq_saved, 'success');
                $state.go('faqs');
              },
              function (err)
              {
                  UTILITIES.errorMessage(pagina,utente,err.data);
              }
            )
          }
          else
          {
            $scope.faq.$update(
              function () 
              {
                  ngNotify.set(CONFIG.messages.faq_updated, 'success');
                  $state.go('faq'); 
              },
              function (err) 
              {
                  UTILITIES.errorMessage(pagina,utente,err.data);
              }
            )
          }
        }

        $scope.cercaFaq = function () 
        {
          $scope.faqs = FAQ.query($scope.cerca,
            function () 
            {
                $scope.searchresults = true;
            }
          )
        } 
     
        $scope.eliminaFaq = function () 
        {
          bootbox.confirm({
            title: CONFIG.messages.faq_delete_confirm_title,
            message: CONFIG.messages.faq_delete_confirm_message,
            buttons: UTILITIES.confirmButtons,
            callback: function (ok) {
              if (ok) {
                $scope.faq.$delete(
                  function (err) 
                  {
                    if (err)
                        UTILITIES.errorMessage(pagina,utente,err.data);
                    ngNotify.set(CONFIG.messages.faq_deleted, 'success');
                    $state.go('faqs');
                  }
                )
              }
            }
          })
        }

}]);
