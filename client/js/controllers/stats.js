app.controller('stats', [
    '$scope',
    '$state',
    '$rootScope',
    'ngNotify',
    'CONFIG',
    'STATS','UTILITIES',
    function ($scope,$state,$rootScope,ngNotify,CONFIG, STATS,UTILITIES) {

        $scope.UTILITIES = UTILITIES; 
        $scope.anni = UTILITIES.anniFiltro();
        $scope.mesi = UTILITIES.mesi();

        if (!$scope.dwhAnno)
            $scope.dwhAnno = String((new Date()).getFullYear())

        $scope.bgColor = [ 
            "#7FFFD4","#FBCEB1","#FFBF00","#FFA500",
            "#ABCDEF","#FC6C85","#FFDEAD","#1E90FF",	
            "#ABCDEF","#87A96B","#FFDEAD","#1E90FF",	
            "#CD5C5C","#F7E89F","#CC8899","#77DD77",
            "#0000FF","#800000","#D8BFD8"
        ],
        $scope.bColor = 'blue';

        var myChart;
        var ctx; 
        
        $scope.manageDwh = function()
        {   
            $scope.totaleRichieste = 0;
            $scope.m;
            if ($scope.dwhMese)
                $scope.m = $scope.dwhMese.id
            else
                $scope.m=0 

           if  ($state.is('stats-richiestaassistenza')){
                STATS.richiestaAssistenza({anno:$scope.dwhAnno, mese:$scope.m}, 
                    function(result) {
                        $scope.richiesteAssistenza_data = result.map(function(a) {
                            $scope.totaleRichieste = $scope.totaleRichieste + a.count 
                            return a.count;
                        });

                        $scope.labels_data = result.map(function(a) { 
                            return UTILITIES.nomeMese(a._id.month);
                        }); 

                        $scope.typeBar = 'polarArea'
                        $scope.displayScales = false
                        $scope.legendProp = { 
                            position: 'right',
                            display: true,
                            labels: {
                                fontColor: 'rgb(255, 99, 132)'
                            }
                        } 
                         
                        if ($scope.dwhMese)
                        {
                            $scope.typeBar = 'bar',
                            $scope.labels_data = result.map(function(a) { 
                                return a._id.week;
                            }); 
                            $scope.displayScales = true 
                            $scope.legendProp = { 
                                position: 'top',
                                display: false,
                                labels: {
                                    fontColor: 'rgb(255, 99, 132)'
                                }
                            }
                        }
                    
                        var config = {
                           type:$scope.typeBar,
                            data: {
                                labels:  $scope.labels_data,
                                datasets: [ {
                                    label: "Richieste assistenza in questa settimana",
                                    data: $scope.richiesteAssistenza_data ,
                                    fill: true,
                                    backgroundColor: $scope.bgColor,
                                    borderColor: $scope.bColor, 
                                }]
                            },
                            options: {
                                responsive: true,
                                legend: $scope.legendProp,
                                hover: {
                                    mode: 'label'
                                },
                                scales: {
                                    xAxes: [{
                                        display: $scope.displayScales,
                                        scaleLabel: {
                                            display: true,
                                            labelString: "Settimana dell'anno"
                                        }
                                    }],
                                    yAxes: [{
                                        display: $scope.displayScales,
                                        ticks: {
                                            beginAtZero: true
                                        },
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'Numero'
                                        }
                                    }]
                                }
                            }
                        }; 

                        if (myChart) myChart.destroy();
                        ctx = document.getElementById("graficoRichiestaAssistenza_data").getContext("2d");
                        myChart = new Chart(ctx, config);
                    }
                )
            }
            if  ($state.is('stats-richiestaassistenza-tipo')){

                STATS.richiestaAssistenzaTipo({anno:$scope.dwhAnno,mese:$scope.m}, 
                    function(result) { 
                        $scope.richiesteAssistenza_tipo = result.map(function(a) {
                            $scope.totaleRichieste = $scope.totaleRichieste + a.count 
                            return a.count;
                        });
                        $scope.tipoRichiesta = result.map(function(a) {return a._id;}); 

                        var config = {
                          //  type: 'bar',
                           // type:'polarArea',
                          // type: 'line',
                          type:'horizontalBar', 
                            data: {
                                labels:  $scope.tipoRichiesta,
                                datasets: [ {
                                    label: "Richieste assistenza per tipo servizio",
                                    data: $scope.richiesteAssistenza_tipo ,
                                    fill: true,
                                    backgroundColor: $scope.bgColor,
                                    borderColor: $scope.bColor
                                }]
                            },
                            options: {
                                responsive: true,
                                legend: { 
                                    position: 'right',
                                    display: false,
                                    labels: {
                                        fontColor:'rgb(255, 99, 132)'
                                    }
                                },
                                hover: {
                                    mode: 'label'
                                },
                                scales: {
                                    xAxes: [{
                                        display: true,
                                        ticks: {
                                            beginAtZero: true,
                                            //steps: 10,
                                            //stepValue: 5
                                        },
                                        scaleLabel: {
                                            display: false,
                                            labelString: 'Tipo servizio'
                                        }
                                    }],
                                    yAxes: [{
                                        display: true,
                                        
                                        scaleLabel: {
                                            display: false,
                                            labelString: 'Numero'
                                        }
                                    }]
                                }
                            } 
                        };

                        if (myChart) myChart.destroy();
                        ctx = document.getElementById("graficoRichiestaAssistenza_tipo").getContext("2d");
                        myChart = new Chart(ctx, config);
                });
            }
        }
        $scope.manageDwh(); 
  
/*
        document.getElementById("graficoRichiestaAssistenza_data").onclick = function(evt)
        {   
            var activePoints = myChart.getElementsAtEvent(evt);
        
            if(activePoints.length > 0)
            {
              //get the internal index of slice in pie chart
              var clickedElementindex = activePoints[0]["_index"];
        
              //get specific label by index 
              var label = myChart.data.labels[clickedElementindex];
        
              //get value by index      
              var value = myChart.data.datasets[0].data[clickedElementindex];
                
              console.log(clickedElementindex, label, value)

              $scope.dwhMese = { id:1}

              $scope.manageDwh();
           }
        }
*/
  }]);