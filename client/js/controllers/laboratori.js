app.controller('laboratori', [
  '$scope','$rootScope','$state','ngNotify','$modal','CONFIG','UTILITIES','LABORATORIO','XLS_LABORATORIO','XLS','regole','analisi', 
    function ($scope,$rootScope,$state,ngNotify,$modal,CONFIG,UTILITIES,
      LABORATORIO,XLS_LABORATORIO,XLS,regole, analisi
      )
    {  
      var modalEsitoUpload = $modal(
      {
          scope: $scope,
          templateUrl: 'html/modals/laboratorio_UploadAnalisi.html',
          show: false,
          backdrop: 'static'
      });

      var pagina = $state.current.url, 
          utente = $rootScope.user.user_metadata.name;
      $scope.UTILITIES = UTILITIES;
      $scope.anni = UTILITIES.anniFiltro();


      $scope.datiRegole = regole;
      $scope.datiAnalisi = analisi; 

      $scope.aggiungImport = function (analisi) {
        $state.reload()
      }

      $scope.salvaRegole = function()
      {
         $scope.datiRegole.$updateRegole(
            function () 
            {
              ngNotify.set(CONFIG.messages.regole_updated, 'success');
            },
            function (err) {
                UTILITIES.errorMessage(pagina,utente,err.data);
            }
          )
      }

      $scope.uploadAnalisi = function(file)
      {
       if(!file) 
       {
          //ngNotify.set(CONFIG.messages.wrongxls,'warn');
        }
        else 
        {
          $scope.uploadInCorso = true;
          XLS_LABORATORIO.upload(file,'analisiAcqua',
            function (data) 
            { // l'upload richiede un parametro tipo per capire cosa processare
              $scope.uploadInCorso=false;
              $scope.datoAnalisi = data;


              if($scope.datoAnalisi.aNorma){
                $scope.aggiungImport($scope.datoAnalisi)
                ngNotify.set(CONFIG.messages.import_xls_success ,'success');
                
              }

              else {

                $scope.valoriCritici = _.filter($scope.datoAnalisi.analisi, function(d){
                  return !d.aNorma;
                }) 

                modalEsitoUpload.show();
                
              }
            }
          )
        }
      }

      $scope.salvaAnalisi = function()
      {
          modalEsitoUpload.hide();
          $scope.aggiungImport($scope.datoAnalisi)
          ngNotify.set(CONFIG.messages.import_xls_success ,'success');
          $scope.aggiornoVisibilita  = false;
      }

      $scope.cancellaAnalisi = function()
      {
        var tmp = new LABORATORIO($scope.datoAnalisi)
        tmp.$deleteAnalisi(
          function () 
          {
            modalEsitoUpload.hide();
            ngNotify.set(CONFIG.messages.import_xls_nonimportato ,'info');
          },
          function (err) {
              UTILITIES.errorMessage(pagina,utente,err.data);
          }
        )
      }

      $scope.checkSospensione = function () {
        if(_.find($scope.datiAnalisi, function(a){
          return a.sospendi === true;
        })){
           
          $scope.sospese = true;
           return;
        }
      }

      $scope.aggiornaPubblicato = function (id) {

        $scope.aggiornoVisibilita = id;

        _.map($scope.datiAnalisi, function (analisi){

            if(id!==analisi._id)
              analisi.visibile = false;
            else
              analisi.visibile = true;

            return analisi;
        })

        // aggiorno lato server in blocco
        LABORATORIO.updateAnalisi({visibile: id},function(){
          $scope.aggiornoVisibilita = false;
          ngNotify.set(CONFIG.messages.analisi_visibile_aggiornato ,'success');
        },function(err){
          UTILITIES.errorMessage(pagina,utente,err.data);
        })


      }

      $scope.sospendi = function () {

        $scope.sospendo = true;
        $scope.dasospendere = _.find($scope.datiAnalisi, function(a){
          return a.visibile
        })

        $scope.dasospendere.sospendi = true;

         // aggiorno lato server in blocco
        LABORATORIO.updateAnalisi({sospendi: $scope.dasospendere._id},function(){
          $scope.sospendo = false;
          ngNotify.set(CONFIG.messages.analisi_sospese ,'success');
        },function(err){
          UTILITIES.errorMessage(pagina,utente,err.data);
        })       
      }


      $scope.ripristina = function () {

        $scope.ripristino = true;

        $scope.daripristinare = _.find($scope.datiAnalisi, function(a){
          return a.visibile
        })

        $scope.daripristinare.sospendi = false;
         // aggiorno lato server in blocco
        LABORATORIO.updateAnalisi({ripristina: $scope.daripristinare._id },function(){
          $scope.ripristino = false;
          $scope.sospese = false;

          ngNotify.set(CONFIG.messages.analisi_ripristinate ,'success');
          
        },function(err){
          UTILITIES.errorMessage(pagina,utente,err.data);
        })       
      }

      $scope.scarica = function (a) { 

        $scope.scarico = a._id; 
       

        LABORATORIO.get({_id:a._id},function(analisi)
        { 

          var output = _.map(analisi.analisi, function(a){
              var o = {
                comune: a.comune,
                sif: a.codiceSif
              }

              _.map(a.valori, function(v){
                  o[v.nome] = v.valore
              })

              return o;
          });

          $scope.scarico = false;
          $scope.export = XLS.export(output);
        })
      }

      $scope.anno = moment().format('YYYY')

      $scope.cercaAnno = function (a) {
        return moment(a.cdate).format('YYYY') === $scope.anno;
      }



}]);
