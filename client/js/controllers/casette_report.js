app.controller('casette_report', [
  '$scope','$rootScope','$location','$window','Upload','$http','$sce','$compile', '$state', '$modal', 'ngNotify', 'CONFIG', 'UTILITIES',
    function ($scope,$rootScope,$location,$window,Upload,$http,$sce,$compile, $state, $modal, ngNotify, CONFIG, UTILITIES)
    {
 
        var pagina = $state.current.url, 
            utente = $rootScope.user.user_metadata.name;
        
        $scope.state = $state;
        // richiamo nello scope tutte le funzioni delle utilities in modo da poterle usare nelle view
        $scope.UTILITIES = UTILITIES; 
        $scope.editorOptions = UTILITIES.ckEditor();
        $scope.editorOptionsRidotto= UTILITIES.ckEditor('small');
        $scope.anni = UTILITIES.anniFiltro();
          

}]);
