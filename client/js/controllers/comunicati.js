app.controller('comunicati', [
	'$scope','$rootScope','$http', 'Upload', '$state', '$modal', 'ngNotify', 'CONFIG', 'UTILITIES', 'COMUNICATO', 'comunicato', 'comunicati',
    function ($scope,$rootScope ,$http, Upload, $state, $modal, ngNotify, CONFIG, UTILITIES, COMUNICATO, comunicato, comunicati)
    {

        var modalCategoriaUpload = $modal({
                scope: $scope,
                templateUrl: 'html/modals/categoria_upload.html',
                show: false
            }),
            modalUpload = $modal({
                scope: $scope,
                templateUrl: 'html/modals/upload.html',
                show: false,
                backdrop: 'static'
            }),
            modalUploadImmagini = $modal({
                scope: $scope,
                templateUrl: 'html/modals/upload_immagini.html',
                show: false,
                backdrop: 'static'
            }),
            // per i messaggi di errore
            pagina = $state.current.url,
            utente = $rootScope.user.user_metadata.name;

        $scope.comunicato = comunicato; // dal resolve del router
        $scope.comunicati = comunicati; // dal resolve del router

        $scope.editorOptions = UTILITIES.ckEditor();
        $scope.editorEestrattoOptions = UTILITIES.ckEditor('small');

        // richiamo nello scope il service degli stati in modo da poterlo usare nelle view
        $scope.state = $state;
        // richiamo nello scope tutte le funzioni delle utilities in modo da poterle usare nelle view
        $scope.UTILITIES = UTILITIES;
        // anni disponibili per il filraggio
        $scope.anni = UTILITIES.anniFiltro();
        $scope.elencoTag = UTILITIES.elencoTag();

        /*
            @@@@@@@@@@@@@@@@@@  Elenco dei comunicati   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        */


        // cerca comunicati

        $scope.cercaComunicati = function () {
            $scope.comunicati = COMUNICATO.query($scope.cerca, function () {
                $scope.searchresults = true;
            })
        }

        /*
            @@@@@@@@@@@@@@@@@@  Singolo comunicato   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        */



        // salvataggio comunicato
        $scope.salvaComunicato = function (immagini, allegati) {

            if (!$state.params.id) {
                COMUNICATO.save($scope.comunicato, function (comunicato) {
                        ngNotify.set(CONFIG.messages.comunicato_saved, 'success');
                        if (!immagini && !allegati) {
                            $state.go('comunicati');
                        }
                        if (immagini || allegati) {
                            $scope.comunicato = new COMUNICATO(comunicato); // creo una nuova istanza per avere i suoi metodi
                        }
                        // salva e aggiungi immagini
                        if (immagini) {
                            $state.go('comunicato.immagini', {
                                id: comunicato._id
                            });
                        }
                        if (allegati) {
                            $state.go('comunicato.allegati', {
                                id: comunicato._id
                            });
                        }
                    },
                    function (err) {
                        UTILITIES.errorMessage(pagina,utente,err.data);
                    });
            } else {
                $scope.comunicato.$update(function () {
                    ngNotify.set(CONFIG.messages.comunicato_updated, 'success');
                    if (!immagini && !allegati)
                        $state.go('comunicato');
                    if (immagini)
                        $state.go('comunicato.immagini');
                    if (allegati)
                        $state.go('comunicato.allegati');
                },
                function (err) {
                    UTILITIES.errorMessage(pagina,utente,err.data);
                });
            }
        }


        // cancella il comunicato
        $scope.eliminaComunicato = function () {
            // chiedi prima conferma
            bootbox.confirm({
                title: CONFIG.messages.comunicato_delete_confirm_title,
                message: CONFIG.messages.comunicato_delete_confirm_message,
                buttons: UTILITIES.confirmButtons,
                callback: function (ok) {
                    if (ok) {
                        $scope.comunicato.$delete(function (err) {
                            if (err)
                                UTILITIES.errorMessage(pagina,utente,err.data);

                            ngNotify.set(CONFIG.messages.comunicato_deleted, 'success');
                            $state.go('comunicati');
                        });
                    }
                }
            });
        }




        /*
            @@@@@@@@@@@@@@@@@@  Singolo comunicato => immagini  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        */



        // trascinamento dei file: scatta l'upload su cloudinary
        $scope.uploadImmagini = function (files) {

            $scope.immagini = files;
            var caricate = [];

            if ($scope.immagini.length) {
                modalUploadImmagini.show();
                async.each($scope.immagini, function (file, cb) {
                    file.upload = Upload.upload({
                        url: UTILITIES.cloudinary_upload_api,
                        fields: {
                            upload_preset: UTILITIES.cloudinary_upload_preset
                        },
                        file: file,
                        skipAuthorization: true
                    })
                    file.upload.then(

                        function (response) {
                            if (response.status === 200) {
                                caricate.push({
                                    id: response.data.public_id,
                                    name: file.name.substr(0, file.name.lastIndexOf('.'))
                                }); // pusho
                                cb();
                            } else {
                                cb(response);
                            }

                        },
                        function (resp) {
                            console.log('Error status: ' + resp.status);
                        },
                        function (evt) {
                            var index = $scope.immagini.indexOf(file);
                            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                            $scope.immagini[index].progress = progressPercentage; // aggiorno la percentuale del singolo file
                        }
                    );
                }, function (err) {
                    if (err)
                        UTILITIES.errorMessage(pagina,utente,err.data);

                    modalUploadImmagini.hide();
                    $scope.comunicato.immagini = _.sortBy(_.union(caricate, $scope.comunicato.immagini), function (img) {
                        return img.name;
                    }); // aggiungo

                    ngNotify.set(CONFIG.messages.upload_completed, 'success');
                })
            }

        }

        // elimina un'immagine
        $scope.eliminaImmagine = function (index) {
            bootbox.confirm({
                title: CONFIG.messages.image_delete_title,
                message: CONFIG.messages.image_delete_message,
                buttons: UTILITIES.confirmButtons,
                callback: function (ok) {
                    if (ok) {

                        if($scope.comunicato.immagini[index].id === $scope.comunicato.copertina.id) // se non ho cancellato la copertina -> assicurati di cancellare la copertina e rimettere il default
                            delete $scope.comunicato.copertina;

                        $scope.comunicato.immagini.splice(index, 1);

                        if(!$scope.comunicato.immagini.length) // se non ho più immagini -> assicurati di cancellare la copertina e rimettere il default
                            delete $scope.comunicato.copertina;

                        ngNotify.set(CONFIG.messages.image_deleted, 'success');
                    }
                }
            });
        }


        // imposta la copertina
        $scope.copertinaImmagine = function (index) {
            $scope.comunicato.copertina = $scope.comunicato.immagini[index];
        }

        // rimuovi la copertina
        $scope.rimuoviCopertina = function () {
            delete $scope.comunicato.copertina;
        }
        
        // se cambio il nome di un'immagine che è anche copertina, aggiorna la copertina di conseguenza
        $scope.cambiaNomeFoto = function (immagine) {
            if(immagine.id===$scope.comunicato.copertina.id)
                $scope.comunicato.copertina.name = immagine.name;
        }

        /*
                 @@@@@@@@@@@@@@@@@@  Singolo Comunicato => Allegati  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
             */


        // categorie di upload di default
        $scope.categorieUploadComunicati = $scope.comunicato.files || UTILITIES.categorieUploadComunicati(); // i suoi o quelli di sistema

        // modale nuova categoria di upload
        $scope.apriModaleCategoriaUpload = function () {
            modalCategoriaUpload.show();
            // resetta
            $scope.nomeCategoriaUpload = undefined;
        }

        // aggiunge o modifica una categoria
        $scope.salvaCategoria = function (nome) {
            if (!$scope.categoria) { // nuova
                var date = new Date(); // ci metto il timestamp com id univoco
                $scope.categorieUploadComunicati.push({
                    _id: date.valueOf(),
                    nome: nome,
                    type: 'categoria'
                });
            } else { // esistente
                // trovo la posizione di quello da cancellare
                _.find($scope.categorieUploadComunicati, function (cat) {
                    return cat._id === $scope.categoria._id
                }).nome = nome;
            }
            modalCategoriaUpload.hide();
        }

        // cancellazione categoria
        $scope.cancellaCategoria = function (categoria) {
            // trovo la posizione di quello da cancellare
            var cat = _.find($scope.categorieUploadComunicati, function (cat, index) {
                if (cat._id === categoria._id) {
                    $scope.daCancellare = index;
                    return true;
                };
            })

            if (!cat.files) { // se non ho files, cancella senza alert
                $scope.categorieUploadComunicati.splice($scope.daCancellare, 1);
                ngNotify.set(CONFIG.messages.upload_deleted_category, 'success');
            } else {
                bootbox.confirm({
                    title: CONFIG.messages.upload_delete_category_title,
                    message: CONFIG.messages.upload_delete_category_message,
                    buttons: UTILITIES.confirmButtons,
                    callback: function (ok) {
                        if (ok) {
                            $scope.categorieUploadComunicati.splice($scope.daCancellare, 1);
                            ngNotify.set(CONFIG.messages.upload_deleted_category, 'success');
                        }
                    }
                });
            }
        }

        // modifica categoria
        $scope.modificaCategoria = function (categoria) {
            $scope.categoria = categoria;
            $scope.nomeCategoriaUpload = categoria.nome;
            modalCategoriaUpload.show();
        }

        // quando chiudi la modale decontestualizza la categoria
        $scope.$on('modal.hide', function () {
            $scope.categoria = undefined;
        });

        // cancella un file caricato
        $scope.cancellaFile = function (idcategoria, index) {
            bootbox.confirm({
                title: CONFIG.messages.upload_delete_file_title,
                message: CONFIG.messages.upload_delete_file_message,
                buttons: UTILITIES.confirmButtons,
                callback: function (ok) {
                    if (ok) {
                        _.find($scope.categorieUploadComunicati, function (c) {
                            return c._id === idcategoria;
                        }).files.splice(index, 1);

                        ngNotify.set(CONFIG.messages.upload_deleted_file, 'success');
                    }
                }
            });
        }

        // trascinamento dei file: scatta l'upload su S3
        $scope.uploadFiles = function (files, categoria) {

            $scope.files = files;
            var caricati = [];

            if ($scope.files.length) {
                modalUpload.show();
                async.each($scope.files, function (file, cb) {

                    var filetype = file.type !== "" ? file.type : 'application/octet-stream', // mi serve per il signed url
                        percorso = $scope.comunicato._id + '/' + file.name;

                    $http
                        ({
                            url: '/s3',
                            data: {
                                percorso: percorso,
                                tipo: filetype,
                                dimensione: file.size
                            },
                            method: 'POST',
                            skipAuthorization: true // non passo dal autorizzazione con token, s3 si incazza
                        })
                        .success(function (url) {
                            // ho un signed url per l'upload su amazon
                            file.upload = Upload.http({
                                url: url,
                                data: file,
                                method: 'PUT',
                                skipAuthorization: true, // non passo dal autorizzazione con token di auth0, s3 si incazza
                                headers: {
                                    'Content-Type': filetype
                                }
                            });

                            file.upload.then(
                                function (response) {
                                    if (response.status === 200) {
                                        caricati.push({
                                            name: file.name.substr(0, file.name.lastIndexOf('.')), // togli l'estensione
                                            url: response.config.url.split('?')[0],
                                            type: 'file' // mi serve per impedire il drag & drop di un file in una categoria
                                        }); // salvo nome e l'url su s3 di quello caricato
                                        cb();
                                    } else {
                                        cb(response)
                                    }
                                },
                                function (resp) {
                                    console.log('Error status: ' + resp.status);

                                },
                                function (evt) {
                                    var index = $scope.files.indexOf(file);
                                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                                    $scope.files[index].progress = progressPercentage; // aggiorno la percentuale del singolo file


                                }
                            );
                        });
                }, function (err) {
                    if (err) {
                        UTILITIES.errorMessage(pagina,utente,err.data);
                    } else {
                        // aggiorno l'array dei files per categoria (sempre in aggiunta)
                        var target = _.find($scope.categorieUploadComunicati, function (cat) {
                            return cat._id === categoria._id;
                        });

                        target.files = _.sortBy(_.flatten(_.union(target.files, caricati)), function (b) {
                            return b.name;
                        });

                        modalUpload.hide();
                        ngNotify.set(CONFIG.messages.upload_completed, 'success');
                    }
                })
            }

        }


        $scope.salvaAllegati = function () {
            $scope.comunicato.files = $scope.categorieUploadComunicati; // attacco
            $scope.comunicato.$update(function () {
                ngNotify.set(CONFIG.messages.comunicato_updated, 'success');
            },function (err) {
                    UTILITIES.errorMessage(pagina,utente,err.data);
            });
        }
}]);