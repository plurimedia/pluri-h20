app.controller('sportelli', [
	'$scope','$rootScope','$location','$window','Upload','$http','$sce','$compile', '$state', '$modal', 'ngNotify', 'CONFIG', 'UTILITIES', 'SPORTELLO', 'sportelli', 'sportello','AVVISO',
    function ($scope,$rootScope,$location,$window,Upload,$http,$sce,$compile, $state, $modal, ngNotify, CONFIG, UTILITIES, SPORTELLO, sportelli, sportello,AVVISO)
    {

        modalUploadImmagini = $modal({
              scope: $scope,
              templateUrl: 'html/modals/upload_immagini.html',
              show: false,
              backdrop: 'static'
          })

        var pagina = $state.current.url, 
            utente = $rootScope.user.user_metadata.name;
        $scope.sportello = sportello; // dal resolve del router
        $scope.sportelli = sportelli; // dal resolve del router
        $scope.state = $state;
        // richiamo nello scope tutte le funzioni delle utilities in modo da poterle usare nelle view
        $scope.UTILITIES = UTILITIES; 
        var  toolBar = [['Source', 'Bold', 'Italic', 'Link', 'NumberedList', 'BulletedList', 'PasteText', 'PasteFromWord']];
        $scope.editorOptionsOrario = UTILITIES.ckEditor("small", toolBar); 
        $scope.editorOptionsAvviso = UTILITIES.ckEditor("small", toolBar); 
        $scope.editorOptionsDescrizione = UTILITIES.ckEditor();
        $scope.comuni = UTILITIES.comuni;
        $scope.manageIndirizzo = 
          function (sportello) 
          {
            if(sportello.comune)
              return UTILITIES.manageIndirizzo(sportello.comune, sportello.indirizzo);
          }

        $scope.manageComuneDesc = 
          function (sportello) 
          {
            if(sportello.comune)
              return UTILITIES.decodificaComune(sportello.comune);
          }

        $scope.avviso = AVVISO.query(
            function (avvisi) 
            {
              console.log(avvisi)
              result =  _.filter(avvisi, function (av) {
                return av.sezione == 'sportelli'
              });

              $scope.avviso = result[0];
              return result[0];
            }
        )

        $scope.salvaSportello = function (immagini) 
        {
          $scope.sportello.indirizzoCompleto = $scope.manageIndirizzo($scope.sportello);
          $scope.sportello.comuneDesc = $scope.manageComuneDesc($scope.sportello);
          if (!$state.params.id) 
          {
            SPORTELLO.save($scope.sportello, 
              function (sportello) 
              {
                ngNotify.set(CONFIG.messages.sportello_saved, 'success');
                if (!immagini)
                    $state.go('sportelli');
                // salva e aggiungi immagini
                if (immagini) 
                {
                  $state.go('sportello.immagini', 
                  {
                      id: sportello._id
                  });
                }   
              },
              function (err) 
              {
                UTILITIES.errorMessage(pagina,utente,err.data);
              });
          }
          else 
          {

            console.log($scope.sportello.copertina);


            $scope.sportello.$update(
            function () 
            {
              ngNotify.set(CONFIG.messages.sportello_updated, 'success');
                if (!immagini)
                  $state.go('sportello');
                if (immagini)
                  $state.go('sportello.immagini'); 
              },
              function (err) {
                  UTILITIES.errorMessage(pagina,utente,err.data);
              }
            )
          }
        }  
        
        $scope.eliminaSportello = function () 
        {   
          bootbox.confirm({
            title: CONFIG.messages.sportello_delete_confirm_title,
            message: CONFIG.messages.sportello_delete_confirm_message,
            buttons: UTILITIES.confirmButtons,
            callback: 
              function (ok) {
                if (ok) {
                  $scope.sportello.$delete(
                    function (err) 
                    {
                      if (err)
                          UTILITIES.errorMessage(pagina,utente,err.data);
                      ngNotify.set(CONFIG.messages.sportello_deleted, 'success');
                      $state.go('sportelli');
                    }
                  )
                }
              }
            }
          )
        }

        $scope.cercaSportello = function () 
        {
          $scope.sportelli = SPORTELLO.query($scope.cerca,
            function () 
            {
              $scope.searchresults = true;
            }
          )
        } 

        $scope.uploadImmagini = function (files) 
        {
            $scope.immagini = files;
            var caricate = [];
            if ($scope.immagini.length) 
            {
              modalUploadImmagini.show();
              async.each($scope.immagini, function (file, cb) 
              {
                file.upload = Upload.upload({
                    url: UTILITIES.cloudinary_upload_api,
                    fields: {
                        upload_preset: UTILITIES.cloudinary_upload_preset
                    },
                    file: file,
                    skipAuthorization: true
                })
                file.upload.then(
                    function (response) 
                    {
                      if (response.status === 200) {
                          caricate.push({
                              id: response.data.public_id,
                              name: file.name.substr(0, file.name.lastIndexOf('.'))
                          }); // pusho
                          cb();
                      } else
                          cb(response);
                    },
                    function (resp) 
                    {
                        console.log('Error status: ' + resp.status);
                    },
                    function (evt) 
                    {
                        var index = $scope.immagini.indexOf(file);
                        var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                        $scope.immagini[index].progress = progressPercentage; // aggiorno la percentuale del singolo file
                    }
                )
              }, 
              function (err) 
              {
                if (err)
                    UTILITIES.errorMessage(pagina,utente,err.data);
                modalUploadImmagini.hide();
                $scope.sportello.immagini = _.sortBy(_.union(caricate, $scope.sportello.immagini), 
                  function (img) 
                  {
                    return img.name;
                  }
                )
                ngNotify.set(CONFIG.messages.upload_completed, 'success');
              }
            )
          }
        }
        
        $scope.eliminaImmagine = function (index) 
        {
          bootbox.confirm({
            title: CONFIG.messages.image_delete_title,
            message: CONFIG.messages.image_delete_message,
            buttons: UTILITIES.confirmButtons,
            callback: 
              function (ok) 
              {
                if (ok) 
                {

                  console.log($scope.sportello.copertina, !$scope.sportello.immagini.length)

                  if($scope.sportello.copertina && $scope.sportello.immagini[index].id === $scope.sportello.copertina.id) {// se non ho cancellato la copertina -> assicurati di cancellare la copertina e rimettere il default
                    $scope.sportello.copertina = null;
                  }

                  $scope.sportello.immagini.splice(index, 1);

                  if(!$scope.sportello.immagini.length || $scope.sportello.immagini.length==0) {// se non ho più immagini -> assicurati di cancellare la copertina e rimettere il default
                       $scope.sportello.copertina = null;
                  }

                  ngNotify.set(CONFIG.messages.image_deleted, 'success');
                }
              }
            }
          )
        }

        // imposta la copertina
        $scope.copertinaImmagine = function (index) 
        {
            $scope.sportello.copertina = $scope.sportello.immagini[index];
        }

        // rimuovi la copertina
        $scope.rimuoviCopertina = function () 
        {
          $scope.sportello.copertina = null;
        }
        
        // se cambio il nome di un'immagine che è anche copertina, aggiorna la copertina di conseguenza
        $scope.cambiaNomeFoto = function (immagine) 
        {
          if(immagine.id===$scope.sportello.copertina.id)
              $scope.sportello.copertina.name = immagine.name;
        }

}]);
