app.controller('dashboard_operatore_casette', [
	'$scope','$rootScope','$state', '$modal', 'ngNotify', 'CONFIG', 'UTILITIES','INTERVENTO_CASETTA','CASETTA',
    function ($scope,$rootScope,$state, $modal, ngNotify, CONFIG, UTILITIES,INTERVENTO_CASETTA,CASETTA)
    {
        
        
        // modale delle note
        var modalNote = $modal({
                scope: $scope,
                templateUrl: 'html/modals/note_manutenzione.html',
                show: false
            }),
            // per i messaggi di errore
            pagina = $state.current.url,
            utente = $rootScope.user.user_metadata.name;
        
        $scope.tabs = [
            {
                title: "rilevamento",
                description: "Nuovo rilevamento",
                template: '/html/includes/casette_nuovo_rilevamento.html',
            },
            {
                title: "manutenzione",
                description: "Nuova manutenzione",
                template: '/html/includes/casette_nuova_manutenzione.html'
            }
        ];

        // recupero le casette
        $scope.casette = CASETTA.query();
        
        // all'inizio nessuna tab è aperta
        $scope.tabs.activeTab = '';  
        
        $scope.$watch('tabs.activeTab', function (newVal,oldVal){
            if(newVal)
                $scope.initRilevamento(newVal); // se ho aperto una tab, inizializza il rilevamento passandogli il tipo
        })
        
        // inizializza
        $scope.initRilevamento = function (tipo) {
            // recupero l'ultimo rilevamento
            $scope.preview = $scope.success = false;
            $scope.rilevamento = { //NB: tratto rilevamento e manutenzione come un unico rilevamento, differenzio col tipo
                tipo: tipo,
                data: new Date()
            }           
        }
        
        // annulla rilevamento
        $scope.annullaRilevamento = function () {
            $scope.tabs.activeTab = null;// chiudi le tabs
            $scope.preview = $scope.success = false; // annulla l'anteprima e il success
            delete $scope.rilevamento;
        }
        
        // modifica, tiene i dati
        $scope.modificaRilevamento = function () {
            $scope.preview = false; // annulla l'anteprima
        }
        
        // anteprima
        $scope.previewRilevamento = function (){
            $scope.preview = true;
        }
        
        // mette la descrizione della casetta per la preview (altrimenti avrei solo l'id)
        // trova anche l'ultimo rilevamento
        $scope.changeCasetta = function () {
            
            $scope.rilevamento.casetta_descrizione = _.find($scope.casette,function(c){
                return c._id === $scope.rilevamento.casetta;
            }).descrizione;
            // recupero l'ultimo rilevamento
            $scope.ultimo_rilevamento = INTERVENTO_CASETTA.query({limit:1,tipo:$scope.rilevamento.tipo,casetta:$scope.rilevamento.casetta}, function(ultimo){

                if($scope.rilevamento.tipo === 'rilevamento') { // casetta senza alcun rilevamento: metto tutto a zero per fargli fare i conti
                    if(!ultimo || ultimo.length===0) {
                       $scope.ultimo_rilevamento[0] = {
                        ambiente: 0,
                        fredda: 0,
                        gasata: 0,
                        ingresso_casetta: 0,
                        ingresso_acquedotto: 0,
                        lampada_uv: 0
                       }
                    }
                }

                // se è una manutenzione, devo recuperare anche l'ultimo rilevamento per mostrare i litri sosituzione filtro
                if($scope.rilevamento.tipo === 'manutenzione') {

                /*
                  $scope.ultimo_rilevamento_rilevamento = INTERVENTO_CASETTA.query({limit:1,tipo:'rilevamento',casetta:$scope.rilevamento.casetta});
                  if(!$scope.ultimo_rilevamento_rilevamento[0]) {
                    alert('Non è mai stato effettuato un rilevamento per questa casetta, per proseguire inserisci almeno un rilevamento')
                  }
                */
               $scope.ultimo_rilevamento_rilevamento = INTERVENTO_CASETTA.query(
                        {limit:1,tipo:'rilevamento',casetta:$scope.rilevamento.casetta},
                        function(result){
                            if(!result || result.length==0) {
                                alert('Non è mai stato effettuato un rilevamento per questa casetta, per proseguire inserisci almeno un rilevamento')
                            }
                            else{
                                $scope.ultimo_rilevamento_rilevamento = result;
                            }
                        }
                    );
                }  
            });

        }
        
        // quando seleziono la data, metto l'ora corrente, anche per non incasinare la min date con la mezzanotte di default
        $scope.changeData = function () {
            var ora = moment().format('H'),
                minuti = moment().format('m');
            
            $scope.rilevamento.data = moment($scope.rilevamento.data).set('hour',ora).set('minute',minuti).toDate();
        }
        
        // quando ho un ultimo rilevamento, non posso inserire un valore inferiore!
        $scope.min = function (ultimo) {
            return ultimo || 0;
        }

        // salvataggio effettivo
        $scope.salvaRilevamento = function (){    
            
            // il messaggio dipende dal tipo di rilevamento
            var msg = $scope.rilevamento.tipo === 'rilevamento' ? CONFIG.messages.rilevamento_saved : CONFIG.messages.manutenzione_saved;
            // do indicazione del savtaggio
            $scope.saving = true;

            // se sto salvando una manutenzione con la spunta su sostituzione filtro, mi segno i litri alla quale è avvenuta
            if($scope.rilevamento.tipo === 'manutenzione' && $scope.rilevamento.sostituzione_filtro)
                $scope.rilevamento.sostituzione_filtro_litri = $scope.ultimo_rilevamento_rilevamento[0].litri_sostituzione_filtro;

            // se sto salvando una manutenzione con la spunta su sostituzione lampade, mi segno i gg alla quale è avvenuta
            if($scope.rilevamento.tipo === 'manutenzione' && $scope.rilevamento.sostituzione_lampade)
                $scope.rilevamento.sostituzione_lampade_giorni = $scope.ultimo_rilevamento_rilevamento[0].lampada_uv;
            
            // salvo sul server
            INTERVENTO_CASETTA.save($scope.rilevamento, function (rilevamento) {
                $scope.success = $scope.preview = true;
                $scope.saving = false;
                var casetta = $scope.rilevamento.casetta_descrizione;
                $scope.rilevamento = new INTERVENTO_CASETTA(rilevamento); // creo la nuova istanza
                $scope.rilevamento.casetta_descrizione = casetta; // riappicco altrimenti avrei l'id
                ngNotify.set(msg, {
                    type: 'success',
                    position: 'top'
                });
                
                // se supero alcuni valori parte una mail ai responsabili
                /*if($scope.rilevamento.lampada_uv <= 20 || $scope.rilevamento.litri_sostituzione_filtro >= 40000) {
                    EMAIL.send({tipo:'casette-superamento-valori-critici'},$scope.rilevamento)
                }*/

            },
            function (err) {
                $state.go('dashboard_operatore_casette',{},{reload: true})
                UTILITIES.errorMessage(pagina,utente,err.data,true);
            })
        }
        
        // cancella quello appena inserito
        $scope.eliminaRilevamento = function () {
            var title = $scope.rilevamento.tipo === 'rilevamento' ? CONFIG.messages.rilevamento_delete_title : CONFIG.messages.manutenzione_delete_title,
                message = $scope.rilevamento.tipo === 'rilevamento' ? CONFIG.messages.rilevamento_delete_message : CONFIG.messages.manutenzione_delete_message,
                confirm = $scope.rilevamento.tipo === 'rilevamento' ? CONFIG.messages.rilevamento_deleted : CONFIG.messages.manutenzione_deleted;
            
            bootbox.confirm({
                title: title,
                message: message,
                buttons: UTILITIES.confirmButtons,
                callback: function (ok) {
                    if (ok) {
                        // cancello lato server
                        $scope.rilevamento.$delete(function (err) {
                            if (err)
                                UTILITIES.errorMessage(pagina,utente,err.data, true);

                            ngNotify.set(confirm, {type:'success',position: 'top'});
                            $scope.annullaRilevamento();
                            
                        });
                    }
                }
            });            
        }

        // bottone non rilevato (solo per rilevamento)

        /*
        *   Al click setta il campo corrispondente a true o false (toggle)
        *  se in stato on, setta il campo al valore dell'ultimo rilevamento 
        */
        var nonRilevato = 0;
        $scope.nonRilevato = function (campo) {
            
            $scope.rilevamento[campo+'_non_rilevato'] = !$scope.rilevamento[campo+'_non_rilevato']; // toggle

            if($scope.rilevamento[campo+'_non_rilevato'])
                nonRilevato = nonRilevato + 1;
            else
                nonRilevato = nonRilevato - 1;


            var ultimo = $scope.ultimo_rilevamento[0] ? $scope.ultimo_rilevamento[0][campo] : 0; // potrebbe esserci una casetta senza alcun intervento
            $scope.rilevamento[campo] =  $scope.rilevamento[campo+'_non_rilevato'] ? ultimo : undefined; // per permettere la validazione della form

            // controllo che ci sia almeno una rilevazione
            if(nonRilevato === 5)
                $scope.nessunRilevamento = true;
            else
                $scope.nessunRilevamento = false;
            
        }
        
        // note di una manutenzione
        $scope.note = function (campo, descrizioneCampo) {
            modalNote.show();
            $scope.nota = $scope.rilevamento[campo+'_note'] !=='' ? $scope.rilevamento[campo+'_note'] : '';
            $scope.campo = campo;
            $scope.descrizioneCampo = descrizioneCampo;
        }
        
        // salva una nota (le note seguono il campo del rilevamento con aggiunta di _note)
        $scope.salvaNota = function (campo,nota) {
           
            if(nota!=='')
                 $scope.rilevamento[campo+'_note'] = nota;
            else
                delete $scope.rilevamento[campo+'_note'];
            
            modalNote.hide();
        }
        
        // classe sul bottone della note: serve anche per capire se c'è una nota
        $scope.esisteNota = function (campo){
            if($scope.rilevamento && $scope.rilevamento[campo+'_note'])
                return 'btn-warning';
        }
        
        $scope.eliminaNota = function (campo) {
            delete $scope.rilevamento[campo+'_note'];
            modalNote.hide();
        }

}]);
