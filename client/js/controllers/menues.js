app.controller('menues', [
	'$scope','$rootScope','$location','$window','$http','$sce','$compile', 'Upload', '$state', '$modal', 'ngNotify', 'CONFIG', 'UTILITIES', 'MENU','PAGINA', 'menu', 'menues',
    function ($scope,$rootScope,$location,$window,$http,$sce,$compile, Upload, $state, $modal, ngNotify, CONFIG, UTILITIES, MENU,PAGINA, menu, menues)
    {

        var page = $state.current.url,
            utente = $rootScope.user.user_metadata.name,
            modalNuovoMenu= $modal({
                scope: $scope,
                templateUrl: 'html/modals/nuovo-menu.html',
                show: false,
                backdrop: 'static'
            });

        $scope.menu = menu; // dal resolve del router
        $scope.menues = menues; // dal resolve del router

        // richiamo nello scope il service degli stati in modo da poterlo usare nelle view
        $scope.state = $state;
        // richiamo nello scope tutte le funzioni delle utilities in modo da poterle usare nelle view
        $scope.UTILITIES = UTILITIES;
        $scope.preview = $location.absUrl().indexOf('localhost') !== -1 ? CONFIG.demowebsitetest : CONFIG.demowebsite;


        $scope.nuovoMenu  = function () {
        	modalNuovoMenu.show();
        	$scope.salvaMenu = function (menu) {
        		if(_.find($scope.menues,function(m){ return m.titolo===menu; })){
        			ngNotify.set(CONFIG.messages.menu_exists, 'warn');
        			return;
        		}
        		else {
		        	$scope.nuovoMenu = {titolo: menu, voci:[] }
	                MENU.save($scope.nuovoMenu, function (menu) {
	                        ngNotify.set(CONFIG.messages.menu_saved, 'success');
	                        modalNuovoMenu.hide();
                            $state.go('menu', {
                                id: menu._id
                            });
	                    },
	                    function (err) {
	                        UTILITIES.errorMessage(page,utente,err.data);
	                });			        	
			        
        		}

        	}
        }

        $scope.aggiornaMenu = function () {
  			// controllo che non esista un menu con lo stesso nome 
  			  MENU.query({titolo: $scope.menu.titolo}, function (result){
    				if(result.length > 0 && $scope.menu._id!==result[0]._id){
    					ngNotify.set(CONFIG.messages.menu_exists, 'warn');
    				}
    				else {
                $scope.menu.$update(function () {
                    ngNotify.set(CONFIG.messages.menu_updated, 'success');
                    $scope.bozza = false;
                },function (err) {
                    UTILITIES.errorMessage(page,utente,err.data);
                });					
    				}
    			}, function(err){
    				  console.log(err)
    			})        	
        }

        // cancella il menu
        $scope.eliminaMenu = function () {
            // chiedi prima conferma
            bootbox.confirm({
                title: CONFIG.messages.menu_delete_confirm_title,
                message: CONFIG.messages.menu_delete_confirm_message,
                buttons: UTILITIES.confirmButtons,
                callback: function (ok) {
                    if (ok) {
                        $scope.menu.$delete(function (err) {
                            if (err)
                                UTILITIES.errorMessage(page,utente,err.data);

                            ngNotify.set(CONFIG.messages.menu_deleted, 'success');
                            $state.go('menues');
                        });
                    }
                }
            });
        }


        $scope.cercaPagine = function (chiave) {
            return PAGINA.query({testo: chiave, visibile: true }).$promise;
        }

        $scope.treeOptions = {
          dropped: function(event) {
            $scope.bozza = true;
          }    
        }

        $scope.cambiaVisibilita = function () {
          $scope.bozza = true;
        }

        $scope.selezionaPagina = function (pagina) {

          $scope.voceEsiste = false;

          function cercaVoce (voci){

            voci.forEach(function(voce){
              if(voce.id === pagina._id){
                $scope.voceEsiste = true;
              }
              else {
                if(voce.voci && voce.voci.length)
                  cercaVoce(voce.voci)
              }
            })
          } 

          cercaVoce($scope.menu.voci)

          if(!$scope.voceEsiste) {
            $scope.paginaSelezionata = {
                id: pagina._id,
                titolo: pagina.titolo,
                descrizione: pagina.titolo,
                visibile: false,
                url: pagina.slug,
                voci: []
            };
          }
          else {
            ngNotify.set(CONFIG.messages.menu_item_exists, 'warn');
          }

        }

        $scope.aggiungiVoce = function () {

          if($scope.paginaSelezionata.titolo && $scope.paginaSelezionata.descrizione){
            $scope.menu.voci.push($scope.paginaSelezionata)
            $scope.paginaSelezionata = false;
            ngNotify.set(CONFIG.messages.menu_item_added, 'success');
            $scope.bozza = true;
          }
          else {
            ngNotify.set(CONFIG.messages.menu_item_invalid, 'warn');
          }

        }

        $scope.annullaAggiungi = function () {
          $scope.paginaSelezionata = false;
        }

        $scope.eliminaVoce = function (scope) {
          
          bootbox.confirm({
              title: CONFIG.messages.menu_item_delete_title,
              message: scope.$modelValue.titolo+' sarà eliminata: '+CONFIG.messages.menu_item_delete_message,
              buttons: UTILITIES.confirmButtons,
              callback: function (ok) {
                  if (ok) {
                    scope.remove();
                    ngNotify.set(CONFIG.messages.menu_item_deleted, 'success');
                    $scope.bozza = true;
                  }
              }
          });
        };

         $scope.modificaVoce = function (scope) {
            $scope.paginaSelezionata = _.clone(scope.$modelValue);
            $scope.paginaSelezionata.edit = true;
         }

         $scope.anteprimaVoce = function (scope) {

            var dest = $scope.preview + scope.$modelValue.url;
            $window.open(dest)
         }

         $scope.aggiornaVoce = function () {

            if($scope.paginaSelezionata.titolo && $scope.paginaSelezionata.descrizione){

              delete $scope.paginaSelezionata.edit;

              function modificaVoce (voci){
                voci.forEach(function(voce){
                  if(voce.id === $scope.paginaSelezionata.id){
                    voce.titolo = $scope.paginaSelezionata.titolo;
                    voce.descrizione = $scope.paginaSelezionata.descrizione;
                    ngNotify.set(CONFIG.messages.menu_item_updated, 'success');
                    $scope.paginaSelezionata = false;
                    $scope.bozza = true;
                  }
                  else {
                    if(voce.voci.length)
                      modificaVoce(voce.voci)
                  }
                })
              } 

              modificaVoce($scope.menu.voci)
            }
            else {
              ngNotify.set(CONFIG.messages.menu_item_invalid, 'warn');
            }
         }



}]);
