app.controller('newsletters', [
  '$scope','$rootScope','$location','$window','Upload','$http','$sce','$compile','$state','$modal','ngNotify','CONFIG','UTILITIES','NEWSLETTER','newsletters','newsletter','ARTICOLO','MAILCHIMP','COMUNICATO','EVENTO',
    function ($scope,$rootScope,$location,$window,Upload,$http,$sce,$compile,$state,$modal,ngNotify,CONFIG,UTILITIES,NEWSLETTER,newsletters,newsletter,ARTICOLO,MAILCHIMP,COMUNICATO,EVENTO)
    {
        var modalNewsletter_sendTest = $modal(
        {
            scope: $scope,
            templateUrl: 'html/modals/newsletter.html',
            show: false
        });
        var modalNewsletter_addNews = $modal(
        {
            scope: $scope,
            templateUrl: 'html/modals/newsletter_addNews.html',
            show: false
        });
        
        $scope.tabs = [
            {
                title: "Articoli",
                template: '/html/includes/newsletter_articoli.html',
                activeTab: true
            },
            {
                title: "Comunicati",
                template: '/html/includes/newsletter_comunicati.html'
            },
            {
                title: "Eventi",
                template: '/html/includes/newsletter_eventi.html'
            },
        ];

        var pagina = $state.current.url, 
            utente = $rootScope.user.user_metadata.name;
           
        $scope.newsletter = newsletter; // dal resolve del router
        $scope.newsletters = newsletters; // dal resolve del router
        $scope.state = $state;
        // richiamo nello scope tutte le funzioni delle utilities in modo da poterle usare nelle view
        $scope.UTILITIES = UTILITIES; 
        $scope.editorOptions = UTILITIES.ckEditor();
        $scope.anni = UTILITIES.anniFiltro();
        $scope.meseAttuale = UTILITIES.meseAttuale();
        
        //$scope.elencoTarget = MAILCHIMP.list(); 
        $scope.elencoTarget = UTILITIES.elencoTarget();
 
        _gestioneSalvataggio = function (msg) 
        {
          if (!$state.params.id) 
          {
            if (!msg)
                msg = CONFIG.messages.newsletter_saved;
              NEWSLETTER.save($scope.newsletter, 
                function (newsletter) 
                {
                  ngNotify.set(msg,'success'); 
                  $state.go('newsletters');
                },
                function (err) {
                    UTILITIES.errorMessage(pagina,utente,err.data);
                }
              )
          } 
          else 
          {
            if (!msg)
                msg = CONFIG.messages.newsletter_saved;
              $scope.newsletter.$update(
                function () 
                {
                  ngNotify.set(msg, 'success');
                  $state.go('newsletter');
                },
                function (err) 
                {
                  UTILITIES.errorMessage(pagina,utente,err.data);
                }
              )
          }
        }

        $scope.salvaNewsletter = function () 
        { 
          _gestioneSalvataggio();
        }  

        $scope.eliminaNewsletter = function () 
        {   
          bootbox.confirm({
            title: CONFIG.messages.newsletter_delete_confirm_title,
            message: CONFIG.messages.newsletter_delete_confirm_message,
            buttons: UTILITIES.confirmButtons,
            callback: 
              function (ok) {
                if (ok) {
                  $scope.newsletter.$delete(
                    function (err) 
                    {
                      if (err)
                          UTILITIES.errorMessage(pagina,utente,err.data);
                      ngNotify.set(CONFIG.messages.newsletter_deleted, 'success');
                      $state.go('newsletters');
                    }
                  )
                }
              }
            }
          )
        }

        $scope.cercaNewsletter = function () 
        { 
          $scope.newsletters = NEWSLETTER.query($scope.cercaNews,
            function () 
            {
              $scope.searchresults = true;
            }
          )
        }

        $scope.testNewsletter = function ()
        {
          modalNewsletter_sendTest.show();
        }
        
        $scope.inviaTest = function ()
        {
          $scope.sendMail = true;
          NEWSLETTER.testmail(
            {
              _id : $scope.newsletter._id,
              mailTest:$scope.newsletter.mailTest
            },
            function (newsletter) 
            {
              modalNewsletter_sendTest.hide();
              $scope.newsletter.dataTest = new Date();
               _gestioneSalvataggio(CONFIG.messages.newsletter_send_test);
               $scope.sendMail = false;
            },
            function (err) 
            {
                UTILITIES.errorMessage(pagina,utente,err.data);
            })
           
          }
        
        $scope.inviaNewsletter = function ()
        {
          bootbox.confirm({
            title: CONFIG.messages.newsletter_send_confirm_title,
            message: CONFIG.messages.newsletter_send_confirm_message + ' ' + $scope.newsletter.titolo + ' ' + $scope.newsletter.periodo,
            buttons: UTILITIES.confirmButtons,
            callback: 
              function (ok) 
              {
                if (ok) 
                {
                  $scope.sendMail = true;
                  MAILCHIMP.newcampaign(
                    {
                      _id : $scope.newsletter._id
                    },
                    function (newsletter) 
                    {
                      $scope.newsletter.dataInvio = new Date();
                      $scope.newsletter.stato='Inviata'
                     _gestioneSalvataggio();
                     $scope.sendMail = false;
                    },
                    function (err) 
                    {
                        UTILITIES.errorMessage(pagina,utente,err.data);
                    }
                  ) 
                }
              }
            }
          )
        }


        _isEmail = function (email) 
        { 
            return /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i.
            test(email);
        }

        $scope.validateListEmail = function ()
        {
          var res=true;

          if (!newsletter.mailTest)
            res =  false;
          else
          {
            newsletter.mailTest.split(",").forEach(
                function(item, i) 
                {
                    item = item.replace(/\s/g, "");
                    if (item!=''){
                      if  (_isEmail(item)==false)
                      {
                        res=false;
                        return
                      }
                    }
                }
            )
            return res;
          }
        } 

        $scope.gestioneLabelTitolo = function () {
          if ($scope.newsletter.target.name.toLowerCase().indexOf('interno')>=0   || 
              $scope.newsletter.target.name.toLowerCase().indexOf('interna')>=0  
              )
            $scope.newsletter.titolo = 'Noi BrianzAcque';
          else
            $scope.newsletter.titolo = "Parole sull\'acqua";
        }
       
        $scope.setTargetId = function (target) 
        {
          $scope.newsletter = 
          {
            target    : target,
            target_id : target.id
          };
           
          $scope.gestioneLabelTitolo();

          $rootScope.newsletter = $scope.newsletter;

          $scope.state.go('newsletter');
        }  
      
        $scope.apriModaleNews = function ()
        {
          var sezione='';
          if ($scope.newsletter.target.name.toLowerCase().indexOf('interno')>=0   || 
              $scope.newsletter.target.name.toLowerCase().indexOf('interna')>=0  
              ){
            sezione='Interno'
          }
          else
            sezione='Esterno'  

          ARTICOLO.query(
            {
              listaNewsletter : true,
              sezione:sezione,
              limit:20
            },
            function (articoli) 
            {
              $scope.articoli = articoli;
              COMUNICATO.query(
                {
                  visibile : true,
                limit:20
                },
                function (comunicati) 
                {
                  $scope.comunicati = comunicati;
                  EVENTO.query(
                    {
                      visibile : true,
                      limit:20
                    },
                    function (eventi) 
                    {
                      $scope.eventi = eventi;
                      modalNewsletter_addNews.show();
                    },
                    function (err) 
                    {
                        UTILITIES.errorMessage(pagina,utente,err.data);
                    }
                  )         
                },
                function (err) 
                {
                  UTILITIES.errorMessage(pagina,utente,err.data);
                }
              )        
            },
            function (err) 
            {
                UTILITIES.errorMessage(pagina,utente,err.data);
            }
          )  
        }

        $scope.addNotizia = function (value,tipoNotizia) 
        {

          if (!$scope.newsletter.notizie)
            $scope.newsletter.notizie = [];
          else{
            //effettuo un controllo per evitare di inserire due volte la stessa news
            esisteNews = _.filter($scope.newsletter.notizie, function (n) {
              return n._id === value._id
            });
            if (esisteNews.length>0){
              ngNotify.set(CONFIG.messages.newsletter_news_exist, 'success');
              return;
            }
          }
       
          value.tipo = tipoNotizia;
          $scope.newsletter.notizie.push(value);

          ngNotify.set(CONFIG.messages.newsletter_articolo_add, 'success');
        }

        $scope.eliminaNotizia = function (index) 
        {
           $scope.newsletter.notizie.splice(index,1);
        }

        if (!$scope.newsletter)
           $scope.state.go('newsletters');

        

}]);
