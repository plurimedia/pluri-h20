app.controller('casette', [
	'$scope','$rootScope','$http', 'Upload', '$state', '$modal','$sce', 'ngNotify', 'CONFIG', 'UTILITIES', 'CASETTA','INTERVENTO_CASETTA','XLS', 'casetta', 'casette', 'interventi','operatori','video','CASETTARISPARMIO','LETTURA_CASETTA','letture',
    function ($scope,$rootScope,$http, Upload, $state, $modal,$sce, ngNotify, CONFIG, UTILITIES, CASETTA,INTERVENTO_CASETTA,XLS,casetta, casette, interventi,operatori,video,CASETTARISPARMIO,LETTURA_CASETTA,letture)
    {
        var modalCategoriaUpload = $modal({
                scope: $scope,
                templateUrl: 'html/modals/categoria_upload.html',
                show: false
            }),
            modalUpload = $modal({
                scope: $scope,
                templateUrl: 'html/modals/upload.html',
                show: false,
                backdrop: 'static'
            }),
            modalUploadImmagini = $modal({
                scope: $scope,
                templateUrl: 'html/modals/upload_immagini.html',
                show: false,
                backdrop: 'static'
            }),
            modalDettaglioIntervento = $modal({
                scope: $scope,
                templateUrl: 'html/modals/dettaglio_intervento.html',
                show: false
            }),
            modalPlayVideo = $modal({
                scope: $scope,
                templateUrl: 'html/modals/playvideo.html',
                show: false
            }),
            modalDettaglioLettura = $modal({
                scope: $scope,
                templateUrl: 'html/modals/dettaglio_lettura_casetta.html',
                show: false
            }),
            // per i messaggi di errore
            pagina = $state.current.url,
            utente = $rootScope.user.user_metadata.name,
            _totale = function (i) {
               
                var tot = 0;
                if(i.ambiente > 0)
                    tot += i.ambiente;
                if(i.fredda > 0)
                    tot += i.fredda;
                if(i.gasata > 0)
                    tot += i.gasata;
                    
                return tot;
            },
            _sistemaCollectionPerXls = function(interventi){
                return _.map(interventi, function(i){
                    return {
                        'comune': UTILITIES.decodificaComune(i.comune),
                        'indirizzo': i.indirizzo,
                        'Data ultimo rilevemento': i.data_rilevamento ? moment(i.data_rilevamento).format('DD/MM/YYYY') : '-',
                        'Data ultima manutenzione': i.data_manutenzione ? moment(i.data_manutenzione).format('DD/MM/YYYY') : '-',
                        'Ambiente': i.ambiente ? i.ambiente : '-',
                        'Fredda': i.fredda ? i.fredda : '-',
                        'Gasata': i.gasata ? i.gasata : '-',
                        'Totale': _totale(i),
                        'Data sostituzione filtro': i.ultima_sostituzione_filtro ? moment(i.ultima_sostituzione_filtro.data).format('DD/MM/YYYY') : '-',
                        'Litri sostituzione filtro': i.ultima_sostituzione_filtro ? i.ultima_sostituzione_filtro.sostituzione_filtro_litri : '-',
                        'Data sostituzione lampade': i.ultima_sostituzione_lampade ? moment(i.ultima_sostituzione_lampade.data).format('DD/MM/YYYY') : '-',
                        'GG rimanenti sost lampade': i.ultima_sostituzione_lampade ? i.ultima_sostituzione_lampade.sostituzione_lampade_giorni : '-',
                        'Data ultima pulizia': i.ultima_pulizia ? moment(i.ultima_pulizia.data).format('DD/MM/YYYY') : '-'
                    }
                })
            };

            _sistemaCollectionConsumiPerXls = function(consumi){
                return _.map(consumi, 
                    function(obj){
                       litri =  _totale(obj)
                       metriCUbi =  litri/1000
                    return {
                        'Comune': UTILITIES.decodificaComune(obj.comune),
                        'Indirizzo':obj.indirizzo ? obj.indirizzo : '-',
                        'Mese': moment(obj.data).format('MM'),
                        'Data rilevemento': moment(obj.data).format('DD/MM/YYYY'),
                        'Totale litri': litri,
                        'Totale metri cubi': metriCUbi
                    }
                })
            };

        $scope.casetta = casetta; // dal resolve del router
        $scope.casette = casette; // dal resolve del router
        $scope.interventi = interventi; // dal resolve del router
        $scope.operatori = operatori;// dal resolve del router
        $scope.video = video;// dal resolve del router 
        $scope.annoReport; 

        $scope.editorOptions = UTILITIES.ckEditor();
        $scope.editorOptionsRidotto= UTILITIES.ckEditor('small');

        // richiamo nello scope il service degli stati in modo da poterlo usare nelle view
        $scope.state = $state;
        // richiamo nello scope tutte le funzioni delle utilities in modo da poterle usare nelle view
        $scope.UTILITIES = UTILITIES;
        // stati delle casetta
        $scope.stati = UTILITIES.statiCasette();
        $scope.comuni = UTILITIES.comuni;
        $scope.elencoUsoFile =UTILITIES.elencoUsoFile();
        $scope.anni = UTILITIES.anniFiltro();

        $scope.letture = letture;

        /*
            @@@@@@@@@@@@@@@@@@  Elenco casette   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        */


        // exporta in excel: per ogni casetta l'ultimo intervento
        $scope.esportaXls = function () {
            // per ogni casetta trovo l'ultimo rilevamento e l'ultima manutenzione
            var interventiXls = [];
            $scope.exporting = true;

            async.each($scope.casette, function(casetta, cb){

                var riga = {comune: casetta.comune,indirizzo: casetta.indirizzo};

                async.waterfall([
                    function(callback) {

                        INTERVENTO_CASETTA.query({limit:1,casetta: casetta._id, tipo:'manutenzione'}, function(interventi){
                            // mi segno la data
                            if(interventi[0]){
                                interventi[0].data_manutenzione = interventi[0].data;
                                riga = _.extend(riga, interventi[0]);
                            }
                            // cerco anche l'ultima sost filtro, l'ultima sost lampade e lìultima pulizia
                            INTERVENTO_CASETTA.query({limit:1,casetta: casetta._id, tipo:'manutenzione',sostituzione_lampade:1}, function(interventi){

                                if(interventi[0]){
                                    riga.ultima_sostituzione_lampade = interventi[0];
                                }

                                INTERVENTO_CASETTA.query({limit:1,casetta: casetta._id, tipo:'manutenzione',sostituzione_filtro:1}, function(interventi){
                                    if(interventi[0]){
                                        riga.ultima_sostituzione_filtro = interventi[0];
                                    }

                                    INTERVENTO_CASETTA.query({limit:1,casetta: casetta._id, tipo:'manutenzione',pulizia:1}, function(interventi){
                                        if(interventi[0]){
                                            riga.ultima_pulizia = interventi[0];
                                        }
                                        callback(null, riga);
                                    })

                                })
                            })        
                        })
                    },
                    function(riga, callback) {

                        INTERVENTO_CASETTA.query({limit:1,casetta: casetta._id, tipo:'rilevamento'}, function(interventi){
                            // mi segno la data
                            if(interventi[0]){
                                interventi[0].data_rilevamento = interventi[0].data;
                                riga = _.extend(riga, interventi[0]);
                            }
                            callback(null, riga);
                        })
                    }
                ],
                // aggiungo l'unione dei due interventi
                function(err, riga) {
                   if(err)
                    console.log(err);

                    interventiXls.push(riga);
                    cb();
                });


            }, function(err){

                // genero l'xls
                
                XLS.export(_sistemaCollectionPerXls(interventiXls), function(){
                    $scope.exporting = false;
                });

            })
        }
        

        // exporta in excel: per ogni casetta tutti gli interventi di rilevamento
        $scope.esportaConsumiXls = function (annoSearc) {
            // per ogni casetta trovo l'ultimo rilevamento e l'ultima manutenzione
            var consumiXls = [];
            $scope.exportingConsumi = true;

            var dal = String(annoSearc)+'/01/01';
            var al =  String(annoSearc)+'/12/31'; 

            async.each($scope.casette, 
                
                function(casetta, cb){ 
              
                    var query = {
                        casetta:casetta._id,
                        dal:dal, 
                        al:al,
                        tipo:'rilevamento'
                    };

                    INTERVENTO_CASETTA.query(query, 
                        
                        function(interventi){ 

                       interventi.forEach(
                           function(obj){
                                obj.comune = casetta.comune,
                                obj.indirizzo = casetta.indirizzo,
                            consumiXls.push(obj);
                       })
                    cb();
                    })
                }, 
                function(err){

                // genero l'xls
                var temp = _sistemaCollectionConsumiPerXls(consumiXls);
                XLS.export(temp, function(){
                    $scope.exportingConsumi = false;
                });

            })
        }

        $scope.ordinaByTenant = function(){
            $scope.casette = _.sortBy( $scope.casette, 'tenant' );
        }

        /*
            @@@@@@@@@@@@@@@@@@  Singola casetta   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        */
        
        $scope.descrizioneCasetta = function (casetta) {
            if(casetta.comune) // almeno questo
                return UTILITIES.descrizioneCasetta(casetta);
        }


         // salvataggio casetta
         $scope.salvaCasetta = function (immagini, allegati, video, multimedia) {
            $scope.casetta.descrizione = UTILITIES.descrizioneCasetta($scope.casetta); // salvo la descrizione estesa 
           // var flagEntity =  $scope.casetta.tipo === 'erogatore'
            var tipologia = $scope.casetta.tipo
            var salvatoOk;
            var updateOk;
            var torna;
            var tornaElenco;
            var vaiImmagini;
            var vaiVideo;
            var vaiAllegati;
            var vaiMultimedia;

            switch (tipologia) {
                case "erogatore":
                        salvatoOk = CONFIG.messages.erogatore_saved;
                        tornaElenco = 'erogatori';
                        vaiImmagini = 'erogatore.immagini';
                        vaiAllegati = 'erogatore.allegati';
                        vaiVideo = 'erogatore.video';
                        vaiMultimedia = 'erogatore.multimedia';
                        updateOk = CONFIG.messages.erogatore_updated;
                        torna = 'erogatore';
                  break;
                case "distributore":
                        salvatoOk = CONFIG.messages.distributore_saved;
                        tornaElenco = 'distributori';
                        vaiImmagini = 'distributore.immagini';
                        vaiAllegati = 'distributore.allegati';
                        vaiVideo = 'distributore.video';
                        vaiMultimedia = 'distributore.multimedia';
                        updateOk = CONFIG.messages.distributore_updated
                        torna = 'distributore';
                  break;
                 default:
                        salvatoOk = CONFIG.messages.casetta_saved;
                        tornaElenco = 'casette';
                        vaiImmagini = 'casetta.immagini';
                        vaiAllegati = 'casetta.allegati';
                        vaiVideo = 'casetta.video';
                        vaiMultimedia = 'casetta.multimedia';
                        updateOk = CONFIG.messages.casetta_updated;
                        torna = 'casetta';
            }

            if (!$state.params.id) {
                CASETTA.save($scope.casetta, function (casetta) {
                        ngNotify.set( salvatoOk , 'success');
                        if (!immagini && !allegati && !video) {
                            //flagEntity ? $state.go('erogatori') : $state.go('casette');
                            $state.go(tornaElenco)
                        }
                        if (immagini || allegati) {
                            $scope.casetta = new CASETTA(casetta); // creo una nuova istanza per avere i suoi metodi
                        }
                        // salva e aggiungi immagini
                        if (immagini) {
                            //flagEntity ? $state.go('erogatore.immagini',{id: casetta._id}) : $state.go('casetta.immagini',{id: casetta._id});
                            $state.go(vaiImmagini,{id: casetta._id})
                        }
                        if (allegati) {
                            //flagEntity ? $state.go('erogatore.allegati',{id: casetta._id}) : $state.go('casetta.allegati',{id: casetta._id});
                            $state.go(vaiAllegati,{id: casetta._id})
                        }
                    },
                    function (err) {
                        UTILITIES.errorMessage(pagina,utente,err.data);
                    });
            } else {
                $scope.casetta.$update(function () {
                    ngNotify.set(updateOk , 'success');
                    if (!immagini && !allegati)
                        $state.go(torna);
                    if (immagini)
                        $state.go(vaiImmagini);
                    if (allegati)
                        $state.go(vaiAllegati)
                    if (video)
                        $state.go(vaiVideo)
                    if (multimedia){
                        //flagEntity ? $state.go('erogatore.multimedia') : $state.go('casetta.multimedia');
                        $state.go(vaiMultimedia)
                    }
                        
                },
                function (err) {
                    UTILITIES.errorMessage(pagina,utente,err.data);
                });
            }
        }


        // cancella la casetta
        $scope.eliminaCasetta = function () {
            //var flagEntity =  $scope.casetta.tipo === 'erogatore'
            var tipologia = $scope.casetta.tipo
            var title;
            var message;
            var deletedOk;
            var tornaElenco;

            switch (tipologia) {
                case "erogatore":
                        title = CONFIG.messages.erogatore_delete_confirm_title;
                        message = CONFIG.messages.erogatore_delete_confirm_message;
                        deletedOk = CONFIG.messages.erogatore_deleted; 
                        tornaElenco = 'erogatori'; 
                  break;
                case "distributore":
                        title = CONFIG.messages.distributore_delete_confirm_title;    
                        message = CONFIG.messages.distributore_delete_confirm_message;
                        deletedOk = CONFIG.messages.distributore_deleted;
                        tornaElenco = 'distributori';
                  break;
                 default:
                        title = CONFIG.messages.casetta_delete_confirm_title;
                        message = CONFIG.messages.casetta_delete_confirm_message;
                        deletedOk = CONFIG.messages.casetta_deleted;
                        tornaElenco = 'casette';
            }

            // chiedi prima conferma
            bootbox.confirm({
                title: title,
                message: message,
                buttons: UTILITIES.confirmButtons,
                callback: function (ok) {
                    if (ok) {
                        $scope.casetta.$delete(function (err) {
                            if (err)
                                UTILITIES.errorMessage(pagina,utente,err.data);

                            ngNotify.set(deletedOk , 'success');
                            $state.go(tornaElenco);
                        });
                    }
                }
            });
        }



        /*
            @@@@@@@@@@@@@@@@@@  Singola casetta => interventi  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        */
       


        // modale dettagli
        $scope.dettaglioIntervento = function (intervento) {
            $scope.intervento = _.clone(intervento);
            modalDettaglioIntervento.show();
        }

        // cerca interventi
        $scope.cercaInterventi = function () {
            $scope.cerca.casetta = $scope.casetta._id; // sempre sulla casetta corrente
            $scope.interventi = INTERVENTO_CASETTA.query($scope.cerca, function () {
                $scope.searchresults = true;
            })
        }


        /*
            @@@@@@@@@@@@@@@@@@  Singola casetta => pagina per video  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        */
       
       $scope.tipiVideo = UTILITIES.tipiVideoCasette();

       $scope.getVimeoId = function (video) {
            return video.id.split('/').pop();
       }

       $scope.playVideo = function (video) {
            $scope.playing = video;
            $scope.playing.src = $sce.trustAsResourceUrl('//player.vimeo.com/video/'+$scope.getVimeoId(video)); // dice ad angular di generare un link "safe"
            modalPlayVideo.show();
       }
        
        /*
            @@@@@@@@@@@@@@@@@@  Singola casetta => immagini  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        */



        // trascinamento dei file: scatta l'upload su cloudinary
        $scope.uploadImmagini = function (files) {

            $scope.immagini = files;
            var caricate = [];

            if ($scope.immagini.length) {
                modalUploadImmagini.show();
                async.each($scope.immagini, function (file, cb) {
                    file.upload = Upload.upload({
                        url: UTILITIES.cloudinary_upload_api,
                        fields: {
                            upload_preset: UTILITIES.cloudinary_upload_preset
                        },
                        file: file,
                        skipAuthorization: true
                    })
                    file.upload.then(

                        function (response) {
                            if (response.status === 200) {
                                caricate.push({
                                    id: response.data.public_id,
                                    name: file.name.substr(0, file.name.lastIndexOf('.'))
                                }); // pusho
                                cb();
                            } else {
                                cb(response);
                            }

                        },
                        function (resp) {
                            console.log('Error status: ' + resp.status);
                        },
                        function (evt) {
                            var index = $scope.immagini.indexOf(file);
                            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                            $scope.immagini[index].progress = progressPercentage; // aggiorno la percentuale del singolo file
                        }
                    );
                }, function (err) {
                    if (err)
                        UTILITIES.errorMessage(pagina,utente,err.data);

                    modalUploadImmagini.hide();
                    $scope.casetta.immagini = _.sortBy(_.union(caricate, $scope.casetta.immagini), function (img) {
                        return img.name;
                    }); // aggiungo

                    ngNotify.set(CONFIG.messages.upload_completed, 'success');
                })
            }

        }

        // elimina un'immagine
        $scope.eliminaImmagine = function (index) {
            bootbox.confirm({
                title: CONFIG.messages.image_delete_title,
                message: CONFIG.messages.image_delete_message,
                buttons: UTILITIES.confirmButtons,
                callback: function (ok) {
                    if (ok) {

                        if($scope.casetta.immagini[index].id === $scope.casetta.copertina.id) // se non ho cancellato la copertina -> assicurati di cancellare la copertina e rimettere il default
                            delete $scope.casetta.copertina;

                        $scope.casetta.immagini.splice(index, 1);

                        if(!$scope.casetta.immagini.length) // se non ho più immagini -> assicurati di cancellare la copertina e rimettere il default
                            delete $scope.casetta.copertina;

                        ngNotify.set(CONFIG.messages.image_deleted, 'success');
                    }
                }
            });
        }


        // imposta la copertina
        $scope.copertinaImmagine = function (index) {
            $scope.casetta.copertina = $scope.casetta.immagini[index];
        }

        // rimuovi la copertina
        $scope.rimuoviCopertina = function () {
            delete $scope.casetta.copertina;
        }
        
        // se cambio il nome di un'immagine che è anche copertina, aggiorna la copertina di conseguenza
        $scope.cambiaNomeFoto = function (immagine) {
            if(immagine.id===$scope.casetta.copertina.id)
                $scope.casetta.copertina.name = immagine.name;
        }

        /*
            @@@@@@@@@@@@@@@@@@  Singola casetta => Allegati  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        */


        // categorie di upload di default
        $scope.categorieUploadCasette = $scope.casetta.files || UTILITIES.categorieUploadCasette(); // i suoi o quelli di sistema

        // modale nuova categoria di upload
        $scope.apriModaleCategoriaUpload = function () {
            modalCategoriaUpload.show();
            // resetta
            $scope.nomeCategoriaUpload = undefined;
        }

        // aggiunge o modifica una categoria
        $scope.salvaCategoria = function (nome) {
            if (!$scope.categoria) { // nuova
                var date = new Date(); // ci metto il timestamp com id univoco
                $scope.categorieUploadCasette.push({
                    _id: date.valueOf(),
                    nome: nome,
                    type: 'categoria'
                });
            } else { // esistente
                // trovo la posizione di quello da cancellare
                _.find($scope.categorieUploadCasette, function (cat) {
                    return cat._id === $scope.categoria._id
                }).nome = nome;
            }
            modalCategoriaUpload.hide();
        }

        // cancellazione categoria
        $scope.cancellaCategoria = function (categoria) {
            // trovo la posizione di quello da cancellare
            var cat = _.find($scope.categorieUploadCasette, function (cat, index) {
                if (cat._id === categoria._id) {
                    $scope.daCancellare = index;
                    return true;
                };
            })

            if (!cat.files) { // se non ho files, cancella senza alert
                $scope.categorieUploadCasette.splice($scope.daCancellare, 1);
                ngNotify.set(CONFIG.messages.upload_deleted_category, 'success');
            } else {
                bootbox.confirm({
                    title: CONFIG.messages.upload_delete_category_title,
                    message: CONFIG.messages.upload_delete_category_message,
                    buttons: UTILITIES.confirmButtons,
                    callback: function (ok) {
                        if (ok) {
                            $scope.categorieUploadCasette.splice($scope.daCancellare, 1);
                            ngNotify.set(CONFIG.messages.upload_deleted_category, 'success');
                        }
                    }
                });
            }
        }

        // modifica categoria
        $scope.modificaCategoria = function (categoria) {
            $scope.categoria = categoria;
            $scope.nomeCategoriaUpload = categoria.nome;
            modalCategoriaUpload.show();
        }

        // quando chiudi la modale decontestualizza la categoria
        $scope.$on('modal.hide', function () {
            $scope.categoria = undefined;
        });

        // cancella un file caricato
        $scope.cancellaFile = function (idcategoria, index) {
            bootbox.confirm({
                title: CONFIG.messages.upload_delete_file_title,
                message: CONFIG.messages.upload_delete_file_message,
                buttons: UTILITIES.confirmButtons,
                callback: function (ok) {
                    if (ok) {
                        _.find($scope.categorieUploadCasette, function (c) {
                            return c._id === idcategoria;
                        }).files.splice(index, 1);

                        ngNotify.set(CONFIG.messages.upload_deleted_file, 'success');
                    }
                }
            });
        }

        // trascinamento dei file: scatta l'upload su S3
        $scope.uploadFiles = function (files, categoria) {

            $scope.files = files;

            var caricati = [];

            if ($scope.files.length) {
                modalUpload.show();
                async.each($scope.files, function (file, cb) {

                    var filetype = file.type !== "" ? file.type : 'application/octet-stream', // mi serve per il signed url
                        percorso = $scope.casetta._id + '/' + file.name; 

                    $http
                        ({
                            url: '/s3',
                            data: {
                                percorso: percorso,
                                tipo: filetype,
                                dimensione: file.size
                            },
                            method: 'POST',
                            skipAuthorization: true // non passo dal autorizzazione con token, s3 si incazza
                        })
                        .success(function (url) {
                            // ho un signed url per l'upload su amazon
                            file.upload = Upload.http({
                                url: url,
                                data: file,
                                method: 'PUT',
                                skipAuthorization: true, // non passo dal autorizzazione con token di auth0, s3 si incazza
                                headers: {
                                    'Content-Type': filetype
                                }
                            });

                            file.upload.then(
                                function (response) {
                                    if (response.status === 200) {
                                        caricati.push({
                                            name: file.name.substr(0, file.name.lastIndexOf('.')), // togli l'estensione
                                            url: response.config.url.split('?')[0],
                                            type: 'file' // mi serve per impedire il drag & drop di un file in una categoria
                                        }); // salvo nome e l'url su s3 di quello caricato

                                        cb();
                                    } else {
                                        cb(response)
                                    }
                                },
                                function (resp) {
                                    console.log('Error status: ' + resp.status);

                                },
                                function (evt) {
                                    var index = $scope.files.indexOf(file);
                                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                                    $scope.files[index].progress = progressPercentage; // aggiorno la percentuale del singolo file


                                }
                            );
                        });
                }, function (err) {
                    if (err) {
                        UTILITIES.errorMessage(pagina,utente,err.data);
                    } else {
                        // aggiorno l'array dei files per categoria (sempre in aggiunta)
                        var target = _.find($scope.categorieUploadCasette, function (cat) {
                            return cat._id === categoria._id;
                        });
                        
                        if (caricati.length) caricati[0].uso={'id':'interno'} //default value

                        target.files = _.sortBy(_.flatten(_.union(target.files, caricati)), function (b) {
                            return b.name;
                        });

                        modalUpload.hide();
                        ngNotify.set(CONFIG.messages.upload_completed, 'success');
                    }
                })
            }

        }

       
        $scope.salvaAllegati = function () {
            $scope.casetta.files = $scope.categorieUploadCasette; // attacco 
             
                $scope.casetta.$update(function () {
                    ngNotify.set(CONFIG.messages.casetta_updated, 'success');
                },function (err) {
                        UTILITIES.errorMessage(pagina,utente,err.data);
                });
            
        }

          /*
            @@@@@@@@@@@@@@@@@@  Singola casetta => Consumi  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        */ 

       

        // cerca interventi
        $scope.calcolaConsumi = function () {
           

            if (!$scope.annoConsumo)
                $scope.annoConsumo = String((new Date()).getFullYear())
         
            var dal = $scope.annoConsumo+'/01/01';
            var al =  $scope.annoConsumo+'/12/31';

            var param = {
                tipo:'rilevamento',
                casetta:$scope.casetta._id,
                dal:dal,
                al:al,
                sort:{data:-1}
            }

            INTERVENTO_CASETTA.query(param, 
                function(consumi){
                 $scope.consumi = consumi;

                if ($scope.consumi.length){

                    $scope.progressivoPrelievo = [];
                    $scope.deltaPrelievo = [];
                    $scope.dataPrelievo = []; 
                    $scope.typeBar = 'bar'; 
                    $scope.displayScales=true; 
                    $scope.legendProp = { 
                        position: 'top',
                        display: false,
                        labels: {
                            fontColor: 'rgb(255, 99, 132)'
                        }
                    }
                    $scope.bgColor = "#ABCDEF"
                    $scope.bColor = 'blue';
                    consumi = _.sortBy(consumi, function(e){
                        return e.data
                    })
                    consumi.forEach( function(e) {
                        var totale = _totale(e);
                        $scope.progressivoPrelievo.push(""+totale)  
                        $scope.dataPrelievo.push(moment(e.data).format('DD/MM/YY'))
                    }) 

                    var configPregressivo = {
                        type:$scope.typeBar,
                            data: {
                                labels:  $scope.dataPrelievo,
                                datasets: [ {
                                    label: "Consumo annuo progressivo ",
                                    data: $scope.progressivoPrelievo,
                                    fill: true,
                                    backgroundColor: $scope.bgColor
                                }]
                                
                            },
                            options: {
                                responsive: true,
                                legend: $scope.legendProp,
                                hover: {
                                    mode: 'label'
                                },
                                scales: {
                                    xAxes: [{
                                        display: $scope.displayScales,
                                        scaleLabel: {
                                            display: true,
                                        }
                                    }],
                                    yAxes: [{
                                        display: $scope.displayScales,
                                        ticks: {
                                            beginAtZero: true
                                        },
                                        scaleLabel: {
                                            display: true 
                                        }
                                    }]
                                }
                            }
                        }; 

 
                        //cerco l'ultimo rilevamento dell'anno precedente
                        var anno=$scope.annoConsumo-1;
                    
                        var dal =  anno+'/01/01';
                        var al = anno+'/12/31';
                        var paramLastYear = {
                            tipo:'rilevamento',
                            casetta:$scope.casetta._id,
                            dal:dal,
                            al:al,
                            sort:{data:1},
                            limit:1
                        }
 
                        INTERVENTO_CASETTA.query(paramLastYear, 
                        function(consumo){  
                            if (consumo.length)
                                consumo = consumo[0];
                            
                            var totaleLastYear = _totale(consumo); 
                            

                            $scope.progressivoPrelievoReverse =   _.clone($scope.progressivoPrelievo).reverse()

                            console.log()
                            var i =$scope.progressivoPrelievoReverse.length-1;

                            $scope.progressivoPrelievoReverse.forEach(
                                function(element){

                                    if (i>0){ 
                                        var delta = (parseFloat(element) - parseFloat($scope.progressivoPrelievo[_.clone(i-1)]));
                                        $scope.deltaPrelievo.push(delta);                         }
                                    else{
                                        var delta = element-totaleLastYear
                                        $scope.deltaPrelievo.push(delta) 
                                    }
                                    i--
                                }
                            ) 

                            var configDelta = {
                                type:$scope.typeBar,
                                data: {
                                    labels:  $scope.dataPrelievo,
                                    datasets: [ {
                                        data: _.clone($scope.deltaPrelievo).reverse(),
                                        fill: false,
                                        backgroundColor: $scope.bgColor,
                                        borderColor: $scope.bColor, 
                                    }]
                                    
                                },
                                options: {
                                    responsive: true,
                                    legend: $scope.legendProp,
                                    
                                    scales: {
                                        xAxes: [{
                                            display: $scope.displayScales,
                                            scaleLabel: {
                                                display: true,
                                                
                                            }
                                        }],
                                        yAxes: [{
                                            display: $scope.displayScales,
                                            ticks: {
                                                beginAtZero: false
                                            },
                                            scaleLabel: {
                                                display: true 
                                            }
                                        }]
                                    }
                                }
                            }; 
                        
                            if ($scope.myChart) {
                                $scope.myChart.destroy(); 
                    
                        }
                        var ctx = document.getElementById("consumo_progressivo").getContext("2d");
                        $scope.myChart = new Chart(ctx, configPregressivo);
                            
                        
                        if ($scope.myChart2) {
                            $scope.myChart2.destroy(); 
                
                            }
                            
                            var ctx2 = document.getElementById("consumo_delta").getContext("2d");
                            $scope.myChart2  = new Chart(ctx2, configDelta);
                        
                        }
                    )
                    }
                }
            )
        }

       
        if  ($state.current.name == 'casetta.consumi' || $state.current.name == 'erogatore.consumi'){
             $scope.calcolaConsumi();
        }

        function _resetChartData(chart, new_segments) {
            chart.data.datasets.length = 0;
        };

        /**
         * REPORT
         */ 
         
        $scope.calcoloRisparmio=false
        $scope.exportingPdf=false
        $scope.exportingXls=false
        $scope.datiRisparmio={};

        $scope.visualizzaRisparmio = function(){ 
            $scope.calcoloRisparmio=true 

            $scope.search = {
                'id':$scope.report.risparmio.casettaId,
                'anno':$scope.report.risparmio.anno
            } 

            if  (!$scope.report.risparmio.casettaId){
                $scope.search.id="tutte";

                if($scope.casette[0].tipo === 'erogatore')
                    $scope.search.tipo="erogatore";
            }

            CASETTARISPARMIO.risparmio(
                $scope.search,
                function (response) { 
                    $scope.datiRisparmio=response;
                    $scope.calcoloRisparmio=false
                }
            )
        } 
       
        $scope.esportaPdf = function(){
            $scope.exportingPdf=true
            CASETTARISPARMIO.reportPdf(
                $scope.search,
                function (response) { 
                    $scope.exportingPdf=false
                }
            )
        }

        _sistemaCollection = function (risparmi) { // prepara per export ATO
            return _.sortBy(_.map(risparmi, function (c) {
                return { 
                    'Casetta': c.descrizione ,
                    'Risparmio litri di acqua': c.rilevamento && c.rilevamento.risparmio_acqua ? c.rilevamento.risparmio_acqua : 0,
                    'Risparmio Kg di greggio':  c.rilevamento && c.rilevamento.risparmio_greggio ? c.rilevamento.risparmio_greggio : 0,
                    'Risparmio Kg di Co2':  c.rilevamento && c.rilevamento.risparmio_co ? c.rilevamento.risparmio_co : 0,
                    'Risparmio in eruo smaltimento bottiglie':  c.rilevamento && c.rilevamento.risparmio_smaltimento ? c.rilevamento.risparmio_smaltimento : 0,
                    'Risparmio in euro':  c.rilevamento && c.rilevamento.risparmio_famiglie ? c.rilevamento.risparmio_famiglie : 0
                }
            }), function (c) {
                return c.codice;
            })
        }


        $scope.esportaRisparmioXls = function(){
            $scope.exportingXls=true
            CASETTARISPARMIO.reportXls(
                $scope.search,
                function (response) { 
                    $scope.listaCasette = response; 

                    console.log($scope.listaCasette.length)
                    if ($scope.listaCasette.length>1){

                        var tot_risparmio_acqua=0;
                        var tot_risparmio_greggio=0;
                        var tot_risparmio_co=0;
                        var tot_risparmio_bottiglie=0;
                        var tot_risparmio_famiglie=0;
                        var tot_risparmio_smaltimento=0;

                        $scope.listaCasette.forEach(function(c){ 
    
                            tot_risparmio_acqua = tot_risparmio_acqua + 
                                (c.rilevamento && c.rilevamento.risparmio_acqua ? c.rilevamento.risparmio_acqua : 0);

                            tot_risparmio_greggio = tot_risparmio_greggio + 
                                (c.rilevamento && c.rilevamento.risparmio_greggio ? c.rilevamento.risparmio_greggio : 0);
    
                            tot_risparmio_co = tot_risparmio_co + 
                            (c.rilevamento && c.rilevamento.risparmio_co ? c.rilevamento.risparmio_co : 0); 

                            tot_risparmio_bottiglie = tot_risparmio_bottiglie + 
                            (c.rilevamento && c.rilevamento.risparmio_bottiglie ? c.rilevamento.risparmio_bottiglie : 0); 

                            tot_risparmio_famiglie = tot_risparmio_famiglie + 
                            (c.rilevamento && c.rilevamento.risparmio_famiglie ? c.rilevamento.risparmio_famiglie : 0); 

                            tot_risparmio_smaltimento = tot_risparmio_smaltimento + 
                            (c.rilevamento && c.rilevamento.risparmio_smaltimento ? c.rilevamento.risparmio_smaltimento : 0); 
                        })

                        var totale = {
                            descrizione : "Totali", 
                            rilevamento:{
                                risparmio_acqua :tot_risparmio_acqua,
                                risparmio_greggio: tot_risparmio_greggio,
                                risparmio_co:tot_risparmio_co,
                                risparmio_bottiglie:tot_risparmio_bottiglie,
                                risparmio_famiglie:tot_risparmio_famiglie,
                                risparmio_smaltimento:tot_risparmio_smaltimento
                            }
                        }
                        response.push(totale) 
                    }

                    XLS.export(_sistemaCollection($scope.listaCasette)); 
                    
                    $scope.exportingXls=false
                }
            )
        }

        // cerca lettura
        $scope.ricercaLetture = function () { 
            $scope.cerca.casetta = $scope.casetta._id;
 
            LETTURA_CASETTA.query($scope.cerca, 
                function (letture) {
                $scope.searchresults = true; 
                $scope.letture = letture
            })
        }

        $scope.dettaglioLettura = function(){ 
            modalDettaglioLettura.show();
        }
        
}]);