app.controller('esiti', [
	'$scope','$rootScope','$http', 'Upload', '$state', '$modal', 'ngNotify', 'CONFIG', 'UTILITIES', 'BANDO', 'ESITO', 'esito', 'esiti',
    function ($scope,$rootScope,$http, Upload, $state, $modal, ngNotify, CONFIG, UTILITIES, BANDO, ESITO, esito, esiti)
    {

        var modalCategoriaUpload = $modal({
                scope: $scope,
                templateUrl: 'html/modals/categoria_upload.html',
                show: false
            }),
            modalUpload = $modal({
                scope: $scope,
                templateUrl: 'html/modals/upload.html',
                show: false,
                backdrop: 'static'
            }),
            // per i messaggi di errore
            pagina = $state.current.url,
            utente = $rootScope.user.user_metadata.name;


        $scope.esito = esito; // dal resolve del router
        $scope.esiti = esiti; // dal resolve del router

        setTimeout(function () {
            $scope.bando = $scope.esito.bando || undefined; // devo aspettare un po, non arriva subito :(
            $scope.$apply();
        }, 500)

        $scope.editorOptions = UTILITIES.ckEditor();

        // richiamo nello scope il service degli stati in modo da poterlo usare nelle view
        $scope.state = $state;
        // richiamo nello scope tutte le funzioni delle utilities in modo da poterle usare nelle view
        $scope.UTILITIES = UTILITIES;
        // anni disponibili per il filraggio
        $scope.anni = UTILITIES.anniFiltro();
        // stati degli esiti
        $scope.statiesito = UTILITIES.statiEsito();


        /*
            @@@@@@@@@@@@@@@@@@  Elenco degli esiti   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        */


        // cerca esiti

        $scope.cercaEsiti = function () {
            $scope.esiti = ESITO.query($scope.cerca, function () {
                $scope.searchresults = true;
            })
        }

        /*
            @@@@@@@@@@@@@@@@@@  Singolo esito   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        */
        

        // autocomplete bandi
        $scope.cercaBandi = function (chiave) {
            return BANDO.query({testo: chiave}).$promise;   
        }
        
        $scope.selezionaBando = function (bando) {
            $scope.bando = {}; // popolo l'oggettino per il suggerimento sotto l'input
            $scope.esito.bando = $scope.bando._id = bando._id;
            $scope.esito.titolo = $scope.bando.titolo = bando.titolo; // suggerisco il titolo dell'esito
            angular.element('.titoloesito').focus(); // mi metto sul titolo            
        }


        // salvataggio esito
        $scope.salvaEsito = function (continua) {
            if (!$state.params.id) {
                ESITO.save($scope.esito, function (esito) {
                        ngNotify.set(CONFIG.messages.esito_saved, 'success');
                        if (!continua) {
                            $state.go('esiti');
                        } // salva e continua
                        else {
                            $scope.esito = new ESITO(esito); // creo una nuova istanza per avere i suoi metodi
                            $state.go('esito.allegati', {
                                id: esito._id
                            });
                        }

                    },
                    function (err) {
                        UTILITIES.errorMessage(pagina,utente,err.data);
                    });
            } else {
                $scope.esito.$update(function () {
                    ngNotify.set(CONFIG.messages.esito_updated, 'success');
                    if (!continua) // salva e continua
                        $state.go('esito');
                    else
                        $state.go('esito.allegati');

                },
                function (err) {
                    UTILITIES.errorMessage(pagina,utente,err.data);
                });
            }
        }


        // cancella l'esito
        $scope.eliminaEsito = function () {
            // chiedi prima conferma
            bootbox.confirm({
                title: CONFIG.messages.esito_delete_confirm_title,
                message: CONFIG.messages.esito_delete_confirm_message,
                buttons: UTILITIES.confirmButtons,
                callback: function (ok) {
                    if (ok) {
                        $scope.esito.$delete(function (err) {
                            if (err)
                                UTILITIES.errorMessage(pagina,utente,err.data);

                            ngNotify.set(CONFIG.messages.esito_deleted, 'success');
                            $state.go('esiti');
                        });
                    }
                }
            });
        }




        /*
            @@@@@@@@@@@@@@@@@@  Singolo Esito => Documenti  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        */


        // categorie di upload di default
        $scope.categorieUploadEsiti = $scope.esito.files || UTILITIES.categorieUploadEsiti(); // i suoi o quelli di sistema

        // modale nuova categoria di upload
        $scope.apriModaleCategoriaUpload = function () {
            modalCategoriaUpload.show();
            // resetta
            $scope.nomeCategoriaUpload = undefined;
        }

        // aggiunge o modifica una categoria
        $scope.salvaCategoria = function (nome) {
            if (!$scope.categoria) { // nuova
                var date = new Date(); // ci metto il timestamp com id univoco
                $scope.categorieUploadEsiti.push({
                    _id: date.valueOf(),
                    nome: nome,
                    type: 'categoria'
                });
            } else { // esistente
                // trovo la posizione di quello da cancellare
                _.find($scope.categorieUploadEsiti, function (cat) {
                    return cat._id === $scope.categoria._id
                }).nome = nome;
            }
            modalCategoriaUpload.hide();
        }

        // cancellazione categoria
        $scope.cancellaCategoria = function (categoria) {
            // trovo la posizione di quello da cancellare
            var cat = _.find($scope.categorieUploadEsiti, function (cat, index) {
                if (cat._id === categoria._id) {
                    $scope.daCancellare = index;
                    return true;
                };
            })

            if (!cat.files) { // se non ho files, cancella senza alert
                $scope.categorieUploadEsiti.splice($scope.daCancellare, 1);
                ngNotify.set(CONFIG.messages.upload_deleted_category, 'success');
            } else {
                bootbox.confirm({
                    title: CONFIG.messages.upload_delete_category_title,
                    message: CONFIG.messages.upload_delete_category_message,
                    buttons: UTILITIES.confirmButtons,
                    callback: function (ok) {
                        if (ok) {
                            $scope.categorieUploadEsiti.splice($scope.daCancellare, 1);
                            ngNotify.set(CONFIG.messages.upload_deleted_category, 'success');
                        }
                    }
                });
            }
        }

        // modifica categoria
        $scope.modificaCategoria = function (categoria) {
            $scope.categoria = categoria;
            $scope.nomeCategoriaUpload = categoria.nome;
            modalCategoriaUpload.show();
        }

        // quando chiudi la modale decontestualizza la categoria
        $scope.$on('modal.hide', function () {
            $scope.categoria = undefined;
        });

        // cancella un file caricato
        $scope.cancellaFile = function (idcategoria, index) {
            bootbox.confirm({
                title: CONFIG.messages.upload_delete_file_title,
                message: CONFIG.messages.upload_delete_file_message,
                buttons: UTILITIES.confirmButtons,
                callback: function (ok) {
                    if (ok) {
                        _.find($scope.categorieUploadEsiti, function (c) {
                            return c._id === idcategoria;
                        }).files.splice(index, 1);

                        ngNotify.set(CONFIG.messages.upload_deleted_file, 'success');
                    }
                }
            });
        }

        // trascinamento dei file: scatta l'upload su S3
        $scope.uploadFiles = function (files, categoria) {

            $scope.files = files;
            var caricati = [];

            if ($scope.files.length) {
                modalUpload.show();
                async.each($scope.files, function (file, cb) {

                    var filetype = file.type !== "" ? file.type : 'application/octet-stream', // mi serve per il signed url
                        percorso = $scope.esito._id + '/' + file.name;

                    $http
                        ({
                            url: '/s3',
                            data: {
                                percorso: percorso,
                                tipo: filetype,
                                dimensione: file.size
                            },
                            method: 'POST',
                            skipAuthorization: true // non passo dal autorizzazione con token, s3 si incazza
                        })
                        .success(function (url) {
                            // ho un signed url per l'upload su amazon
                            file.upload = Upload.http({
                                url: url,
                                data: file,
                                method: 'PUT',
                                skipAuthorization: true, // non passo dal autorizzazione con token di auth0, s3 si incazza
                                headers: {
                                    'Content-Type': filetype
                                }
                            });

                            file.upload.then(
                                function (response) {
                                    if (response.status === 200) {
                                        caricati.push({
                                            name: file.name.substr(0, file.name.lastIndexOf('.')), // togli l'estensione
                                            url: response.config.url.split('?')[0],
                                            type: 'file' // mi serve per impedire il drag & drop di un file in una categoria
                                        }); // salvo nome e l'url su s3 di quello caricato
                                        cb();
                                    } else {
                                        cb(response)
                                    }
                                },
                                function (resp) {
                                    console.log('Error status: ' + resp.status);

                                },
                                function (evt) {
                                    var index = $scope.files.indexOf(file);
                                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                                    $scope.files[index].progress = progressPercentage; // aggiorno la percentuale del singolo file


                                }
                            );
                        });
                }, function (err) {
                    if (err) {
                        UTILITIES.errorMessage(pagina,utente,err.data);
                    } else {
                        // aggiorno l'array dei files per categoria (sempre in aggiunta)
                        var target = _.find($scope.categorieUploadEsiti, function (cat) {
                            return cat._id === categoria._id;
                        });

                        target.files = _.sortBy(_.flatten(_.union(target.files, caricati)), function (b) {
                            return b.name;
                        });

                        modalUpload.hide();
                        ngNotify.set(CONFIG.messages.upload_completed, 'success');
                    }
                })
            }

        }


        $scope.salvaAllegati = function () {
            $scope.esito.files = $scope.categorieUploadEsiti; // attacco
            $scope.esito.$update(function () {
                ngNotify.set(CONFIG.messages.esito_updated, 'success');
            },function (err) {
                    UTILITIES.errorMessage(pagina,utente,err.data);
            });
        }


}]);
