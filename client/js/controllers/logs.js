app.controller('logs', [
  '$scope',
  '$modal',
  'CONFIG',
  'logs',
  'logcodes',
  'LOG',
    function ($scope,$modal,CONFIG,logs,logcodes,LOG) {

    	$scope.logs = logs;

    	$scope.logcodes = logcodes;
    	
        var modalLog = $modal({
                scope: $scope,
                templateUrl: 'html/modals/log.html',
                show: false,
                backdrop: 'static'
            });

        $scope.apriLog = function (log) {
        	$scope.log = _.omit(_.clone(log),'codes');
        	modalLog.show();
        }

        $scope.cercaLogs = function () {

            $scope.logs = LOG.query($scope.cerca, function () {
                $scope.searchresults = true;
            })
        }
}]);
