app.controller('articoli', [
  '$scope','$rootScope','$location','$window','Upload','$http','$sce','$compile', '$state', '$modal', 'ngNotify', 'CONFIG', 'UTILITIES', 'ARTICOLO', 'articoli', 'articolo',
    function ($scope,$rootScope,$location,$window,Upload,$http,$sce,$compile, $state, $modal, ngNotify, CONFIG, UTILITIES, ARTICOLO, articoli, articolo)
    {

        var modalCategoriaUpload = $modal(
          {
              scope: $scope,
              templateUrl: 'html/modals/categoria_upload.html',
              show: false
          }),
          modalUpload = $modal(
          {
              scope: $scope,
              templateUrl: 'html/modals/upload.html',
              show: false,
              backdrop: 'static'
          }),
          modalUploadImmagini = $modal(
          {
              scope: $scope,
              templateUrl: 'html/modals/upload_immagini.html',
              show: false,
              backdrop: 'static'
          })
        var pagina = $state.current.url, 
            utente = $rootScope.user.user_metadata.name;
        $scope.articolo = articolo; // dal resolve del router
        $scope.articoli = articoli; // dal resolve del router
        $scope.state = $state;
        // richiamo nello scope tutte le funzioni delle utilities in modo da poterle usare nelle view
        $scope.UTILITIES = UTILITIES; 
        $scope.editorOptions = UTILITIES.ckEditor();
        $scope.editorOptionsRidotto= UTILITIES.ckEditor('small');
        $scope.anni = UTILITIES.anniFiltro();
        $scope.elencoTag = UTILITIES.elencoTag();
        $scope.elencoSezioni = UTILITIES.elencoSezioni();

        _gestionefieldDisabled  = function () 
        {
          $scope.fieldDisabled =  
            $rootScope.user.roles.indexOf('autore')>=0
            && 
            ( $scope.articolo.stato=='In revisione' ||
              $scope.articolo.stato=='Revisionato'  || 
              $scope.articolo.stato=='Pubblicato'
            );
        }
        
        _gestioneSalvataggio = function (immagini,allegati) {
          if (!$state.params.id) 
          {
            ARTICOLO.save($scope.articolo, 
              function (articolo) 
              {
                ngNotify.set(CONFIG.messages.articolo_saved, 'success');
                if (!immagini && !allegati)
                    $state.go('articoli');
                if (immagini || allegati) 
                {
                    $scope.articolo = new ARTICOLO(articolo); // creo una nuova istanza per avere i suoi metodi
                    $scope.articolo.stato=articolo.stato;
                }
                // salva e aggiungi immagini
                if (immagini) 
                {
                    $state.go('articolo.immagini', 
                    {
                        id: articolo._id
                    });
                }
                if (allegati) 
                {
                    $state.go('articolo.allegati', 
                    {
                        id: articolo._id
                    });
                }
              },
              function (err) 
              {
                  UTILITIES.errorMessage(pagina,utente,err.data);
              })
          } 
          else 
          {
            $scope.articolo.$update(
              function () 
              {
                  ngNotify.set(CONFIG.messages.articolo_updated, 'success');
                  if (!immagini && !allegati)
                      $state.go('articolo');
                  if (immagini)
                      $state.go('articolo.immagini');
                  if (allegati)
                      $state.go('articolo.allegati');
              },
              function (err) {
                  UTILITIES.errorMessage(pagina,utente,err.data);
              }
            )
          }
        } 
        
        $scope.fieldDisabled = false;
        if($state.params.id){ 
          $scope.articolo.$promise.then(_gestionefieldDisabled); 
        }

        $scope.salvaArticolo = function (immagini,allegati) 
        {
          if ($scope.articolo.stato=='In revisione' && $rootScope.user.roles.indexOf('autore')>=0){
           bootbox.confirm({
            title: CONFIG.messages.articolo_inrevisione_confirm_title,
            message: CONFIG.messages.articolo_inrevisione_confirm_message,
            buttons: UTILITIES.confirmButtons,
            callback: 
              function (ok) {
                if (ok) {
                  _gestioneSalvataggio(immagini,allegati);
                  _gestionefieldDisabled();
                }
              }
            })
          }
          else{
            _gestioneSalvataggio(immagini,allegati);
          }
        } 
                
        $scope.eliminaArticolo = function () 
        {   
          bootbox.confirm({
            title: CONFIG.messages.articolo_delete_confirm_title,
            message: CONFIG.messages.articolo_delete_confirm_message,
            buttons: UTILITIES.confirmButtons,
            callback: 
              function (ok) {
                if (ok) {
                  $scope.articolo.$delete(
                    function (err) 
                    {
                      if (err)
                          UTILITIES.errorMessage(pagina,utente,err.data);
                      ngNotify.set(CONFIG.messages.articolo_deleted, 'success');
                      $state.go('articoli');
                    }
                  )
                }
              }
            }
          )
        }

        $scope.cercaArticolo = function () 
        {
          $scope.articoli = ARTICOLO.query($scope.cerca,
            function () 
            {
              $scope.searchresults = true;
            }
          )
        } 

        $scope.uploadImmagini = function (files) 
        {
            $scope.immagini = files;
            var caricate = [];
            if ($scope.immagini.length) 
            {
              modalUploadImmagini.show();
              async.each($scope.immagini, function (file, cb) 
              {
                file.upload = Upload.upload({
                    url: UTILITIES.cloudinary_upload_api,
                    fields: {
                        upload_preset: UTILITIES.cloudinary_upload_preset
                    },
                    file: file,
                    skipAuthorization: true
                })
                file.upload.then(
                  function (response) 
                  {
                    if (response.status === 200) {
                        caricate.push({
                            id: response.data.public_id,
                            name: file.name.substr(0, file.name.lastIndexOf('.'))
                        }); // pusho
                        cb();
                    } else
                        cb(response);
                  },
                  function (resp) 
                  {
                      console.log('Error status: ' + resp.status);
                  },
                  function (evt) 
                  {
                      var index = $scope.immagini.indexOf(file);
                      var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                      $scope.immagini[index].progress = progressPercentage; // aggiorno la percentuale del singolo file
                  }
                )
              }, 
              function (err) 
              {
                if (err)
                    UTILITIES.errorMessage(pagina,utente,err.data);
                modalUploadImmagini.hide();
                $scope.articolo.immagini = _.sortBy(_.union(caricate, $scope.articolo.immagini), 
                  function (img) 
                  {
                    return img.name;
                  }
                )
                ngNotify.set(CONFIG.messages.upload_completed, 'success');
              }
            )
          }
        }
        
        $scope.eliminaImmagine = function (index) 
        {
          bootbox.confirm({
            title: CONFIG.messages.image_delete_title,
            message: CONFIG.messages.image_delete_message,
            buttons: UTILITIES.confirmButtons,
            callback: 
              function (ok) 
              {
                if (ok) 
                {
                  if($scope.articolo.copertina && $scope.articolo.immagini[index].id === $scope.articolo.copertina.id) // se non ho cancellato la copertina -> assicurati di cancellare la copertina e rimettere il default
                      delete $scope.articolo.copertina;
                  $scope.articolo.immagini.splice(index, 1);
                  if(!$scope.articolo.immagini.length) // se non ho più immagini -> assicurati di cancellare la copertina e rimettere il default
                      delete $scope.articolo.copertina;
                  ngNotify.set(CONFIG.messages.image_deleted, 'success');
                }
              }
            }
          )
        }

        // imposta la copertina
        $scope.copertinaImmagine = function (index) 
        {
            $scope.articolo.copertina = $scope.articolo.immagini[index];
        }

        // rimuovi la copertina
        $scope.rimuoviCopertina = function () 
        {
            delete $scope.articolo.copertina;
        }
        
        // se cambio il nome di un'immagine che è anche copertina, aggiorna la copertina di conseguenza
        $scope.cambiaNomeFoto = function (immagine) 
        {
          if(immagine.id===$scope.articolo.copertina.id)
              $scope.articolo.copertina.name = immagine.name;
        }

        // categorie di upload di default
        $scope.categorieUploadArticoli = $scope.articolo.files || UTILITIES.categorieUploadArticoli(); // i suoi o quelli di sistema

        // modale nuova categoria di upload
        $scope.apriModaleCategoriaUpload = function () {
            modalCategoriaUpload.show();
            // resetta
            $scope.nomeCategoriaUpload = undefined;
        }

        // aggiunge o modifica una categoria
        $scope.salvaCategoria = function (nome) {
            if (!$scope.categoria) { // nuova
                var date = new Date(); // ci metto il timestamp com id univoco
                $scope.categorieUploadArticoli.push({
                    _id: date.valueOf(),
                    nome: nome,
                    type: 'categoria'
                });
            } else { // esistente
                // trovo la posizione di quello da cancellare
                _.find($scope.categorieUploadArticoli, function (cat) {
                    return cat._id === $scope.categoria._id
                }).nome = nome;
            }
            modalCategoriaUpload.hide();
        }

        // cancellazione categoria
        $scope.cancellaCategoria = function (categoria) {
            // trovo la posizione di quello da cancellare
            var cat = _.find($scope.categorieUploadArticoli, function (cat, index) {
                if (cat._id === categoria._id) {
                    $scope.daCancellare = index;
                    return true;
                };
            })

            if (!cat.files) { // se non ho files, cancella senza alert
                $scope.categorieUploadArticoli.splice($scope.daCancellare, 1);
                ngNotify.set(CONFIG.messages.upload_deleted_category, 'success');
            } else {
                bootbox.confirm({
                    title: CONFIG.messages.upload_delete_category_title,
                    message: CONFIG.messages.upload_delete_category_message,
                    buttons: UTILITIES.confirmButtons,
                    callback: function (ok) {
                        if (ok) {
                            $scope.categorieUploadArticoli.splice($scope.daCancellare, 1);
                            ngNotify.set(CONFIG.messages.upload_deleted_category, 'success');
                        }
                    }
                });
            }
        }

        // modifica categoria
        $scope.modificaCategoria = function (categoria) {
            $scope.categoria = categoria;
            $scope.nomeCategoriaUpload = categoria.nome;
            modalCategoriaUpload.show();
        }

        // quando chiudi la modale decontestualizza la categoria
        $scope.$on('modal.hide', function () {
            $scope.categoria = undefined;
        });

        // cancella un file caricato
        $scope.cancellaFile = function (idcategoria, index) {
            bootbox.confirm({
                title: CONFIG.messages.upload_delete_file_title,
                message: CONFIG.messages.upload_delete_file_message,
                buttons: UTILITIES.confirmButtons,
                callback: function (ok) {
                    if (ok) {
                        _.find($scope.categorieUploadArticoli, function (c) {
                            return c._id === idcategoria;
                        }).files.splice(index, 1);

                        ngNotify.set(CONFIG.messages.upload_deleted_file, 'success');
                    }
                }
            });
        }

        // trascinamento dei file: scatta l'upload su S3
        $scope.uploadFiles = function (files, categoria) {

            $scope.files = files;
            var caricati = [];

            if ($scope.files.length) {
                modalUpload.show();
                async.each($scope.files, function (file, cb) {

                    var filetype = file.type !== "" ? file.type : 'application/octet-stream', // mi serve per il signed url
                        percorso = $scope.articolo._id + '/' + file.name;

                    $http
                        ({
                            url: '/s3',
                            data: {
                                percorso: percorso,
                                tipo: filetype,
                                dimensione: file.size
                            },
                            method: 'POST',
                            skipAuthorization: true // non passo dal autorizzazione con token, s3 si incazza
                        })
                        .success(function (url) {
                            // ho un signed url per l'upload su amazon
                            file.upload = Upload.http({
                                url: url,
                                data: file,
                                method: 'PUT',
                                skipAuthorization: true, // non passo dal autorizzazione con token di auth0, s3 si incazza
                                headers: {
                                    'Content-Type': filetype
                                }
                            });

                            file.upload.then(
                                function (response) {
                                    if (response.status === 200) {
                                        caricati.push({
                                            name: file.name.substr(0, file.name.lastIndexOf('.')), // togli l'estensione
                                            url: response.config.url.split('?')[0],
                                            type: 'file' // mi serve per impedire il drag & drop di un file in una categoria
                                        }); // salvo nome e l'url su s3 di quello caricato
                                        cb();
                                    } else {
                                        cb(response)
                                    }
                                },
                                function (resp) {
                                    console.log('Error status: ' + resp.status);

                                },
                                function (evt) {
                                    var index = $scope.files.indexOf(file);
                                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                                    $scope.files[index].progress = progressPercentage; // aggiorno la percentuale del singolo file


                                }
                            );
                        });
                }, function (err) {
                    if (err) {
                        UTILITIES.errorMessage(pagina,utente,err.data);
                    } else {
                        // aggiorno l'array dei files per categoria (sempre in aggiunta)
                        var target = _.find($scope.categorieUploadArticoli, function (cat) {
                            return cat._id === categoria._id;
                        });

                        target.files = _.sortBy(_.flatten(_.union(target.files, caricati)), function (b) {
                            return b.name;
                        });

                        modalUpload.hide();
                        ngNotify.set(CONFIG.messages.upload_completed, 'success');
                    }
                })
            }
        }

        $scope.salvaAllegati = function () {
            $scope.articolo.files = $scope.categorieUploadArticoli; // attacco
            $scope.articolo.$update(function () {
                ngNotify.set(CONFIG.messages.cantiere_updated, 'success');
            },function (err) {
                    UTILITIES.errorMessage(pagina,utente,err.data);
            });
        }

}]);
