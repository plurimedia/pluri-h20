app.controller('glossario', [
	'$scope','$rootScope','$location','$window','$http','$sce','$compile', '$state', '$modal', 'ngNotify', 'CONFIG', 'UTILITIES', 'GLOSSARIO', 'glossario', 'glossari',
    function ($scope,$rootScope,$location,$window,$http,$sce,$compile, $state, $modal, ngNotify, CONFIG, UTILITIES, GLOSSARIO, glossario, glossari)
    {

        var pagina = $state.current.url,
            utente = $rootScope.user.user_metadata.name;

        $scope.glossario = glossario; // dal resolve del router
        $scope.glossari = glossari; // dal resolve del router

        // richiamo nello scope il service degli stati in modo da poterlo usare nelle view
        $scope.state = $state;
        // richiamo nello scope tutte le funzioni delle utilities in modo da poterle usare nelle view
        $scope.UTILITIES = UTILITIES; 
        $scope.editorOptions = UTILITIES.ckEditor(); 
      	 
        $scope.salvaGlossario = function () 
        {
          if (!$state.params.id) 
          {
            GLOSSARIO.save($scope.glossario, 
              function (glossario) 
              {
                ngNotify.set(CONFIG.messages.glossario_saved, 'success');
                $state.go('glossari');
              },
              function (err)
              {
                  UTILITIES.errorMessage(pagina,utente,err.data);
              }
            )
          }
          else
          {
            $scope.glossario.$update(
              function () 
              {
                  ngNotify.set(CONFIG.messages.glossario_updated, 'success');
                  $state.go('glossario'); 
              },
              function (err) 
              {
                  UTILITIES.errorMessage(pagina,utente,err.data);
              }
            )
          }
        }

        $scope.cercaGlossario = function () 
        {
          $scope.glossari = GLOSSARIO.query($scope.cerca,
            function () 
            {
                $scope.searchresults = true;
            }
          )
        } 
     
        $scope.eliminaGlossario = function () 
        {
          bootbox.confirm({
            title: CONFIG.messages.glossario_delete_confirm_title,
            message: CONFIG.messages.glossario_delete_confirm_message,
            buttons: UTILITIES.confirmButtons,
            callback: function (ok) {
              if (ok) {
                $scope.glossario.$delete(
                  function (err) 
                  {
                    if (err)
                        UTILITIES.errorMessage(pagina,utente,err.data);
                    ngNotify.set(CONFIG.messages.glossario_deleted, 'success');
                    $state.go('glossari');
                  }
                )
              }
            }
          })
        }

}]);
