app.controller('login', [
  '$rootScope',
  '$scope',
  function($rootScope, $scope) {

    manageInfoTenantByUrl($rootScope);

    $scope.login = function () {
      
      $rootScope.lock.show({
        language: 'it',
        allowSignUp: false,
        languageDictionary: {
          title: $rootScope.appName
        },
        theme: {
          logo: $rootScope.logoAuth0,
          primaryColor: 'rgb(21, 106, 171)'
        }
      })
    }

  }
]);

