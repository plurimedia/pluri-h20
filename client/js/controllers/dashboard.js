app.controller('dashboard', [
	'$rootScope', '$scope', 'bandi', 'esiti','comunicati','casette','eventi','pagine','menues','faqs','sportelli','articoli','newsletters','glossari','erogatori','distributori',
    function ($rootScope, $scope, bandi, esiti,comunicati,casette, eventi,pagine,menues,faqs,sportelli,articoli,newsletters,glossari,erogatori,distributori) {

        $scope.bandi = bandi;
        $scope.esiti = esiti;
        $scope.comunicati = comunicati;
        $scope.eventi = eventi;
        $scope.casette = casette;
        $scope.erogatori = erogatori;
        $scope.distributori = distributori;
        $scope.pagine = pagine;
        $scope.menues = menues;
        $scope.faqs = faqs;
        $scope.sportelli = sportelli;
        $scope.articoli = articoli;
        $scope.newsletters = newsletters;
        $scope.glossari = glossari;
}]);
