app.controller('pagine', [
	'$scope','$location','$window','$rootScope','$http','$sce','$compile', 'Upload', '$state', '$modal', 'ngNotify', 'CONFIG', 'UTILITIES', 'PAGINA','SLUG', 'pagina', 'pagine',
    function ($scope,$location,$window,$rootScope,$http,$sce,$compile, Upload, $state, $modal, ngNotify, CONFIG, UTILITIES, PAGINA,SLUG, pagina, pagine)
    {

        var page = $state.current.url,
            utente = $rootScope.user.user_metadata.name,
            modalUpload = $modal({
                scope: $scope,
                templateUrl: 'html/modals/upload.html',
                show: false,
                backdrop: 'static'
            }),
            modalIntestazione= $modal({
                scope: $scope,
                templateUrl: 'html/modals/intestazione-tabella.html',
                show: false,
                backdrop: 'static'
            });

        $scope.pagina = pagina; // dal resolve del router
        $scope.pagine = pagine; // dal resolve del router

        // richiamo nello scope il service degli stati in modo da poterlo usare nelle view
        $scope.state = $state;
        // richiamo nello scope tutte le funzioni delle utilities in modo da poterle usare nelle view
        $scope.UTILITIES = UTILITIES;
        $scope.editorOptions = UTILITIES.ckEditorRidotto();
        $scope.preview = $location.absUrl().indexOf('localhost') !== -1 ? CONFIG.demowebsitetest : CONFIG.demowebsite;


        /*
            @@@@@@@@@@@@@@@@@@  Elenco delle pagine   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        */


        // cerca pagine

        $scope.cercaP = function () {
            $scope.pagine = PAGINA.query($scope.cerca, function () {
                $scope.searchresults = true;
            })
        }

        /*
            @@@@@@@@@@@@@@@@@@  Singola pagina   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        */
       
        $scope.checkSlug = function () {
            if($scope.pagina.slug!==''){
                $scope.slug = SLUG.get({candidate: $scope.pagina.slug, collection: 'Pagina', pagina: $scope.pagina._id}, function (result){
                    $scope.pagina.slug = result.string;
                });
            }
        }


        // salvataggio pagina
        $scope.salvaPagina = function (anteprima) {

            var dest = $scope.preview + $scope.pagina.slug;

            $scope.pagina.contenuti.forEach(function(c){
                if(c.hasOwnProperty('showActions'))
                    delete c.showActions;
            })

            if (!$state.params.id) {
                PAGINA.save($scope.pagina, function (pagina) {
                        
                        if(!anteprima){
                            ngNotify.set(CONFIG.messages.pagina_saved, 'success');
                            $location.path('/pagina/'+pagina._id)
                        }
                        else {
                             $window.open(dest)
                        }
                
                    },
                    function (err) {
                        UTILITIES.errorMessage(pagina,utente,err.data);
                    });
            } else {
                $scope.pagina.$update(function () {
                    if(!anteprima){
                        ngNotify.set(CONFIG.messages.pagina_updated, 'success');
                    }
                    if(anteprima){
                        $window.open(dest)
                    }
                },function (err) {
                    UTILITIES.errorMessage(pagina,utente,err.data);
                });
            }
        }


        // cancella la pagina
        $scope.eliminaPagina = function () {
            // chiedi prima conferma
            bootbox.confirm({
                title: CONFIG.messages.pagina_delete_confirm_title,
                message: CONFIG.messages.pagina_delete_confirm_message,
                buttons: UTILITIES.confirmButtons,
                callback: function (ok) {
                    if (ok) {
                        $scope.pagina.$delete(function (err) {
                            if (err)
                                UTILITIES.errorMessage(page,utente,err.data);

                            ngNotify.set(CONFIG.messages.pagina_deleted, 'success');
                            $state.go('pagine');
                        });
                    }
                }
            });
        }

        $scope.sposta = function(element, delta) {
          var array = $scope.pagina.contenuti;
          var index = array.indexOf(element);
          var newIndex = index + delta;
          if (newIndex < 0  || newIndex == array.length){
            return; //Already at the top or bottom.
          }
          var indexes = [index, newIndex].sort(); //Sort the indixes
          array.splice(indexes[0], 2, array[indexes[1]], array[indexes[0]]); //Replace from lowest index, two elements, reverting the order
        };

        $scope.elimina = function (index) {
            console.log(index)
            $scope.pagina.contenuti.splice(index,1);
        }


        $scope.numeroCaratteri = function (testo) {
            return testo ? testo.length : false;
        }

        $scope.idContenuto = function () {
            var d = new Date();
            return d.getTime();
        }

        $scope.aggiungiDivisore = function () {
            $scope.pagina.contenuti.push({
                id: $scope.idContenuto(),
                tipo:'divisore'
            })
        }

        $scope.aggiungiTitoloSezione = function () {
            $scope.pagina.contenuti.push({
                id: $scope.idContenuto(),
                tipo:'titolo_sezione',
                testo:'Titolo di sezione',
                livello: 2
            })            
        }

        $scope.aggiungiTesto = function () {
            $scope.pagina.contenuti.push({
                id: $scope.idContenuto(),
                tipo:'testo',
                testo:'Lorem ipsum dolor sit amet, <strong>consectetur</strong> adipisicing elit. Minus repudiandae quasi tenetur in fugit, natus minima, debitis voluptate nostrum dolorem, facere, sed cupiditate. Illum minima perferendis sunt asperiores eligendi nulla.'
            })            
        }

        $scope.aggiungiImmagine = function () {
            $scope.pagina.contenuti.push({
                id: $scope.idContenuto(),
                tipo:'immagine'
            })            
        }

        $scope.aggiungiAllegati = function () {
            $scope.pagina.contenuti.push({
                id: $scope.idContenuto(),
                tipo:'allegati'
            })            
        }

        $scope.aggiungiAlert = function () {
            $scope.pagina.contenuti.push({
                id: $scope.idContenuto(),
                tipo:'alert',
                tono: 'warning',
                testo:'Testo avviso'
            })            
        }

        $scope.aggiungiLinks = function () {

            $scope.pagina.contenuti.push({
                id: $scope.idContenuto(),
                tipo:'links',
                links: []
            })

            $scope.current =  $scope.pagina.contenuti[$scope.pagina.contenuti.length -1];          
        }

        $scope.aggiungiTabella = function () {

            $scope.pagina.contenuti.push({
                id: $scope.idContenuto(),
                tipo:'tabella',
                data: [
                    ['Colonna 1','Colonna 2'],
                    ['A','B'],
                    ['C','D']
                ]
            })     
        }


        
        $scope.aggiungiRiga = function (tabella, riga,index) {

            var clone = _.collect(_.clone(riga), function(item){
                return 'inserisci testo'
            })
            
            tabella.splice(index+1,0,clone)
        }

        

        $scope.eliminaRiga = function (tabella,index) {

            console.log(index)
            
            tabella.splice(index,1)

        }

    

        $scope.aggiungiColonna = function (tabella, index) {

           var clone =[];
           tabella.forEach(function(row){
            row.push('Nuova colonna')
           })
        }


        $scope.settaLivelloHeading = function (heading, livello) {
            heading.livello = livello;
        }

        $scope.showActions = function (contenuto) {
            contenuto.showActions = true;
        }

        $scope.hideActions = function (contenuto) {
            contenuto.showActions = false;
        }

        $scope.cercaPagine = function (chiave) {
            return PAGINA.query({testo: chiave }).$promise;
        }

        $scope.selezionaPagina = function (pagina) {

            var p;

            if(pagina) 
                p = { titolo: pagina.titolo, descrizione:'Clicca qui', url: pagina._id, internal: true };
            else 
                p = { titolo: this.$parent.bindModel, descrizione:'Clicca qui', url: this.$parent.bindModel, internal: false }

            $scope.current.links.push(p);       
        }

        $scope.setCurrent = function (c){
            $scope.current = c;
        }
        // trascinamento dei file: scatta l'upload su cloudinary
        $scope.uploadImmagine = function (files, contenuto) {


            $scope.immagini = files;
            var caricate = [];

            if ($scope.immagini.length) {
                async.each($scope.immagini, function (file, cb) {
                    file.upload = Upload.upload({
                        url: UTILITIES.cloudinary_upload_api,
                        fields: {
                            upload_preset: UTILITIES.cloudinary_upload_preset
                        },
                        file: file,
                        skipAuthorization: true
                    })
                    file.upload.then(

                        function (response) {
                            if (response.status === 200) {
                                caricate.push({
                                    id: response.data.public_id,
                                    alt: file.name.substr(0, file.name.lastIndexOf('.')),
                                    didascalia: ''
                                }); // pusho
                                cb();
                            } else {
                                cb(response);
                            }
                        },
                        function (resp) {
                            console.log('Error status: ' + resp.status);
                        },
                        function (evt) {
                            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                            contenuto.progress = progressPercentage; // aggiorno la percentuale del singolo file
                        }
                    );
                }, function (err) {
                    if (err)
                        UTILITIES.errorMessage(pagina,utente,err.data);

                    delete contenuto.progress;

                    contenuto.immagine = caricate[0];

                })
            }

        }

        // trascinamento dei file: scatta l'upload su S3
        $scope.uploadAllegati = function (files, contenuto) {

            $scope.files = files;
            var caricati = [];

            if ($scope.files.length) {
                modalUpload.show();
                async.each($scope.files, function (file, cb) {

                    var filetype = file.type !== "" ? file.type : 'application/octet-stream', // mi serve per il signed url
                        stamp = new Date().getTime(),
                        percorso = 'pagine/'+stamp+'/'+file.name;

                    $http
                        ({
                            url: '/s3',
                            data: {
                                percorso: percorso,
                                tipo: filetype,
                                dimensione: file.size
                            },
                            method: 'POST',
                            skipAuthorization: true // non passo dal autorizzazione con token, s3 si incazza
                        })
                        .success(function (url) {
                            // ho un signed url per l'upload su amazon
                            file.upload = Upload.http({
                                url: url,
                                data: file,
                                method: 'PUT',
                                skipAuthorization: true, // non passo dal autorizzazione con token di auth0, s3 si incazza
                                headers: {
                                    'Content-Type': filetype
                                }
                            });

                            file.upload.then(
                                function (response) {
                                    if (response.status === 200) {
                                        caricati.push({
                                            name: file.name.substr(0, file.name.lastIndexOf('.')), // togli l'estensione
                                            url: response.config.url.split('?')[0],
                                        }); // salvo nome e l'url su s3 di quello caricato
                                        cb();
                                    } else {
                                        cb(response)
                                    }
                                },
                                function (resp) {
                                    console.log('Error status: ' + resp.status);

                                },
                                function (evt) {
                                    var index = $scope.files.indexOf(file);
                                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                                    $scope.files[index].progress = progressPercentage; // aggiorno la percentuale del singolo file
                                }
                            );
                        });
                }, function (err) {
                    if (err) {
                        UTILITIES.errorMessage(pagina,utente,err.data);
                    } else {

                        if(contenuto.files) {
                            contenuto.files = _.union(caricati, contenuto.files);
                        }
                        else {
                            contenuto.files = _.sortBy(caricati, function(c){
                                return c.name;
                            })
                        }

                        modalUpload.hide();
                        ngNotify.set(CONFIG.messages.upload_completed, 'success');
                    }
                })
            }

        }

}]);
