CKEDITOR.plugins.add( 'info', {
    icons: 'info',
    init: function( editor ) {
        editor.addCommand( 'insertInfo', {
            exec: function( editor ) {
                editor.insertHtml( '<div class="alert alert-info">Inserisci qui il testo</div>');
            }
        });
        editor.ui.addButton( 'Info', {
            label: 'Inserisci un messaggio di avviso',
            command: 'insertInfo',
            toolbar: "insert"
        });
    }
});