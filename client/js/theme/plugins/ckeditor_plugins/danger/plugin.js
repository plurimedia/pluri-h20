CKEDITOR.plugins.add( 'danger', {
    icons: 'danger',
    init: function( editor ) {
        editor.addCommand( 'insertDanger', {
            exec: function( editor ) {
                editor.insertHtml( '<div class="alert alert-danger">Inserisci qui il testo</div>');
            }
        });
        editor.ui.addButton( 'Danger', {
            label: 'Inserisci un messaggio di avviso',
            command: 'insertDanger',
            toolbar: "insert"
        });
    }
});