app
    .factory('BANDO', function ($resource) {
        return $resource('/secure/bandi/:id', {
            id: '@_id' // metti come id la proprietà _id dell'oggetto
        }, {
            update: {
                method: 'PUT' // this method issues a PUT request
            },
            count: {
                method: 'GET',
                url: '/secure/bandi/count'
            }
        });
    })
    .factory('ESITO', function ($resource) {
        return $resource('/secure/esiti/:id', {
            id: '@_id' // metti come id la proprietà _id dell'oggetto
        }, {
            update: {
                method: 'PUT' // this method issues a PUT request
            },
            count: {
                method: 'GET',
                url: '/secure/esiti/count'
            }
        });
    })
    .factory('COMUNICATO', function ($resource) {
        return $resource('/secure/comunicati/:id', {
            id: '@_id' // metti come id la proprietà _id dell'oggetto
        }, {
            update: {
                method: 'PUT' // this method issues a PUT request
            },
            count: {
                method: 'GET',
                url: '/secure/comunicati/count'
            }
        });
    })
    .factory('EVENTO', function ($resource) {
        return $resource('/secure/eventi/:id', {
            id: '@_id' // metti come id la proprietà _id dell'oggetto
        }, {
            update: {
                method: 'PUT' // this method issues a PUT request
            },
            count: {
                method: 'GET',
                url: '/secure/eventi/count'
            }
        });
    })
    .factory('CASETTA', function ($resource) {
        return $resource('/secure/casette/:id', {
            id: '@_id' // metti come id la proprietà _id dell'oggetto
        }, {
            update: {
                method: 'PUT' // this method issues a PUT request
            },
            count: {
                method: 'GET',
                url: '/secure/casette/count-casette'
            } 
        });
    })
    .factory('EROGATORE', function ($resource) {
        return $resource('/secure/casette/:id', {
            id: '@_id', // metti come id la proprietà _id dell'oggetto
            tipo: 'erogatore' //chiama la stessa api ma anzichè prendere casetta prende erogatore
        }, {
            update: {
                method: 'PUT' // this method issues a PUT request
            },
            count: {
                method: 'GET',
                url: '/secure/casette/count-erogatori'
            } 
        });
    })
    .factory('DISTRIBUTORE', function ($resource) {
        return $resource('/secure/casette/:id', {
            id: '@_id', // metti come id la proprietà _id dell'oggetto
            tipo: 'distributore' //chiama la stessa api ma anzichè prendere casetta prende erogatore
        }, {
            update: {
                method: 'PUT' // this method issues a PUT request
            },
            count: {
                method: 'GET',
                url: '/secure/casette/count-distributori'
            } 
        });
    })
    .factory('CASETTARISPARMIO', function ($resource) {
        return $resource('/casettereportrisparmio/:id', {
            id: '@_id' // metti come id la proprietà _id dell'oggetto
        },
        {
            risparmio: {
                isArray: false ,
                url: '/casettereportrisparmio/:id/:anno',
                method: 'GET'  
            }, 
            reportPdf: {
                url: '/casettereportrisparmio_pdf/:id/:anno',
                method: 'GET',
                responseType: 'arraybuffer',
                transformResponse: function (data) {
                var blob = new Blob([data], {type: "application/pdf"}),
                    filename = "report-risparmio-energetico.pdf";
                    saveAs(blob, filename);
                }
            },
            reportXls: {
                url: '/casettereportrisparmio_xls/:id/:anno',
                method: 'GET',
                isArray: true
            } 

        });
    })
    .factory('VIDEO_CASETTA', function ($resource) {
        return $resource('/secure/video_casetta/:id', {
            id: '@_id' // metti come id la proprietà _id dell'oggetto
        }, {
            update: {
                method: 'PUT' // this method issues a PUT request
            }
        });
    })
    .factory('INTERVENTO_CASETTA', function ($resource) {
        return $resource('/secure/interventi_casetta/:id', {
            id: '@_id' // metti come id la proprietà _id dell'oggetto
        }, {
            update: {
                method: 'PUT' // this method issues a PUT request
            }
        });
    })
    .factory('LETTURA_CASETTA', function ($resource) {
        return $resource('/secure/lettura_casetta/:id', {
            id: '@_id' // metti come id la proprietà _id dell'oggetto
        }, {
            update: {
                method: 'PUT' // this method issues a PUT request
            }
        });
    })
    .factory('CONSUMI', function ($resource) {
        return $resource('/secure/consumi_casetta/:id', {
            id: '@_id' // metti come id la proprietà _id dell'oggetto
        });
    })
    .factory('LOG', function ($resource) {
        return $resource('/secure/logs/:id', {
            id: '@_id'}, // metti come id la proprietà _id dell'oggetto
            { codes: {
                method: 'GET',
                isArray: true,
                url: '/secure/logcodes'
            }
        });
    })
    .factory('PAGINA', function ($resource) {
        return $resource('/secure/pagine/:id', {
            id: '@_id' // metti come id la proprietà _id dell'oggetto
        }, {
            update: {
                method: 'PUT' // this method issues a PUT request
            },
            count: {
                method: 'GET',
                url: '/secure/pagine/count'
            }
        });
    })
    .factory('MENU', function ($resource) {
        return $resource('/secure/menues/:id', {
            id: '@_id' // metti come id la proprietà _id dell'oggetto
        }, {
            update: {
                method: 'PUT' // this method issues a PUT request
            },
            count: {
                method: 'GET',
                url: '/secure/menues/count'
            }
        });
    })
    .factory('FAQ', function ($resource) {
        return $resource('/secure/faqs/:id', {
            id: '@_id' // metti come id la proprietà _id dell'oggetto
        }, {
            update: {
                method: 'PUT' // this method issues a PUT request
            },
            count: {
                method: 'GET',
                url: '/secure/faqs/count'
            }
        });
    })
     .factory('GLOSSARIO', function ($resource) {
        return $resource('/secure/glossario/:id', {
            id: '@_id' // metti come id la proprietà _id dell'oggetto
        }, {
            update: {
                method: 'PUT' // this method issues a PUT request
            },
            count: {
                method: 'GET',
                url: '/secure/glossario/count'
            }
        });
    })
    .factory('SPORTELLO', function ($resource) {
        return $resource('/secure/sportelli/:id', {
            id: '@_id' // metti come id la proprietà _id dell'oggetto
        }, {
            update: {
                method: 'PUT' // this method issues a PUT request
            },
            count: {
                method: 'GET',
                url: '/secure/sportelli/count'
            }
        });
    })
    .factory('AVVISO', function ($resource) {
        return $resource('/secure/avvisi/:id', {
            id: '@_id'  
        }, {
            update: {
                method: 'PUT'
            }
        });
    })
    .factory('LABORATORIO', function ($resource) {
        return $resource('/laboratorio/analisi/:id', {
            id: '@_id'  
        }, {
            //Regole import dati
            updateRegole: {
                method: 'PUT',
                url: '/secure/laboratorio/regole/:id'
            }, 
            listaRegole: {
                method: 'GET',
                url: '/secure/laboratorio/regole'
            },
            listaAnalisi: {
                method: 'GET',
                isArray:true,
                url: '/secure/laboratorio/analisi'
            },
            deleteAnalisi: {
                method: 'DELETE',
                url: '/secure/laboratorio/analisi/:id'
            }, 
            updateAnalisi: { // aggiornamento massivo
                method: 'GET',
                url: '/secure/laboratorio/analisi/update'                
            }
        });
    })
    .factory('ARTICOLO', function ($resource) {
        return $resource('/secure/articoli/:id', {
            id: '@_id' // metti come id la proprietà _id dell'oggetto
        }, {
            update: {
                method: 'PUT' // this method issues a PUT request
            },
            count: {
                method: 'GET',
                url: '/secure/articoli/count'
            }
        });
    })
    .factory('NEWSLETTER', function ($resource) {
        return $resource('/secure/newsletters/:id', {
            id: '@_id' // metti come id la proprietà _id dell'oggetto
        }, {
            update: {
                method: 'PUT' // this method issues a PUT request
            },
            count: {
                method: 'GET',
                url: '/secure/newsletters/count'
            },
            testmail: {
                method: 'GET',
                url: '/secure/newsletters/testmail'
            }
        });
    })
    .factory('MAILCHIMP', function ($resource) {
        return $resource('/secure/mailchimp/:id', {
            id: '@_id' // metti come id la proprietà _id dell'oggetto
        }, {
             list: {method: 'GET', isArray:true, url: '/secure/mailchimp/list'},
             newcampaign: {method: 'POST', url: '/secure/mailchimp/newcampaign'},
        });
    })
    .factory('SLUG', function ($resource) {
        return $resource('/secure/slug/:id', {
            id: '@_id' // metti come id la proprietà _id dell'oggetto
        }, {
            update: {
                method: 'PUT' // this method issues a PUT request
            }
        });
    })
    .factory('STATS', function ($resource,$rootScope) {
        var apiurl = $rootScope.CONFIG.urlApiServizioClienti;
        return $resource(apiurl + 'richieste/stats/:anno', {
            id: '@_id' // metti come id la proprietà _id dell'oggetto
        }, {
            richiestaAssistenza: {
                method: 'GET',
                url: apiurl + 'richieste/stats/:anno/:mese',
                isArray: true
            },
            richiestaAssistenzaTipo: {
                method: 'GET',
                url: apiurl + 'richieste/statsTipo/:anno/:mese',
                isArray: true
            }
        });
    })
     

    /* Recupero utenti da auth0  */
    .factory('AUTH0', function ($resource, $q) {
        var auth0 = {};
        
        // operatori casette
        auth0.operatoriCasette = function () {
            var operatori = $resource("/secure/auth0/operatori_casette");
            return operatori.query().$promise.then(function (data) {
                return data;
            });
        };

        return auth0;
    })

    // export excel, vale per tutti gli export
    .factory('XLS', function ($resource) {
        return $resource('secure/xls', {}, {
                    export: {
                        method: 'POST',
                        responseType: 'arraybuffer',
                        transformResponse: function (data) {
                            var a = document.createElement("a");
                            document.body.appendChild(a);
                            a.style = "display: none";
                            var blob = new Blob([data], {type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"});
                            var url = window.URL.createObjectURL(blob);
                            a.href = url;
                            a.download = 'export.xlsx';
                            a.click();
                            window.URL.revokeObjectURL(url);
                        }
                    }
                });
    })
    /* import csv gmail */
    .factory('CSV', function (Upload) {

        // servizio che riceve un file csv lo carica e attende che venga processato lato server
        var CSV = {};

        CSV.upload = function (file,tipo,callback) {
 
            file.upload = Upload.upload({
                url: '/secure/csv/import/'+tipo,
                file: file,
                method: 'POST',
                headers: {'Content-Type': file.type}
            });

            file.upload.then(function (response) {
                if(response.status === 200) {
                    callback(response.data);
                }
            });

        };
        return CSV;
    }) 
    .factory('XLS_LABORATORIO', function (Upload) {

        // servizio che riceve un file xls lo carica e attende che venga processato lato server
        var XLS = {};

        XLS.upload = function (file,tipo,callback) {
 
            file.upload = Upload.upload({
                url: '/secure/xls/import/'+tipo,
                file: file,
                method: 'POST',
                headers: {'Content-Type': file.type}
            });

            file.upload.then(function (response) {
                if(response.status === 200) {
                    callback(response.data);
                }
            });

        };
        return XLS;
    }) 

    // funzioni di utilità generale cross-controllers
    .factory('UTILITIES', function ($resource, $location, ngNotify) {
        var utilities = {};

        // chiave per upload unsigned su cloudinary, cambia da sviluppo a produzione
        utilities.cloudinary_upload_preset = $location.absUrl().indexOf('localhost') !== -1 ? 'toaunttf' : 'xbfmmey3';
        // endpoint per upload lato client su cloudinary
        utilities.cloudinary_upload_api = 'https://api.cloudinary.com/v1_1/plurimedia/upload';

        /* setta un messaggio di errore con indicazioni per richiedere assistenza */
        utilities.errorMessage = function (pagina,utente,errore, erroreReteOffLine) {
            var msg = "";
            if (erroreReteOffLine)
                msg = 'Rete off-line <br /><br /> I dati verranno sincronizzati non appena ritornerai on-line'
            else
                msg = 'Oops, qualcosa è andato storto <br /><br /> <a class="btn btn-warning" href="mailto:centroclienti@plurimedia.it?subject=Errore sul Backend &body=pagina: '+pagina+'%0D%0A utente:'+utente+' %0D%0A errore:'+encodeURIComponent(errore)+'"><i class="fa fa-envelope"></i> Clicca qui per richiedere assistenza</a>'
            ngNotify.set(msg, {
                position: 'top',
                type: 'error',
                html: true,
                sticky: true
            });           
        }

        /* Anni disponibili nel filtro delle ricerche*/
        utilities.anniFiltro = function () {
                var anni = [moment().format('YYYY')]
                _(10).times(function (i) {
                    anni.push(moment().subtract(i + 1, 'years').format('YYYY'));
                });
                return anni;
            }
        var mesi = 
        [
           { id:1,nome:'Gennaio'},
           { id:'2',nome:'Febbraio'},
           { id:'3',nome:'Marzo'},
           { id:'4',nome:'Aprile'},
           { id:'5',nome:'Maggio'},
           { id:'6',nome:'Giugno'},
           { id:'7',nome:'Luglio'},
           { id:'8',nome:'Agosto'},
           { id:'9',nome:'Settembre'},
           { id:'10',nome:'Ottobre'},
           { id:'11',nome:'Novembre'},
           { id:'12',nome:'Dicembre'}
        ]

        utilities.nomeMese = function (num) {
            mese = _.filter(mesi, function(m){
                return m.id == num;
            })
           
            return mese[0].nome;
        }

        utilities.mesi = function () {
            return mesi;
        }

        /* Tipi di bando */
        utilities.tipiBando = function () {
                return ['Manifestazione di interesse', 'Procedura aperta', 'Sorteggio pubblico', 'Avviso'];
            }
        /* Stati degli esiti */
        utilities.statiEsito = function () {
                return ['Assegnato', 'Non assegnato'];
            }
        /* Stati delle casette */
        utilities.statiCasette = function () {
                return ['Attiva','In manutenzione','Da attivare'];
            }
        /* mese attuale */
        utilities.meseAttuale = function () {
            return new Date().toLocaleString("it", { month: "long" }); // result: Aprile
        }
        /* le categorie upload di default dei bandi*/
        utilities.categorieUploadBandi = function () {
            return [
                {
                    _id: 0,
                    nome: 'Documenti generali',
                    sistema: true,
                    type: 'categoria' // mi serve per impedire il drag & drop di una categoria in un file
                },
                {
                    _id: 1,
                    nome: 'Chiarimenti',
                    sistema: true,
                    type: 'categoria'
                },
                {
                    _id: 2,
                    nome: 'Comunicazioni',
                    sistema: true,
                    type: 'categoria'
                }];
        }

        /* le categorie upload di default degli esiti */
        utilities.categorieUploadEsiti = function () {
            return [
                {
                    _id: 0,
                    nome: 'Esito del bando',
                    sistema: true,
                    type: 'categoria' // mi serve per impedire il drag & drop di una categoria in un file
                }
            ];
        }

        /* le categorie upload di default dei comunicati */
        utilities.categorieUploadComunicati = function () {
            return [
                {
                    _id: 0,
                    nome: 'Allegati',
                    sistema: true,
                    type: 'categoria' // mi serve per impedire il drag & drop di una categoria in un file
                }
            ];
        }

        /* le categorie upload di default degli eventi */
        utilities.categorieUploadEventi = function () {
            return [
                {
                    _id: 0,
                    nome: 'Allegati',
                    sistema: true,
                    type: 'categoria' // mi serve per impedire il drag & drop di una categoria in un file
                }
            ];
        }

        /* le categorie upload di default delle casette */
        utilities.categorieUploadCasette = function () {
            return [
                {
                    _id: 0,
                    nome: 'Allegati',
                    sistema: true,
                    type: 'categoria' // mi serve per impedire il drag & drop di una categoria in un file
                }
            ];
        }

        /* le categorie upload di default degli articoli */
        utilities.categorieUploadArticoli = function () {
            return [
                {
                    _id: 0,
                    nome: 'Allegati',
                    sistema: true,
                    type: 'categoria' // mi serve per impedire il drag & drop di una categoria in un file
                }
            ];
        }
        /* 
            include i plugin extra e definisce le impostazioni di ckeditor
            @input: small/undefined
            @toolbar:l'elenco dei componenti da mostrare nella toolbar
    
        */
        utilities.ckEditor = function (small, toolbar) {
            // includi i plugin extra di ckeditor
            CKEDITOR.plugins.addExternal('alert', '/js/theme/plugins/ckeditor_plugins/alert/');
            CKEDITOR.plugins.addExternal('info', '/js/theme/plugins/ckeditor_plugins/info/');
            CKEDITOR.plugins.addExternal('danger', '/js/theme/plugins/ckeditor_plugins/danger/');
            CKEDITOR.plugins.addExternal('bt_table', '/js/theme/plugins/ckeditor_plugins/bt_table/');

            var tb = [];
            if (toolbar)
                tb = toolbar;
            else 
                tb = [['Source', 'Bold', 'Italic', 'Link', 'NumberedList', 'BulletedList', 'PasteText', 'PasteFromWord', 'Table', 'Info', 'Alert', 'Danger']];
            return {
                extraPlugins: 'alert,info,danger,bt_table', // bottoni extra
                contentsCss: '/css/ckeditor.css', // un bootstrap senza font awesome che incasina
                extraAllowedContent: 'div(alert,alert-*,table-responsive);table(table,table-*)', // consenti solo questi elementi con queste classi
                language: 'it',
                height: small ? '100px' : '300px',
                toolbar: tb
            };
        }

        utilities.ckEditorRidotto = function () {
            return {
                extraPlugins: 'divarea',
                contentsCss: '/css/ckeditor.css', // un bootstrap senza font awesome che incasina
                language: 'it',
                toolbar: [['Bold', 'Italic', 'NumberedList', 'BulletedList','Link']],
                height: '100px'
            };
        }

         /* tipo di tag */
        utilities.elencoTag = function () {
           return [
           'Acquedotto','Ambiente','Azienda','Brand identity',
           'Case dell\'acqua','Comunicati','Corsi',
           'Depurazione','Dipendenti',
           'Educazione','Eventi',
           'Fognatura',
           'Gli uffici si presentano',
           'Iniziative','Investimenti','Intranet','Istituzionale',
           'Laboratori',
           'Pensionamenti','Personale','Progettazione','Progetti',
           'Servizi', 'Sicurezza','Sociale','Sostenibilità','Tecnologia e innovazione']
        }

        /* tipo di sezioni */
        utilities.elencoSezioni = function () {
           return ['Interno','Esterno','Entrambi']
        } 

        /*tipo di target*/
        utilities.elencoTarget = function () {
            return [
                {'id':'interna','name':'Interna'},
                {'id':'esterna','name':'Esterna'},
                {'id':'refcom','name':'Ref. Comunicazione'}
            ]
        } 

        utilities.elencoUsoFile = function () {
            return [ 
                {'id':'esterno','name':'Visibile sul sito'},
                {'id':'interno','name':'Non visibile sul sito'}
            ]
        }

        // i bottoni conferma e cancella di bootbox
        utilities.confirmButtons = {
            'cancel': {
                label: 'Annulla',
                className: 'btn-default'
            },
            'confirm': {
                label: 'Conferma',
                className: 'btn-primary'
            }
        };
        
        // comuni BA con il loro codice
        utilities.comuni = [
            {nome:'Aicurzio', codice: 'AI'},
            {nome:'Albiate', codice: 'AT'},
            {nome:'Agrate Brianza', codice: 'AB'},
            {nome:'Arcore', codice: 'AR'},
            {nome:'Barlassina', codice: 'BN'},
            {nome:'Bellusco', codice: 'BL'},
            {nome:'Bernareggio', codice: 'BR'},
            {nome:'Besana Brianza', codice: 'BZ'},
            {nome:'Biassono', codice: 'BI'},
            {nome:'Bovisio Masciago', codice: 'BV'},
            {nome:'Briosco', codice: 'BO'},
            {nome:'Brugherio', codice: 'BG'},
            {nome:'Burago Molgora', codice: 'BM'},
            {nome:'Busnago', codice: 'BU'},
            {nome:'Cabiate', codice: 'CB'},
            {nome:'Camparada', codice: 'CM'},
            {nome:'Caponago', codice: 'CP'},
            {nome:'Carate', codice: 'CT'},
            {nome:'Carnate', codice: 'CR'},
            {nome:'Cavenago Brianza', codice: 'CI'},
            {nome:'Ceriano Laghetto', codice: 'CL'},
            {nome:'Cesano Maderno', codice: 'CE'},
            {nome:'Cogliate', codice: 'CG'},
            {nome:'Concorrezzo', codice: 'CN'},
            {nome:'Cornate d\'Adda', codice: 'CO'},
            {nome:'Correzzana', codice: 'CZ'},
            {nome:'Desio', codice: 'DE'},
            {nome:'Giussano', codice: 'GI'},
            {nome:'Gorgonzola', codice: 'GO'},
            {nome:'Lazzate', codice: 'LZ'},
            {nome:'Lentate sul Seveso', codice: 'LN'},
            {nome:'Lesmo', codice: 'LE'},
            {nome:'Limbiate', codice: 'LB'},
            {nome:'Lissone', codice: 'LS'},
            {nome:'Meda', codice: 'MD'},
            {nome:'Macherio', codice: 'MC'},
            {nome:'Mezzago', codice: 'MZ'},
            {nome:'Misinto', codice: 'MI'},
            {nome:'Monza', codice: 'MO'},
            {nome:'Muggiò', codice: 'MU'},
            {nome:'Nova Milanese', codice: 'NM'},
            {nome:'Ornago', codice: 'OR'},
            {nome:'Pozzo D’Adda', codice: 'PO'},
            {nome:'Renate', codice: 'RE'},
            {nome:'Roncello', codice: 'RO'},
            {nome:'Ronco Briantino', codice: 'RB'},
            {nome:'Seregno', codice: 'SE'},
            {nome:'Seveso', codice: 'SV'},
            {nome:'Sovico', codice: 'SO'},
            {nome:'Sulbiate', codice: 'SU'},
            {nome:'Trezzano Rosa', codice: 'TR'},
            {nome:'Trezzo sull\'Adda', codice: 'TS'},
            {nome:'Triuggio', codice: 'TG'},
            {nome:'Trucazzano', codice: 'TU'},
            {nome:'Usmate Velate', codice: 'UV'},
            {nome:'Varedo', codice: 'VR'},
            {nome:'Vedano al Lambro', codice: 'VL'},
            {nome:'Verano Brianza', codice: 'VB'},
            {nome:'Veduggio con Colzano', codice: 'VC'},
            {nome:'Villasanta', codice: 'VS'},
            {nome:'Vimercate', codice: 'VM'},
            {nome:'Diversi comuni', codice: '00'}

        ];
    
        // restituisce il nome di un comune dal suo codice
        utilities.decodificaComune = function (codice){
            return _.find(utilities.comuni, function(c){return c.codice===codice}).nome;
        }
        
        // resitutisce la descrizione di una casetta dal suo comune e dal suo indirizzo
        utilities.descrizioneCasetta = function (casetta) {
            var n = utilities.decodificaComune(casetta.comune);
            if(casetta.indirizzo)
                n += ', '+casetta.indirizzo;
            return n;
        }

        /*
         Il metodo concatena l'indizirizo completo di una località a partire 
         dal Codice Comune(mandatory) +  l'indirizzo 
         @input: Codice Comune, Indirizzo
         @output: es: Desio, via Caconico, 27
        */
        utilities.manageIndirizzo = function (codiceComune, indirizzo) 
        {
            if (!codiceComune)
                return "";
            var n = utilities.decodificaComune(codiceComune);
            if(indirizzo)
                n += ', '+indirizzo;
            return n;
        }

        utilities.tipiVideoCasette = function () {
            return [
                {
                    codice: 10,
                    nome: 'Solo il video'
                },
                {
                    codice: 20,
                    nome: 'Solo la pagina delle analisi'
                },
                {
                    codice: 30,
                    nome: 'Entrambi'
                }                    
            ];

        }

        return utilities;
    });
