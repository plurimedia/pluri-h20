FROM node:14.18-slim

# Define Variables
ARG ApplicationName="plurih2o"
ARG ApplicationRoot="/opt/app-root/$ApplicationName"

# Create WORKDIR
RUN mkdir -p "$ApplicationRoot" && \
    mkdir "${ApplicationRoot}/.tmp" && \
    chmod 777 "${ApplicationRoot}/.tmp"

RUN apt-get update && \
    apt-get install -y tar bzip2 libnss3 libfontconfig1 fontconfig libfontconfig1-dev libfontconfig git

# Set WORKDIR
WORKDIR "$ApplicationRoot"

# Copy basic configuration files
COPY app.js ibm_mongodb_ca.pem package.json Gruntfile.js bower.json .bowerrc "$ApplicationRoot"/

# Clean Install
# npm install dependencies
RUN npm install -g bower && \
    npm install nodemon && \
    bower install --allow-root

COPY npm-shrinkwrap.json "$ApplicationRoot"/

RUN npm install

# Copy SRC
COPY ./client "$ApplicationRoot"/client
COPY ./server "$ApplicationRoot"/server
COPY ./scripts "$ApplicationRoot"/scripts

EXPOSE 8080
CMD npm run dev
