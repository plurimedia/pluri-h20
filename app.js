// test branch casette 2
var CONFIG = require(process.cwd() + '/server/config.js'),
    express = require('express'), // express
    multipart = require('connect-multiparty'), // per uplaod files
    cors = require('cors'), // stabilisce i cors per le api
    jwt = require('express-jwt'),
    jwks = require('jwks-rsa'),
    mkdirp = require('mkdirp'),
    fs = require('fs'),
    mongoose = require('mongoose'), // mongoose wrapper for mongo
    bodyParser = require('body-parser'), // body parser middleware
    json2xls = require('json2xls'), // export in excel, ha un middleware per la risposta
    helmet = require('helmet'), // previene gli attacchi più comuni alle app fatte con express
    mongoSanitize = require('express-mongo-sanitize'), // previene le injection
    app = express(), // express app
    mongoUrl = CONFIG.MONGO,
    _ = require('underscore'),
    mustacheExpress = require('mustache-express'),
    /* @@@@@@ models @@@@@@ */
    BandoSchema = require('./server/models/bando'),
    EsitoSchema = require('./server/models/esito'),
    ComunicatoSchema = require('./server/models/comunicato'),
    EventoSchema = require('./server/models/evento'),
    CasettaSchema = require('./server/models/casetta'),
    VideoCasettaSchema = require('./server/models/video_casetta'),
    InterventoCasettaSchema = require('./server/models/intervento_casetta'),
    PaginaSchema = require('./server/models/pagina'),
    MenuSchema = require('./server/models/menu'),
    LogSchema = require('./server/models/log'),
    FaqSchema = require('./server/models/faq'),
    GlossarioSchema = require('./server/models/glossario'),
    SportelloSchema = require('./server/models/sportello'),
    AvvisoSchema = require('./server/models/avviso'),
    ArticoloSchema = require('./server/models/articolo'),
    NewsletterSchema = require('./server/models/newsletter'),
    LaboratorioRegolaSchema = require('./server/models/laboratorio_regola'),
    LaboratorioAnalisiSchema = require('./server/models/laboratorio_analisi'),
    LetturaCasetta = require('./server/models/lettura_casetta'),


    /* @@@@@@ middleware @@@@@@ */
    Authz = require(process.cwd() + '/server/middleware/authz.js'), // gestisce le logiche di autorizzazione per le routes
    /* @@@@@@ routes @@@@@@ */
    Bando = require('./server/routes/bando'),
    Esito = require('./server/routes/esito'),
    Comunicato = require('./server/routes/comunicato'),
    Evento = require('./server/routes/evento'),
    Casetta = require('./server/routes/casetta'),
    VideoCasetta = require('./server/routes/video_casetta'),
    InterventoCasetta = require('./server/routes/intervento_casetta'),
    LetturaCasetta = require('./server/routes/lettura_casetta'),
    Pagina = require('./server/routes/pagina'),
    Menu = require('./server/routes/menu'),
    Log = require('./server/routes/log'),
    Auth0 = require('./server/routes/auth0'),
    Slug = require('./server/routes/slug'),
    S3 = require('./server/routes/s3'),
    Excel = require('./server/routes/excel'),
    Email = require('./server/routes/mail'),
    Faq = require('./server/routes/faq'),
    Glossario = require('./server/routes/glossario'),
    Sportello = require('./server/routes/sportello'),
    Avviso = require('./server/routes/avviso'),
    Articolo = require('./server/routes/articolo'),
    Newsletter = require('./server/routes/newsletter'),
    Mailchimp = require('./server/routes/mailchimp'),
    LaboratorioRegola = require('./server/routes/laboratorio_regola'),
    LaboratorioAnalisi = require('./server/routes/laboratorio_analisi'),

    /* @@@@@@ api @@@@@@ */
    ApiV1 = require('./server/api/v1.js'),
    ApiV2 = require('./server/api/v2.js'),
    mongoOptions = {
        server: {
            socketOptions: {
                keepAlive: 1
            }
        }
    };
    /* @@@@@@ crons @@@@@@ */
    // cronVideoCasette = require('./server/cron/videoCasette'),
    // cronVideoCasette = require('./server/cron/statoVideoCasette');


// Register '.mustache' extension with The Mustache Express
app.engine('mustache', mustacheExpress());
app.set('view engine', 'mustache');
app.set('views', process.cwd() + '/server/templates');

/* @@@@@@ mongo @@@@@@ */
certFileDB = fs.readFileSync('./ibm_mongodb_ca.pem'); // Carica certificato SSL
// Controllo se le variabili di OpenShift sono popolate
if (process.env.MONGOUSERNAME && process.env.MONGOPWD && process.env.MONGOHOST && process.env.MONGODBNAME) {
  mongoUrl = "mongodb://" + process.env.MONGOUSERNAME +
    ":" + process.env.MONGOPWD +
    "@" + process.env.MONGOHOST +
    "/" + process.env.MONGODBNAME

  if (process.env.MONGOOPTIONS) {
    mongoUrl = mongoUrl + "?" + process.env.MONGOOPTIONS
  }
} else {
  mongoUrl = CONFIG.MONGO // Variabile impostata nei config.js
}

var mongoOptions = {
  socketOptions: { keepAlive: 1 },
  sslValidate: true,
  sslCA: certFileDB,
  useNewUrlParser: true
}

mongoose.Promise = global.Promise;
mongoose.connect(mongoUrl, mongoOptions, function (err) {
  if (err) throw err;

  console.info('connected to ', mongoUrl);
  console.info('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ ambiente', CONFIG.ENV);
  console.info('config', CONFIG);
});

/*
if (!process.env.NODE_ENV) mongoose.set('debug', true);*/

// cors origins
var whitelist = ['http://localhost:8080','http://www.brianzacque.it','https://brianzacque-preprod.herokuapp.com'],
    corsOptions = {
        origin: function (origin, callback) {
            var originIsWhitelisted = whitelist.indexOf(origin) !== -1;
            callback(null, originIsWhitelisted);
        }
    };

// token d auth0
var authenticate = jwt({
  secret: jwks.expressJwtSecret({
    cache: true,
    rateLimit: true,
    jwksRequestsPerMinute: 5,
    jwksUri: `https://${CONFIG.AUTH0_DOMAIN}/.well-known/jwks.json`
  }),
  aud: `https://${CONFIG.AUTH0_DOMAIN}/api/v2/`,
  algorithms: ['RS256']
});

const sistemaUser = function (req, res, next) {
  const namespace = 'https://plurih2o-backend-it/' // See Auth0 rules

  // console.log('sistemaUser', req.user)
  req.user.roles = req.user[namespace + 'app_metadata'].roles
  req.user.tenant = req.user[namespace + 'app_metadata'].tenant
  req.user.app_metadata = req.user[namespace + 'app_metadata']
  req.user.user_metadata = req.user[namespace + 'user_metadata']
  next()
}

mkdirp('.tmp', function (err) {
  if (err)
    console.error(err)
  else
    console.log('Creata la cartella temporanea')
});

// use body parser
app.use(bodyParser.json({ limit: '5mb' }))
app.use(helmet({ contentSecurityPolicy: false }))
app.use(mongoSanitize());
app.use('/secure', authenticate, sistemaUser);
app.use(cors());
app.use('/secure/xls', json2xls.middleware);
app.use(express.static('.tmp'));
app.use(express.static('client'));
app.use(multipart({ uploadDir: '.tmp' }));

/* @@@@@@@@@@@@@@ ROUTES @@@@@@@@@@@@@@  */

// bandi
app.post('/secure/bandi', Authz.role(['appalti','admin']),Bando.save); // salvataggio bando
app.get('/secure/bandi', Bando.query); // elenco bandi
app.get('/secure/bandi/count', Bando.count); // count bandi
app.get('/secure/bandi/:id', Bando.get); // get singolo bando
app.put('/secure/bandi/:id', Authz.role(['appalti','admin']), Bando.update); // update singolo bando
app.delete('/secure/bandi/:id', Authz.role(['appalti','admin']),Bando.delete); // delete singolo bando
// esiti
app.post('/secure/esiti',Authz.role(['appalti','admin']),Esito.save); // salvataggio esito
app.get('/secure/esiti', Esito.query); // elenco esiti
app.get('/secure/esiti/count', Esito.count); // count esiti
app.get('/secure/esiti/:id', Esito.get); // get singolo esito
app.put('/secure/esiti/:id', Authz.role(['appalti','admin']), Esito.update); // update singolo esito
app.delete('/secure/esiti/:id', Authz.role(['appalti','admin']), Esito.delete); // delete singolo esito
// comunicati
app.post('/secure/comunicati',Authz.role(['comunicati','admin']), Comunicato.save); // salvataggio comunicato
app.get('/secure/comunicati', Comunicato.query); // elenco comunicati
app.get('/secure/comunicati/count', Comunicato.count); // count comunicati
app.get('/secure/comunicati/:id', Comunicato.get); // get singolo comunicato
app.put('/secure/comunicati/:id',Authz.role(['comunicati','admin']) ,Comunicato.update); // update singolo comunicato
app.delete('/secure/comunicati/:id',Authz.role(['comunicati','admin']),Comunicato.delete); // delete singolo comunicato
// eventi
app.post('/secure/eventi',Authz.role(['eventi','admin']), Evento.save); // salvataggio evento
app.get('/secure/eventi', Evento.query); // elenco comunicati
app.get('/secure/eventi/count', Evento.count); // count eventi
app.get('/secure/eventi/:id', Evento.get); // get singolo comunicato
app.put('/secure/eventi/:id',Authz.role(['eventi','admin']) ,Evento.update); // update singolo evento
app.delete('/secure/eventi/:id',Authz.role(['eventi','admin']),Evento.delete); // delete singolo evento
// casette & erogatori (come sottoinsieme di casette)
app.post('/secure/casette',Authz.role(['casette','erogatori','distributori','admin']), Casetta.save); // salvataggio casetta
app.get('/secure/casette', Casetta.query); // elenco casette
app.get('/secure/casette/count-casette', Casetta.countCasette); // count casette
app.get('/secure/casette/count-erogatori', Casetta.countErogatori); // count erogatori
app.get('/secure/casette/count-distributori', Casetta.countDistributori); // count erogatori

app.get('/casettereportrisparmio/:id/:anno',  Casetta.reportRisparmio); // pagina per il report risparmio
app.get('/casettereportrisparmio_pdf/:id/:anno', Casetta.reportRisparmioPdf); // pagina per il report risparmio
app.get('/casettereportrisparmio_xls/:id/:anno', Casetta.reportRisparmioXls); // pagina per il report risparmio

app.get('/secure/casette/:id', Casetta.get); // get singola casetta
app.put('/secure/casette/:id',Authz.role(['casette','admin','comune']), Casetta.update); // update singola casetta
app.delete('/secure/casette/:id',Authz.role(['casette','admin']), Casetta.delete); // delete singola casetta

app.get('/casette/:id/video', Casetta.video); // pagina per la casetta

// video disponibili per le casette
app.get('/secure/video_casetta/', VideoCasetta.query)

// interventi sulle casette (menutenzione e rilevamento)
app.post('/secure/interventi_casetta',Authz.role(['operatore_casette','admin']), InterventoCasetta.save); // salvataggio intervento
app.get('/secure/interventi_casetta', InterventoCasetta.query); // elenco interventi
app.get('/secure/interventi_casetta/:id', InterventoCasetta.get); // get singolo intervento
app.delete('/secure/interventi_casetta/:id',Authz.role(['operatore_casette','admin']), InterventoCasetta.delete); // delete singolo intervento

// pagine
app.post('/secure/pagine',Authz.role(['pagine','admin']), Pagina.save); // salvataggio pagina
app.get('/secure/pagine', Pagina.query); // elenco pagine
app.get('/secure/pagine/count', Pagina.count); // count pagine
app.get('/secure/pagine/:id', Pagina.get); // get singola pagina
app.put('/secure/pagine/:id',Authz.role(['pagine','admin']), Pagina.update); // update singola pagina
app.delete('/secure/pagine/:id',Authz.role(['pagine','admin']),Pagina.delete); // delete singola pagina

// menu
app.post('/secure/menues',Authz.role(['pagine','admin']), Menu.save); // salvataggio menu
app.get('/secure/menues', Menu.query); // elenco menu
app.get('/secure/menues/count', Menu.count); // count menues
app.get('/secure/menues/:id', Menu.get); // get singolo menu
app.put('/secure/menues/:id',Authz.role(['pagine','admin']), Menu.update); // update singolo menu
app.delete('/secure/menues/:id',Authz.role(['pagine','admin']),Menu.delete); // delete singolo menu

//faqs
app.post('/secure/faqs', Authz.role(['admin','faq']), Faq.save);
app.get('/secure/faqs', Faq.query);
app.get('/secure/faqs/count', Faq.count);
app.get('/secure/faqs/:id', Faq.get);
app.put('/secure/faqs/:id', Authz.role(['admin','faq']), Faq.update);
app.delete('/secure/faqs/:id', Authz.role(['admin','faq']),Faq.delete);

//glossario
app.post('/secure/glossario', Authz.role(['admin','glossario']), Glossario.save);
app.get('/secure/glossario', Glossario.query);
app.get('/secure/glossario/count', Glossario.count);
app.get('/secure/glossario/:id', Glossario.get);
app.put('/secure/glossario/:id', Authz.role(['admin','glossario']), Glossario.update);
app.delete('/secure/glossario/:id', Authz.role(['admin','glossario']),Glossario.delete);

//sportelli
app.post('/secure/sportelli', Authz.role(['admin','sportelli']), Sportello.save);
app.get('/secure/sportelli', Sportello.query);
app.get('/secure/sportelli/count', Sportello.count);
app.get('/secure/sportelli/:id', Sportello.get);
app.put('/secure/sportelli/:id', Authz.role(['admin','sportelli']), Sportello.update);
app.delete('/secure/sportelli/:id', Authz.role(['admin','sportelli']),Sportello.delete);

//articoli
app.post('/secure/articoli', Authz.role(['admin','autore','revisore']), Articolo.save);
app.get('/secure/articoli',  Articolo.query);
app.get('/secure/articoli/count', Articolo.count);
app.get('/secure/articoli/:id', Articolo.get);
app.put('/secure/articoli/:id', Authz.role(['admin','autore','revisore']), Articolo.update);
app.delete('/secure/articoli/:id', Authz.role(['admin','autore','revisore']),Articolo.delete);

//newsletter
app.post('/secure/newsletters', Authz.role(['admin','revisore']), Newsletter.save);
app.get('/secure/newsletters',Newsletter.query);
app.get('/secure/newsletters/count', Newsletter.count);
app.get('/secure/newsletters/testmail', Authz.role(['admin','revisore']), Newsletter.testmail);
app.get('/secure/newsletters/:id', Authz.role(['admin','revisore']),Newsletter.get);
app.put('/secure/newsletters/:id', Authz.role(['admin','revisore']), Newsletter.update);
app.delete('/secure/newsletters/:id', Authz.role(['admin','revisore']),Newsletter.delete);


//avvisi
app.post('/secure/avvisi', Authz.role(['admin','sportelli']), Avviso.save);
app.get('/secure/avvisi', Avviso.query);
app.get('/secure/avvisi/:id', Avviso.get);
app.put('/secure/avvisi/:id', Authz.role(['admin','sportelli']), Avviso.update);
app.delete('/secure/avvisi/:id', Authz.role(['admin','sportelli']),Avviso.delete);

//Mailchimp
app.get('/secure/mailchimp/list', Authz.role(['admin','revisore']),Mailchimp.list);
app.post('/secure/mailchimp/newcampaign', Authz.role(['admin','revisore']),Mailchimp.newcampaign);

//Laboratorio regole
app.post('/secure/laboratorio/regole', Authz.role(['admin','laboratorio']), LaboratorioRegola.save);
app.get('/secure/laboratorio/regole', LaboratorioRegola.listaRegole);
app.put('/secure/laboratorio/regole/:id', Authz.role(['admin','laboratorio']), LaboratorioRegola.update);

app.get('/secure/laboratorio/analisi', LaboratorioAnalisi.listaAnalisi);
app.delete('/secure/laboratorio/analisi/:id',  Authz.role(['admin','laboratorio']), LaboratorioAnalisi.deleteAnalisi);
app.get('/secure/laboratorio/analisi/update', Authz.role(['admin','laboratorio']), LaboratorioAnalisi.updateAnalisi);
app.get('/laboratorio/analisi', LaboratorioAnalisi.get);

// recupero dei logs
app.get('/secure/logs', Authz.role(['admin']), Log.query);
app.get('/secure/logcodes', Authz.role(['admin']), Log.codes);

// lettura casetta
app.get('/secure/lettura_casetta', LetturaCasetta.query);
app.get('/secure/lettura_casetta/:id', LetturaCasetta.get);

// recupera utenti da Auth0
app.get('/secure/auth0/operatori_casette', Auth0.operatori_casette); // prende da auth0 tutti gli operatori casette

// signed url per upload lato client
app.post('/s3', S3.getSignedUrl); // genera un signed url di s3 per fare un upload lato client
// export in excel
app.post('/secure/xls', Excel.generate); // genera un excel da una collection
// import da excel
app.post('/secure/xls/import/:tipo', Excel.import); // importa da excel (tipo: cosa importare, esempio 'analisi')

// invio mail
app.post('/secure/email', Email.send); // invia una mail

// cerca se esiste uno slug e se non esista lo setta (vale per tutte le collection)
app.get('/secure/slug', Slug.get);

/* @@@@@@@@@@@@@@ API V1 @@@@@@@@@@@@@@  */

 /* elenco comunicati per il sito: per anno, con limite (opzionale) torna l'elenco di quelli col flag visibile in ordine di data descrescente */
app.get('/api/v1/comunicati/sito', ApiV1.comunicati.sito);
 /* get del singolo comunicato */
app.get('/api/v1/comunicato/:id', ApiV1.comunicati.get);
 /* elenco eventi col flag visibile sul sito in ordine di data descrescente*/
 app.get('/api/v1/eventi/sito', ApiV1.eventi.sito);
 /* get del singolo evento */
app.get('/api/v1/evento/:id', ApiV1.eventi.get);
 /* elenco dei bandi col flag visibile sul sito in ordine di data descrescente*/
app.get('/api/v1/bandi/sito', ApiV1.bandi.sito);
 /* get del singolo bando */
app.get('/api/v1/bando/:id', ApiV1.bandi.get);
 /* Download massivo dei documenti di un bando */
app.get('/api/v1/bando/:id/download', ApiV1.bandi.download);
 /* elenco esiti per il sito: per anno, con limite (opzionale) torna l'elenco di quelli col flag visibile in ordine di data descrescente */
app.get('/api/v1/esiti/sito', ApiV1.esiti.sito);
 /* get del singolo esito */
app.get('/api/v1/esito/:id', ApiV1.esiti.get);
 /* elenco casette */
app.get('/api/v1/casette/sito', ApiV1.casette.sito);
app.get('/api/v1/casette/sito_esteso', ApiV1.casette.sito_esteso);
 /* singola casetta con ultimo rilevamento e dati derivati */
app.get('/api/v1/casetta/:id', ApiV1.casette.get);
 /* get del singola pagina */
app.get('/api/v1/pagina/:slug(*)', ApiV1.pagine.get);
 /* get dei menu disponibili */
app.get('/api/v1/menues', ApiV1.menues.get);
 /* get menu per nome */
app.get('/api/v1/menues/:titolo', ApiV1.menues.titolo);
/* elenco articoli per il sito:torna l'elenco di quelli con stato pubblicato in ordine di data descrescente */
app.get('/api/v1/articoli/sito', ApiV1.articoli.sito);
/* get del singolo articolo */
app.get('/api/v1/articolo/:id', ApiV1.articoli.get);

/* elenco sportelli per il sito:torna l'elenco di quelli con stato pubblicato in ordine di comune descrescente */
app.get('/api/v1/sportelli/sito', ApiV1.sportelli.sito);

 /* get del singolo avviso per sezione*/
 app.get('/api/v1/avvisi/:sezione', ApiV1.avvisi.sito);

/* elenco faq per il sito:torna l'elenco di quelli con stato pubblicato in ordine di comune descrescente */
app.get('/api/v1/faqs/sito', ApiV1.faqs.sito);

/* elenco glossario per il sito:torna l'elenco di quelli con stato pubblicato in ordine di comune descrescente */
app.get('/api/v1/glossario/sito', ApiV1.glossario.sito);

/* elenco faq per il sito:torna l'elenco di quelli con stato pubblicato in ordine di comune descrescente */
app.get('/api/v1/faqs/sito', ApiV1.faqs.sito);

/* analisi laboratorio: restituisce l'ultima analisi pubblicata */
app.get('/api/v1/analisi/sito', ApiV1.analisi.sito);

/* analisi laboratorio: restituisce l'ultima analisi pubblicata */
app.get('/api/v1/analisi/pdf/:codiceSif', ApiV1.analisi.pdf);

/*Lettura casetta V1 */
app.post('/api/v1/casetta_lettura', ApiV1.casetta.lettura.save);
app.get('/api/v1/casetta/:id', ApiV1.casetta.dettaglio);

/* salvataggio rilevamento */
app.post('/api/v1/intervento', InterventoCasetta.save); // salvataggio intervento
app.get('/api/v1/interventi_casetta', InterventoCasetta.query); // elenco interventi

/* @@@@@@@@@@@@@@ API V2 @@@@@@@@@@@@@@  */
app.get('/api/v2/casette/:id/video', ApiV2.casette.video); // pagina per la casetta
app.post('/api/v2/casetta_lettura', ApiV2.casetta.lettura.save);

// listen on...
app.listen(process.env.PORT || 3000);
