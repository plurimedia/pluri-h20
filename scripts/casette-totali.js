/*
 * aggiorna il settore di un cantiere a quello della sua commessa
 */

var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = 'mongodb://plurimedia:Canonico27!@ds021388-a0.mlab.com:21388,ds021388-a1.mlab.com:21388/heroku_4fs9nlvh?replicaSet=rs-ds021388',
    InterventoCasettaSchema = require(process.cwd() + '/server/models/intervento_casetta'),
    InterventoCasetta = mongoose.model('InterventoCasetta'),
    CasettaSchema = require(process.cwd() + '/server/models/casetta'),
    Casetta = mongoose.model('Casetta'),
    fs = require("fs"),
    csv = require("fast-csv"),
    ws = fs.createWriteStream(process.cwd() + "/.tmp/totali_casette.csv"),
    result = [];


mongoose.connect(db);

mongoose.connection.once('open', function() {

    Casetta.find({}, function(err, casette){

    	async.each(casette, function(casetta, cb) {

    		InterventoCasetta.find({casetta: casetta._id, tipo: 'rilevamento', data: { $gte: new Date("2017-01-01T00:00:00.000Z"), $lt: new Date("2017-12-31T00:00:00.000Z")}}, function (err, interventi) {

    			console.log('trovati '+ interventi.length + ' interventi per ' + casetta.descrizione)


    			interventi = _.map(interventi, function(i) {
    				return { 
    					nome: casetta.descrizione,
    					ambiente: interventi[interventi.length - 1].ambiente - interventi[0].ambiente,
    					fredda: interventi[interventi.length - 1].fredda - interventi[0].fredda,
    					gasata: interventi[interventi.length - 1].gasata - interventi[0].gasata,
    					totale: (interventi[interventi.length - 1].ambiente - interventi[0].ambiente) +
    							(interventi[interventi.length - 1].fredda - interventi[0].fredda) +
    							(interventi[interventi.length - 1].gasata - interventi[0].gasata)
    				}
    			})

    			if(interventi) {
					result.push(interventi)		
					cb()
    			}
    			else {
    				cb();
    			}
    		}).sort({data: 1})

    	}, function(){
            
            var output = _.groupBy(_.flatten(result), function(a){
            	return a.nome
            })


            var final = []
            _.each(output, function(value, key) { 
            	//console.log(value)
            		final.push(
            			{ 
            				casetta: key,
            				ambiente: value[0].ambiente,
            				fredda: value[0].fredda,
            				gasata: value[0].gasata,
            				totale: value[0].totale
            			})
           		}, 
            0);

			csv
			   .write(final, {headers: true})
			   .pipe(ws);

            mongoose.connection.close()    		
    	})

    })

});
